package com.coloring.book.mandala.forme.art.Model;

import android.graphics.Bitmap;

import java.io.Serializable;
import java.util.ArrayList;

public class ImageObject implements Serializable {

    public String status;
    public String message;

    public ArrayList<dataobj> data;


    public static class dataobj implements Serializable {

        public String id;
        public String app_id;
        public String position;
        public String image;

        public Bitmap getImg_bitmap() {
            return img_bitmap;
        }

        public void setImg_bitmap(Bitmap img_bitmap) {
            this.img_bitmap = img_bitmap;
        }

        public Bitmap img_bitmap;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getApp_id() {
            return app_id;
        }

        public void setApp_id(String app_id) {
            this.app_id = app_id;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getThumb_image() {
            return thumb_image;
        }

        public void setThumb_image(String thumb_image) {
            this.thumb_image = thumb_image;
        }

        public String thumb_image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<dataobj> getData() {
        return data;
    }

    public void setData(ArrayList<dataobj> data) {
        this.data = data;
    }
}
