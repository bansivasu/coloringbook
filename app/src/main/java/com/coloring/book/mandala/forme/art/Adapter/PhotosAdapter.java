package com.coloring.book.mandala.forme.art.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.coloring.book.mandala.forme.art.Model.MainStoreObj;
import com.coloring.book.mandala.forme.art.R;
import com.coloring.book.mandala.forme.art.share.DisplayMetricsHandler;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PhotosAdapter extends Adapter<ViewHolder> {
    private List<File> al_commons = new ArrayList<>();
    private Context mContext;
    private OnItemClickListener mOnItemClickListener;
    private ArrayList<MainStoreObj> pathlists = new ArrayList<>();

    public class MenuItemViewHolder extends ViewHolder {
        public ImageView iv_full_img;
        private ProgressBar progressBar1;

        public MenuItemViewHolder(View itemView) {
            super(itemView);
            this.iv_full_img = (ImageView) itemView.findViewById(R.id.iv_full_image);
            progressBar1 = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int i);
    }

    public PhotosAdapter(Context context, List<File> commons, OnItemClickListener onItemClickListener) {
        this.mContext = context;
        this.al_commons = commons;
        this.mOnItemClickListener = onItemClickListener;
    //    sd = new SectorsDAO(mContext, DataBaseHelper.SECTORS.SECTORS_PHIL);
    }

    public int getItemViewType(int position) {
        return position;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MenuItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false));
    }

    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Log.e("TAG", "onBindViewHolder==>");
//        holder.setIsRecyclable(false);
        final MenuItemViewHolder holder1 = (MenuItemViewHolder) holder;

        Glide.with(this.mContext).load(this.al_commons.get(position))
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .listener(new RequestListener<File, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, File model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, File model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            ((MenuItemViewHolder) holder).progressBar1.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(holder1.iv_full_img);

        holder1.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemClickListener.onItemClick(view, position);
            }
        });

        holder1.iv_full_img.getLayoutParams().height = (int) (DisplayMetricsHandler.getScreenWidth() * 0.46);
    }

    public int getItemCount() {
        Log.e("TAG", "Home Adpater array size:==>" + this.al_commons.size());
        return this.al_commons.size();
    }
}
