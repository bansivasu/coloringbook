package com.coloring.book.mandala.forme.art;

/**
 * Created by Bansi on 17-02-2018.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;
import com.coloring.book.mandala.forme.art.Adapter.CustomDrawerAdapter;
import com.coloring.book.mandala.forme.art.Billing.IabHelper;
import com.coloring.book.mandala.forme.art.Billing.IabResult;
import com.coloring.book.mandala.forme.art.Billing.Purchase;
import com.coloring.book.mandala.forme.art.Fragment.MoreappFragment;
import com.coloring.book.mandala.forme.art.Fragment.MyFavouriteFragment;
import com.coloring.book.mandala.forme.art.Fragment.MyPhotosFragment;
import com.coloring.book.mandala.forme.art.Interface.FragmentCommunicator;
import com.coloring.book.mandala.forme.art.Model.DrawerItem;
import com.coloring.book.mandala.forme.art.share.InAppBillingHandler;
import com.coloring.book.mandala.forme.art.share.Share;
import com.coloring.book.mandala.forme.art.share.SharedPrefs;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    //Widgets------------------
    private DrawerLayout mDrawerLayout;
    private Toolbar toolbar;
    TextView tv_menu_title;
    //NavigationView navigationView;
    LinearLayout ll_Main;
    MenuItem mi_drawMore, mi_more_apps, mi_remove_ad, mi_delete_pic;
    ImageView iv_no_adv;
    //-------------------------

    //Variables----------------
    SharedPreferences mPrefs;
    private Boolean binded = false;

    // ====== For Ads ====== //
    ImageView iv_home_arrow, iv_draw_more_arrow, iv_fav_arrow, iv_share_arrow, iv_contact_arrow;
    public final int MY_PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL = 1;
    int k = 0;
    ProgressDialog progressBar;

    private IInAppBillingService mService;
    /**
     * android.test.purchased   so you can use this test in place of PRODUCT_ID
     * When you make an In-app Billing request with this product ID, Google Play responds as though you successfully purchased an item.
     * The response includes a JSON string, which contains fake purchase information (for example, a fake order ID). In some cases,
     * the JSON string is signed and the response includes the signature so you can test your signature verification implementation using these responses.
     */
//    static final String SKU_AD_REMOVE_ITEM = "android.test.purchased";
    //private static final String LICENSE_KEY = "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ"; // PUT YOUR MERCHANT KEY HERE;     bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ is a test key

    // TODO: 27-02-2017 Change
    static final String SKU_AD_REMOVE_ITEM = "com.coloring.book.mandala.forme.art.inapp";
    private static final String LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhLT4uxpLUY3IaWX3QJkVL7ZGyo5bK2PQ7V8Vmg5FwdUCi0l3RmVwwnGxuhyrkOBQl+Br8/Yz2kgXLc8cDQPoaXQEnzO0HdV1bbZ2GyTmzWRdqYJH5P++LNw+Ieh4PvH60uQc7wE0OV4QAhU0FzGfTZt7nNWAdn0SujmKgIhbmjqLKZf57am5NVU+sJ5jWX7N96MZV+mDXloUXtLThxOVLaZZYADWBFju6hHaPlutRsStqkOC7TRfNaGK2mAMSWFv9MAJs6E6cQPT7WLnt/qwGELV90A7+wtJt2sVYclwQM28JCK3YTPbislajCl7/+5GwqTFg64FCKGol5isGQNTkQIDAQAB"; // PUT YOUR MERCHANT KEY HERE;     bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ is a test key

    ProgressDialog upgradeDialog;
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener;

    IabHelper mHelper;
    static final int RC_REQUEST = 1;
//    private GoogleApiClient client;

    private ListView mDrawerList;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle1;
    CustomDrawerAdapter adapter;
    List<DrawerItem> dataList;

    private ActionBarDrawerToggle mDrawerToggle;

    String mTitle[];
    public Fragment fragment = null;
    private FirebaseAnalytics mFirebaseAnalytics;

    // ====== For Ads ====== //
    private AdRequest ins_adRequest;
    private InterstitialAd mInterstitialAd;
    Animation rotation;

    public FragmentCommunicator fragmentCommunicator;
    //-------------------------


    @Override
    protected void onStart() {
        super.onStart();

       /* client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.mohanbharti.draw3d/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);*/
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_start);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        InitView();
        setToolBar();
        setListeners();
        InitViewAction();
        setupNavigationView();
        bindServices();

        /*if (!SharedPrefs.getBoolean(this, SharedPrefs.IS_ADS_REMOVED)) {
            LoadAds();
        }*/

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                setFragmentView(0);
            } else {
                if (extras.getString("Page").equals("DrawMore"))
                    mDrawerList.performItemClick(mDrawerList, 1, mDrawerList.getItemIdAtPosition(1));
            }
        } else {
            setFragmentView(0);
        }
        //FirebaseCrash.report(new Exception("Test Error.."));
    }

    private void InitView() {

        ll_Main = (LinearLayout) findViewById(R.id.ll_Main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tv_menu_title = (TextView) toolbar.findViewById(R.id.tv_menu_title);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        //navigationView = (NavigationView) findViewById(R.id.nav_view);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        //Titles of side menu items
        mTitle = new String[]{getResources().getString(R.string.app_name), getResources().getString(R.string.my_favorite), getResources().getString(R.string.my_saved), getResources().getString(R.string.remove_ads),
                getResources().getString(R.string.contact_us), getResources().getString(R.string.rate_us), getResources().getString(R.string.more_app)};

        //--------------added
//        initInAppPurchase();
        //------------------
    }

    private void setupNavigationView() {

        /*iv_home_arrow = (ImageView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_home));
        iv_home_arrow.setImageDrawable(getResources().getDrawable(R.drawable.ic_next));

        iv_draw_more_arrow = (ImageView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_draw_more));
        iv_draw_more_arrow.setImageDrawable(getResources().getDrawable(R.drawable.ic_next));

        iv_fav_arrow = (ImageView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_my_favorite));
        iv_fav_arrow.setImageDrawable(getResources().getDrawable(R.drawable.ic_next));

        iv_share_arrow = (ImageView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_share));
        iv_share_arrow.setImageDrawable(getResources().getDrawable(R.drawable.ic_next));

        iv_contact_arrow = (ImageView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_contact_us));
        iv_contact_arrow.setImageDrawable(getResources().getDrawable(R.drawable.ic_next));
*/
    }

    private void setListeners() {

        //navigationView.setNavigationItemSelectedListener(this);
    }

    private void setToolBar() {

        mPrefs = getPreferences(MODE_PRIVATE);

        //setup Toolbar
        setSupportActionBar(toolbar);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerList.getLayoutParams().width = (int) (Share.screenWidth * 0.6);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name, R.string.app_name) {

            public void onDrawerClosed(View view) {
            }

            public void onDrawerOpened(View drawerView) {
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);

                //For Move Activity with slide...
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    ll_Main.setTranslationX(slideOffset * drawerView.getWidth());
                }
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        getSupportActionBar().setTitle("");
    }

    private void bindServices() {
        try {
            binded = bindService(InAppBillingHandler.getBindServiceIntent(), mServiceConn, Context.BIND_AUTO_CREATE);
        } catch (Exception e) {
        }
    }

    ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
        }
    };


    //--------------added

    private void initInAppPurchase() {
        mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
            @Override
            public void onIabPurchaseFinished(IabResult result, Purchase purchaseInfo) {

                Log.d("In App Purchase", "Purchase finished: " + result + ", purchase: " + purchaseInfo);

                // if we were disposed of in the meantime, quit.
                if (mHelper == null) {
                    Log.e("In App Purchase", "IabHelper is null");
                    return;
                }

                Log.e("In App Purchase", "result: " + result);

                if (result.isFailure()) {
                    Log.e("In App Purchase", "Error purchasing: " + result);
                    upgradeDialog.dismiss();
                } else if (purchaseInfo.getSku().equals(SKU_AD_REMOVE_ITEM)) {
                    Log.e("In App Purchase", "info.getSku().equals: " + result);
                    Toast.makeText(getApplicationContext(), "Remove ads successfully.", Toast.LENGTH_SHORT).show();
                    // alert("Thank you for upgrade");
                    /*mIsPremium = true;
                    setUserStatus(true);*/
                    upgradeDialog.dismiss();
                } else if (result.isSuccess()) {
                    Log.e("In App Purchase", "Success purchasing: " + result);
                    Toast.makeText(getApplicationContext(), "Remove ads successfully.", Toast.LENGTH_SHORT).show();
                }
            }

        };


        String base64EncodedPublicKey = LICENSE_KEY;

        // compute your public key and store it in base64EncodedPublicKey
        mHelper = new IabHelper(this, base64EncodedPublicKey);

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    Log.e("In App Purchase", "Problem setting up In-app Billing: " + result);
                }
                // Hooray, IAB is fully set up!
            }

        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void checkpurchaseItem() {

        try {
            Bundle ownedItems = mService.getPurchases(3, getPackageName(), "inapp", null);
            int response = ownedItems.getInt("RESPONSE_CODE");

            Log.e("response", response + "");

            if (response == 0) {

                ArrayList<String> ownedSkus = ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
                ArrayList<String> purchaseDataList = ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                ArrayList<String> signatureList = ownedItems.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
                String continuationToken = ownedItems.getString("INAPP_CONTINUATION_TOKEN");

                if (purchaseDataList.size() > 0) {

                    for (int i = 0; i < purchaseDataList.size(); ++i) {
                        String purchaseData = purchaseDataList.get(i);
                        String signature = signatureList.get(i);
                        String sku = ownedSkus.get(i);
                        //Log.e("Item", purchaseData + "---" + signature + "---" + sku + "----" + continuationToken);
                        //tv_details.setText("Item" + purchaseData + "---" + signature + "---" + sku + "----" + continuationToken);
                    }
//                    removeAds();
                    SharedPrefs.savePref(this, SharedPrefs.IS_ADS_REMOVED, true);
//                    showToastMsg("You already purchased.");
                    Share.showAlert(HomeActivity.this, getString(R.string.app_name), getString(R.string.purchased_msg));

                } else {
                    purchaseItem();
                }
                // if continuationToken != null, call getPurchases again and pass in the token to retrieve more items
            }
        } catch (Exception e) {
            //Log.e("Errorget", e.getMessage());
            e.printStackTrace();
        }
    }

    private void purchaseItem() {
        AlertDialog.Builder upgradeAlert = new AlertDialog.Builder(this);
//        upgradeAlert.setTitle("Remove Ads?");
        upgradeAlert.setMessage(getString(R.string.remove_ad_msg));
        upgradeAlert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //set progress dialog and start the in app purchase process
                upgradeDialog = ProgressDialog.show(HomeActivity.this, "Please wait", "", true);

              /* TODO: for security, generate your payload here for verification. See the comments on
     *        verifyDeveloperPayload() for more info. Since this is a SAMPLE, we just use
     *        an empty string, but on a production app you should carefully generate this. */
                String payload = "";

                mHelper.launchPurchaseFlow(HomeActivity.this, SKU_AD_REMOVE_ITEM, RC_REQUEST, mPurchaseFinishedListener, payload);
                //  mHelper.l

                upgradeDialog.dismiss();
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                if (upgradeDialog != null && upgradeDialog.isShowing())
                    upgradeDialog.dismiss();
            }
        });

        upgradeAlert.show();
    }
    //---------------------

    /*private void purchageItem() {

        try {
            Bundle ownedItems = mService.getPurchases(3, getPackageName(), "inapp", null);
            int response = ownedItems.getInt("RESPONSE_CODE");
            //Log.e("response", response + "");

            if (response == 0) {

                ArrayList<String> ownedSkus = ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
                ArrayList<String> purchaseDataList = ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                ArrayList<String> signatureList = ownedItems.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
                String continuationToken = ownedItems.getString("INAPP_CONTINUATION_TOKEN");

                if (purchaseDataList.size() > 0) {

                    for (int i = 0; i < purchaseDataList.size(); ++i) {
                        String purchaseData = purchaseDataList.get(i);
                        String signature = signatureList.get(i);
                        String sku = ownedSkus.get(i);
                        //Log.e("Item", purchaseData + "---" + signature + "---" + sku + "----" + continuationToken);
                        //tv_details.setText("Item" + purchaseData + "---" + signature + "---" + sku + "----" + continuationToken);
                    }
                    removeAds();
                    SharedPrefs.savePref(this, SharedPrefs.IS_ADS_REMOVED, true);
                    showToastMsg("You already purchased.");
                } else {
                    buyItem();
                }
                // if continuationToken != null, call getPurchases again and pass in the token to retrieve more items
            }
        } catch (Exception e) {
            //Log.e("Errorget", e.getMessage());
        }
    }

    private void buyItem() {
        try {
            Bundle buyIntentBundle = mService.getBuyIntent(3, getPackageName(), SKU_AD_REMOVE_ITEM, "inapp", LICENSE_KEY);

            PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");

            startIntentSenderForResult(pendingIntent.getIntentSender(), 1001, new Intent(), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0));
        } catch (RemoteException e) {
            //e.printStackTrace();
        } catch (IntentSender.SendIntentException e) {
            //e.printStackTrace();
        }
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        mi_more_apps = menu.findItem(R.id.more_apps);
        mi_delete_pic = menu.findItem(R.id.delete_items);

        /* For give animation on header icon
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        iv_no_adv = (ImageView) inflater.inflate(R.layout.layout_iv_add, null);
        rotation = AnimationUtils.loadAnimation(this, R.anim.shake_anim);
        rotation.setRepeatCount(Animation.ABSOLUTE);
        iv_no_adv.startAnimation(rotation);
        mi_remove_ad.setActionView(iv_no_adv); */

        // change menu icon on titlebar for each fragment
        if (tv_menu_title.getText().equals(getResources().getString(R.string.app_name))) {
            mi_delete_pic.setVisible(false);
            mi_more_apps.setVisible(true);
        }  else if (tv_menu_title.getText().equals(getResources().getString(R.string.my_favorite))) {
            mi_delete_pic.setVisible(true);
            mi_more_apps.setVisible(false);
        } else if (tv_menu_title.getText().equals(getResources().getString(R.string.my_saved))) {
            mi_delete_pic.setVisible(true);
            mi_more_apps.setVisible(false);
        } else {
            mi_delete_pic.setVisible(false);
            mi_more_apps.setVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Log.e("id of clicked item","id"+item.getItemId());
        switch (item.getItemId()) {
            case R.id.delete_items:
                break;
            case R.id.more_apps:
                Intent go2more = new Intent(HomeActivity.this, DrawMoreSingleActivity.class);
                startActivity(go2more);
                //AccountRedirectActivity.get_url(HomeActivity.this);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void InitViewAction() {

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_header_side_menu);

        dataList = new ArrayList<DrawerItem>();
        dataList.add(new DrawerItem(getResources().getString(R.string.home), R.drawable.ic_side_menu_home));
        dataList.add(new DrawerItem(getResources().getString(R.string.my_favorite), R.drawable.ic_side_menu_favourites));
        dataList.add(new DrawerItem(getResources().getString(R.string.my_saved), R.drawable.ic_side_menu_mysaved));
//        dataList.add(new DrawerItem(getResources().getString(R.string.unlocked), R.drawable.ic_noads));
        dataList.add(new DrawerItem(getResources().getString(R.string.contact_us), R.drawable.ic_side_menu_contactus));
        dataList.add(new DrawerItem(getResources().getString(R.string.rate_us), R.drawable.ic_side_menu_rate_us));
        dataList.add(new DrawerItem(getResources().getString(R.string.more_app), R.drawable.ic_side_menu_moreapps));

        adapter = new CustomDrawerAdapter(this, R.layout.drawer_menu_layout, dataList);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        //For getting Actionbat Height
        int[] abSzAttr;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            abSzAttr = new int[]{android.R.attr.actionBarSize};
        } else {
            abSzAttr = new int[]{R.attr.actionBarSize};
        }
        TypedArray a = obtainStyledAttributes(abSzAttr);
        int actionBarHeight = a.getDimensionPixelSize(0, -1);
        //----------------------------
        //for getting navigation header view
       /* View hView = navigationView.getHeaderView(0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            int statusBarHeight = 0;
            int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                statusBarHeight = getResources().getDimensionPixelSize(resourceId);
            }


            LinearLayout ll_status = (LinearLayout) hView.findViewById(R.id.ll_status);
            ll_status.getLayoutParams().height = (int) (statusBarHeight);
            Log.e("statusBarHeight", " " + statusBarHeight);

            //for getting navigation header view
            LinearLayout ll_header_layout = (LinearLayout) hView.findViewById(R.id.ll_header_layout);
            ll_header_layout.getLayoutParams().height = (int) (actionBarHeight);
        }*/

        progressBar = new ProgressDialog(HomeActivity.this);
        progressBar.setCancelable(false);
        progressBar.setMessage("Downloading Offline Data...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.left_in, R.anim.right_out);
        }
    }

    public void setFragmentView(int position) {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        switch (position) {
            case 0:
                fragment = new MoreappFragment();
                tv_menu_title.setText(mTitle[0]);
                //((MainHomeFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame)).adRemove();
                break;
            case 1:
                fragment = new MyFavouriteFragment();
                tv_menu_title.setText(mTitle[1]);
                break;
            case 2:
                fragment = new MyPhotosFragment();
                tv_menu_title.setText(mTitle[2]);
                break;
            /*case 3:
                if (SharedPrefs.getBoolean(HomeActivity.this, SharedPrefs.IS_ADS_REMOVED)) {
                    checkpurchaseItem();
                } else {
                    checkpurchaseItem();
                }
                break;*/
            case 3:
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@vasundharavision.com"});
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
                drawer.closeDrawer(GravityCompat.START);
                break;
            case 4:
                drawer.closeDrawer(GravityCompat.START);
                try {
                    Intent ratingIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName()));
                    startActivity(ratingIntent);

                } catch (ActivityNotFoundException e) {
                    // If play store not found then it open in browser
                    Intent ratingIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName()));
                    startActivity(ratingIntent);
                }
                return;
            case 5:
                AccountRedirectActivity.get_url(HomeActivity.this);
                break;
            default:
                break;
        }
        // TODO: Added adv. Option in Home 27-02-2017
        if (mi_drawMore != null && mi_more_apps != null && mi_remove_ad != null) {
            if (tv_menu_title.getText().equals(getResources().getString(R.string.app_name))) {
                mi_remove_ad.setVisible(true);
                iv_no_adv.setVisibility(View.VISIBLE);
                mi_drawMore.setVisible(true);
                mi_more_apps.setVisible(false);
            } else if (tv_menu_title.getText().equals(getResources().getString(R.string.my_favorite))) {
                mi_remove_ad.setVisible(false);
                iv_no_adv.setVisibility(View.GONE);
                mi_drawMore.setVisible(true);
                mi_more_apps.setVisible(false);
            } else if (tv_menu_title.getText().equals(getResources().getString(R.string.my_saved))) {
                mi_remove_ad.setVisible(false);
                iv_no_adv.setVisibility(View.GONE);
                mi_drawMore.setVisible(true);
                mi_more_apps.setVisible(false);
            }
        }
        if (fragment != null) {
            if (!isFinishing()) {
                try {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commitAllowingStateLoss();
                } catch (Exception e) {
                }
            }
        } else {
            showToastMsg(getResources().getString(R.string.exception_msg));
        }
        mDrawerList.setItemChecked(position, true);
        drawer.closeDrawer(GravityCompat.START);
    }

    private void showToastMsg(String msg) {
        Toast toastMsg = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT);
        toastMsg.show();
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            if (position >= 0 && position <= 2) {
                Share.SELECTED = position;
            }
            adapter.notifyDataSetChanged();
            setFragmentView(position);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        /*if (requestCode == 1001) {
            int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
            String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
            String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");

            if (resultCode == RESULT_OK) {
                try {
                    JSONObject jo = new JSONObject(purchaseData);
                    String sku = jo.getString("productId");
                    String purchaseToken = jo.getString("purchaseToken");

                    //Log.e("onActivityResult", "Purchaged");
                    SharedPrefs.savePref(this, SharedPrefs.IS_ADS_REMOVED, true);
                    removeAds();
                    //Log.e("MainActivity", "You have bought the " + jo + ". Excellent choice,adventurer!");
                    //Log.e("MainActivity", "You have bought the " + purchaseToken + ". Excellent choice,adventurer!");
                    //tv_details.setText(jo.toString());
                } catch (JSONException e) {
                    //Log.e("MainActivity Error", "Failed to parse purchase data.");
                    e.printStackTrace();
                }
            }
        }*/

        Log.e("InAppPurchase", "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        if (mHelper == null) return;

//        Log.e("onActivityResult", "" + mHelper.handleActivityResult(requestCode, resultCode, data));

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app billing...
            Log.e("InAppPurchase", "onActivityResult not handled by IABUtil.");
            super.onActivityResult(requestCode, resultCode, data);
        } else {

            try {
                int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
                String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
                String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");

                if (purchaseData != null) {
                    JSONObject jo = new JSONObject(purchaseData);
                    String sku = jo.getString("productId");
                    String purchaseToken = jo.getString("purchaseToken");

                    Log.e("onActivityResult", "Purchased");
                    SharedPrefs.savePref(this, SharedPrefs.IS_ADS_REMOVED, true);
//                    removeAds();
                 //   Share.showAlert(HomeActivity.this, getString(R.string.app_name), getString(R.string.remove_ads_msg));
                    //Log.e("MainActivity", "You have bought the " + jo + ". Excellent choice,adventurer!");
                    //Log.e("MainActivity", "You have bought the " + purchaseToken + ". Excellent choice,adventurer!");
                }
            } catch (JSONException e) {
                //Log.e("MainActivity Error", "Failed to parse purchase data.");
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onPause() {
        try {
            if (binded) {
                unbindService(mServiceConn);
                binded = false;
            }
        }
        catch (Exception e){
            e.printStackTrace();
            super.onPause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /*if (mService != null) {
            unbindService(mServiceConn);
        }*/

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
      /*  Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.mohanbharti.draw3d/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();*/
    }

/*
    public void removeAds() {

        if (fragment instanceof MainHomeFragment) {
            ((MainHomeFragment) fragment).adRemove();
            return;
        } else if (fragment instanceof DrawMoreFragment) {
            ((DrawMoreFragment) fragment).adRemove();
            return;
        } else if (fragment instanceof MyFavoriteFragment) {
            ((MyFavoriteFragment) fragment).adRemove();
            return;
        } else if (fragment instanceof MySavedFragment) {
            ((MySavedFragment) fragment).adRemove();
            return;
        }
    }
*/

    private void LoadAds() {


        mInterstitialAd = new InterstitialAd(getApplication());

        mInterstitialAd.setAdUnitId(getApplicationContext().getResources().getString(R.string.Intertial_Id));

        //String android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        //String deviceId = md5(android_id).toUpperCase();

        ins_adRequest = new AdRequest.Builder()
                //.addTestDevice(deviceId)
                .addTestDevice("E19949FB5E7C5A28C30A875934AC8181") //SWIPE
                .addTestDevice("41E9C9F5D1F985FB36C9760EFC8F3916") //Lenovo
                .addTestDevice("CEB37F370ECF287C057F70BBC902632E")  //Coolpad
                .addTestDevice("51A49E7B1B359D1999E5C85CE4F54978") //XOLO
                .addTestDevice("F9EBC1840023CB004A83005514278635")  //MI 6otu (No SIM)
                .addTestDevice("2442EC754FEF046014B26ACCEEAE9C23") // Micromax New
                .addTestDevice("413FAED40213710754F4D30AC4F60355")  //INTEX
                .addTestDevice("A7A19E06342F7D3868ABA7863D707BD7") //Samsung Tab
                .addTestDevice("F9EB72307322A818A3399B51B00A765D") //LAVA
                .addTestDevice("0E45853B0874EAA6418891AD964CC470") //X-ZIOX
                .addTestDevice("3C8E4AA9C3802D60B83603426D16E430") //Celkon
                .addTestDevice("89FAEE279F58CCC3FA1ABC0904E1FCBE") //Karbonn
                .addTestDevice("74527FD0DD7B0489CFB68BAED192733D") //Nexus TAB
                .addTestDevice("26DFCD14E05E54F19609B18400F672A0") //Samsung j2
                .addTestDevice("E56855A0C493CEF11A7098FE6EA840CB") //Videocon
                .addTestDevice("390FED1AE343E9FF9D644C4085C3868E") //jivi
                .addTestDevice("ACFC7B7082B3F3FD4E0AC8E92EA10D53") //MI Tab
                .addTestDevice("863D8BAE88E209F38FF3C94A0403C776") //Samsung old
                .addTestDevice("85F8E335F3BBDFA2782587EEA19688D2") //Samsung new
                .addTestDevice("517048997101BE4535828AC2360582C2") //motorola
                .addTestDevice("8BB4BCB27396AB8ED222B7F902E13420") //micromax old
                .addTestDevice("7F3924D7AB776DC5FF2D314CCFEE0EE0")//Gionee
                .addTestDevice("F7803FE72A2748F6028D87DC36D7C574") //Mi Chhotu JIO
                .addTestDevice("36FE3612E964A00DC40A18301E964202") // nokia
                .build();

        mInterstitialAd.loadAd(ins_adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                requestNewInterstitial();
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }
        });
    }

    private void requestNewInterstitial() {

        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
            mInterstitialAd.setAdListener(null);
            mInterstitialAd = null;
            ins_adRequest = null;
        }
    }
}