package com.coloring.book.mandala.forme.art.share;

import com.coloring.book.mandala.forme.art.Model.ImageObject;
import com.coloring.book.mandala.forme.art.Model.moreAppObject;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

public class Urls {

    public static String BASE_URL_APPS = "http://vasundharaapps.com/artwork_apps/api";
    public static String BASE_URL = "http://admin.vasundharavision.com/art_work/api";
//    public static String DRAW_MORE_URL = "http://www.vasundharavision.com/Home_products/app.txt";

    public interface urls {

        @GET("/ColoringOnPicAndroidApplications/{page}/1")
        void getstatusObj(@Path("page") int page, Callback<ImageObject> callback);

        @GET("/ColoringOnPic/1")
        void getmoreAppObj(Callback<moreAppObject> callback);
    }

    public static final String URL0 = "https://play.google.com/store/apps/developer?id=Suit+Photo+Editor+Montage+Maker+%26+Face+Changer";
    public static final String URL1 = "https://play.google.com/store/apps/developer?id=Vipulpatel808";
    public static final String URL2 = "https://play.google.com/store/apps/developer?id=Pic+Frame+Photo+Collage+Maker+%26+Picture+Editor";
    public static final String URL3 = "https://play.google.com/store/apps/developer?id=Prank+App";
    public static final String URL4 = "https://play.google.com/store/apps/developer?id=Vasundhara%20Game%20Studios";
    public static final String URL5 = "https://play.google.com/store/apps/developer?id=Background+Changer,+Eraser+%26+Booth+Photo+Editor";
    public static final String URL6 = "https://play.google.com/store/apps/dev?id=7088147337905312097";

}

