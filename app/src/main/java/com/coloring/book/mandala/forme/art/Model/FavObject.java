package com.coloring.book.mandala.forme.art.Model;

import java.io.Serializable;

public class FavObject implements Serializable {

    public String image_id;
    public String app_id;
    public String path;
    public String thumb_url;
    public String image_url;
    public String dateString;
    public String cat_name;

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String app_name) {
        this.cat_name = app_name;
    }

    public String getDateString() {
        return dateString;
    }

    public String getThumb_url() {
        return thumb_url;
    }

    public void setThumb_url(String thumb_url) {
        this.thumb_url = thumb_url;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getImage_id() {
        return image_id;
    }

    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

}
