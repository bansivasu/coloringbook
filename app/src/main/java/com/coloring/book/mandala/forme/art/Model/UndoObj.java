package com.coloring.book.mandala.forme.art.Model;

/**
 * Created by Bansi on 22-11-2017.
 */
public class UndoObj {

    public int sector;
    public int prev_color;
    public int fill_color;

    public int getSector() {
        return sector;
    }

    public int getPrev_color() {
        return prev_color;
    }

    public int getFill_color() {
        return fill_color;
    }

    public void setSector(int sector) {
        this.sector = sector;
    }

    public void setPrev_color(int prev_color) {
        this.prev_color = prev_color;
    }

    public void setFill_color(int fill_color) {
        this.fill_color = fill_color;
    }
}
