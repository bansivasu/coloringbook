package com.coloring.book.mandala.forme.art.Interface;

/**
 * Created by Bansi on 26-05-2017.
 */
public interface FragmentCommunicator {

    public void communicateToCustomDraw();

    public void communicateToMyPhotos();

    public void communicateToFav();
}
