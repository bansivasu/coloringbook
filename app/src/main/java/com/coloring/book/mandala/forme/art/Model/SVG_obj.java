package com.coloring.book.mandala.forme.art.Model;

/**
 * Created by Bansi on 24-11-2017.
 */
public class SVG_obj {

    private int sector;
    private int fill_color;


    public int getSector() {
        return sector;
    }

    public void setSector(int sector) {
        this.sector = sector;
    }

    public int getFill_color() {
        return fill_color;
    }

    public void setFill_color(int fill_color) {
        this.fill_color = fill_color;
    }
}
