package com.coloring.book.mandala.forme.art;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.coloring.book.mandala.forme.art.share.NetworkManager;
import com.coloring.book.mandala.forme.art.share.Share;

public class FullScreenAdActivity extends Activity implements OnClickListener {

    private Intent intent;
    private Button btn_cancel, btn_download;
    private ImageView imageView;
    private RelativeLayout relative;
    Animation zoomin, zoomout, buttonanim;
    Bitmap b;

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.full_screen_ad);

        findParams();
        setListners();
        InitAction();
    }

    private void findParams() {
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_download = (Button) findViewById(R.id.btn_download);
        imageView = (ImageView) findViewById(R.id.cock);
        relative = (RelativeLayout) findViewById(R.id.relative);
    }

    private void setListners() {
        btn_download.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
    }

    private void InitAction() {

        Log.e("init", "init");
        try {

            if (!Share.full_ad.get(Share.AD_index).getFull_img().equalsIgnoreCase("")) {
                byte[] b = Base64.decode(Share.full_ad.get(Share.AD_index).getFull_img(), Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
                imageView.setImageBitmap(bitmap);
            }
            /*    if (GlobalData.full_ad.get(GlobalData.AD_index).getB() != null)
                imageView.setImageBitmap(GlobalData.full_ad.get(GlobalData.AD_index).getB());*/
              /*  if (GlobalData.icon_url != null) {
                    Glide.with(FullScreenAdActivity.this)
                            .load(GlobalData.icon_url)
                            .into(imageView);
                } else if (isPackegeAvail("com.onexsoftech.gpsroutefinder", getApplicationContext())) {
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.adcalleranouncer));
                } else {
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.route));
                }*/


            zoomin = AnimationUtils.loadAnimation(this, R.anim.zoomin);
            zoomout = AnimationUtils.loadAnimation(this, R.anim.zoomout);
            buttonanim = AnimationUtils.loadAnimation(this, R.anim.shake);
            // imageView.setAnimation(zoomin);
            // imageView.setAnimation(zoomout);
            btn_download.setAnimation(buttonanim);

            buttonanim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    btn_download.startAnimation(buttonanim);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            zoomin.setAnimationListener(new Animation.AnimationListener() {

                @Override
                public void onAnimationStart(Animation arg0) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onAnimationRepeat(Animation arg0) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onAnimationEnd(Animation arg0) {
                    imageView.startAnimation(zoomout);

                }
            });

            zoomout.setAnimationListener(new Animation.AnimationListener() {

                @Override
                public void onAnimationStart(Animation arg0) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onAnimationRepeat(Animation arg0) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onAnimationEnd(Animation arg0) {
                    imageView.startAnimation(zoomin);

                }
            });

            imageView.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    try {
                        if (NetworkManager.isInternetConnected(FullScreenAdActivity.this)) {
                            intent = new Intent("android.intent.action.VIEW", Uri.parse(String.format("market://details?id=%1$s", new Object[]{Share.full_ad.get(Share.AD_index).getPackage_name()})));
                        }
                        /*else if (isPackegeAvail("com.onexsoftech.gpsroutefinder", getApplicationContext())) {
                            intent = new Intent("android.intent.action.VIEW", Uri.parse(String.format("market://details?id=%1$s", new Object[]{"com.onexsoftech.fingerprintbloodpressureprank"})));
                        } else {
                            intent = new Intent("android.intent.action.VIEW", Uri.parse(String.format("market://details?id=%1$s", new Object[]{"com.onexsoftech.gpsroutefinder"})));
                        }*/
                        startActivity(intent);
                    } catch (Exception e) {
                        intent = new Intent("android.intent.action.VIEW", Uri.parse(String.format("market://details?id=%1$s", new Object[]{"com.onexsoftech.gpsroutefinder"})));
                        startActivity(intent);
                    }
                    return false;
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isPackegeAvail(String str, Context context) {
        try {
            context.getPackageManager().getPackageInfo(str, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {

        if (v == btn_cancel) {
            finish();
        } else if (v == btn_download) {
            try {
                // if (NetworkManager.isInternetConnected(getApplicationContext())) {
                intent = new Intent("android.intent.action.VIEW", Uri.parse(String.format("market://details?id=%1$s", new Object[]{Share.full_ad.get(Share.AD_index).getPackage_name()})));
                /*} else if (isPackegeAvail("com.onexsoftech.gpsroutefinder", getApplicationContext())) {
                    intent = new Intent("android.intent.action.VIEW", Uri.parse(String.format("market://details?id=%1$s", new Object[]{"com.onexsoftech.fingerprintbloodpressureprank"})));
                } else {
                    intent = new Intent("android.intent.action.VIEW", Uri.parse(String.format("market://details?id=%1$s", new Object[]{"com.onexsoftech.gpsroutefinder"})));
                }*/
                startActivity(intent);
            } catch (Exception e) {
                intent = new Intent("android.intent.action.VIEW", Uri.parse(String.format("market://details?id=%1$s", new Object[]{"com.onexsoftech.gpsroutefinder"})));
                startActivity(intent);
            }
        }
    }
}
