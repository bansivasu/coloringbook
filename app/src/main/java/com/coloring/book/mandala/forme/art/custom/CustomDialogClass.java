package com.coloring.book.mandala.forme.art.custom;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.coloring.book.mandala.forme.art.AccountRedirectActivity;
import com.coloring.book.mandala.forme.art.Model.AdModel;
import com.coloring.book.mandala.forme.art.R;
import com.coloring.book.mandala.forme.art.share.Share;
import com.coloring.book.mandala.forme.art.share.SharedPrefs;
import com.coloring.book.mandala.forme.art.Splash_MenuActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bansi on 27-10-2017.
 */
public class CustomDialogClass extends Dialog implements
        View.OnClickListener {

    public Activity c;
    public Button btn_exit, btn_cancel;
    private RecyclerView rcv_ad_images;
    private CardView card_view;
    private ImageView iv_more_apps;

    public CustomDialogClass(Activity a) {
        super(a);
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.addcenter_custom_dialog);

        findViews();
        setListners();
        initViewAction();
    }

    private void findViews() {
        rcv_ad_images = (RecyclerView) findViewById(R.id.rv_load_ads);
        card_view = (CardView) findViewById(R.id.card_view);
        iv_more_apps = (ImageView) findViewById(R.id.iv_more_apps);
        btn_exit = (Button) findViewById(R.id.btn_exit);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
    }

    private void setListners() {
        btn_exit.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
        iv_more_apps.setOnClickListener(this);
    }

    private void initViewAction() {

        card_view.getLayoutParams().height = (int) (Share.screenHeight * 0.75);
        card_view.getLayoutParams().width = (int) (Share.screenWidth * 0.9);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this.c , 3);
        rcv_ad_images.setLayoutManager(gridLayoutManager);

        ArrayList<AdModel> temp_list = new ArrayList<>();
        for (int i = Share.al_ad_data.size()-1; i >= 0 ; i--) {
            temp_list.add(Share.al_ad_data.get(i));
        }
        AdImageAdapter adImageAdapter = new AdImageAdapter(this.c, temp_list);
        rcv_ad_images.setAdapter(adImageAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_exit:
                dismiss();
                Intent finish = new Intent(this.c, Splash_MenuActivity.class);
               // finish.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                c.startActivity(finish);
                c.finish();
                break;
            case R.id.btn_cancel:
                dismiss();
                break;
            case R.id.iv_more_apps:
                AccountRedirectActivity.get_url(getContext());
                break;
            default:
                break;
        }
        dismiss();
    }

    public class AdImageAdapter extends RecyclerView.Adapter<AdImageAdapter.ViewHolder> {
        Context context;
        List<AdModel> al_ad_item = new ArrayList<>();


        public AdImageAdapter(final Context context, List<AdModel> list_data) {
            this.context = context;
            this.al_ad_item = list_data;

        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView iv_ad_image;
            TextView tv_download;
            ProgressBar progressBar;

            public ViewHolder(View itemView) {
                super(itemView);
                iv_ad_image = (ImageView) itemView.findViewById(R.id.iv_icon);
                tv_download = (TextView) itemView.findViewById(R.id.tv_download);
                progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
            }
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_row_ad_data, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.setIsRecyclable(false);

            int itemHeight = SharedPrefs.getInt(context, SharedPrefs.ITEM_SIZE);

            int height = (int) (Share.screenWidth * 0.23);
            holder.tv_download.getLayoutParams().width = holder.iv_ad_image.getLayoutParams().height = holder.iv_ad_image.getLayoutParams().width = height;

//            Picasso.with(context)
//                    .load(al_ad_item.get(position).getThumb_image())
//                    .into(new Target() {
//                        @Override
//                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                            holder.iv_ad_image.setImageBitmap(bitmap);
//                        }
//
//                        @Override
//                        public void onBitmapFailed(Drawable errorDrawable) {
//
//                        }
//
//                        @Override
//                        public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//                        }
//                    });


            Glide.with(context)
                    .load(al_ad_item.get(position).getThumb_image())
                    .asBitmap().into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    Log.e("bmp size", " Height => " + resource.getHeight() + " Width => " + resource.getWidth());

                    holder.iv_ad_image.setImageBitmap(resource);
                    holder.progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onLoadFailed(Exception e, Drawable errorDrawable){
                    super.onLoadFailed(e, errorDrawable);
                }
            });

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(al_ad_item.get(position).getApp_link())));
                }
            });
        }

        @Override
        public int getItemCount() {
            return al_ad_item.size();
        }
    }

}