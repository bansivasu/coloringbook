package com.coloring.book.mandala.forme.art.Adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.coloring.book.mandala.forme.art.Model.ImageObject;
import com.coloring.book.mandala.forme.art.R;
import com.coloring.book.mandala.forme.art.share.DisplayMetricsHandler;
import com.coloring.book.mandala.forme.art.share.Share;

import java.util.ArrayList;
import java.util.List;

public class HomeAdapter extends Adapter<ViewHolder> {
    private Integer[] al_commons = new Integer[]{};
    private Context mContext;
    private OnItemClickListener mOnItemClickListener;
    private ArrayList<String> pathlists = new ArrayList<>();

    public class MenuItemViewHolder extends ViewHolder {
        public ImageView iv_full_img;
        private ProgressBar progressBar1;

        public MenuItemViewHolder(View itemView) {
            super(itemView);
            this.iv_full_img = (ImageView) itemView.findViewById(R.id.iv_full_image);
            progressBar1 = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int i);
    }

    public HomeAdapter(Context context, Integer[] commons, ArrayList<String> list, OnItemClickListener onItemClickListener) {
        this.mContext = context;
        this.al_commons = commons;
        pathlists = list;
        this.mOnItemClickListener = onItemClickListener;
    //    sd = new SectorsDAO(mContext, DataBaseHelper.SECTORS.SECTORS_PHIL);
    }

    public int getItemViewType(int position) {
        return position;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MenuItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false));
    }

    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Log.e("TAG", "onBindViewHolder==>");
        holder.setIsRecyclable(false);
        MenuItemViewHolder holder1 = (MenuItemViewHolder) holder;

       // ImageObject.dataobj item = (ImageObject.dataobj) this.al_commons.get(position);

        String path = null;
        //((MenuItemViewHolder) holder).progressBar1.getIndeterminateDrawable().setColorFilter(mContext.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        if (Share.getFilePath(mContext, "1", String.valueOf(position+1)) != null) {
            Uri uri = Uri.parse(Share.getFilePath(mContext, "1", String.valueOf(position+1)));
            path = uri.getPath();
        }

        if (path != null) {
           /* Picasso.with(this.mContext).load(pathlists.get(position))
                    .skipMemoryCache()
                    .resize(50,50)
                    .centerCrop()
                    .into(holder1.iv_full_img, new Callback(){
                        @Override
                        public void onSuccess() {
                            ((MenuItemViewHolder) holder).progressBar1.setVisibility(View.GONE);

                        }

                        @Override
                        public void onError() {

                        }
                    });*/

            Glide.with(this.mContext).load(path)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .override(300,300)
                    .placeholder(R.drawable.progress_animation)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(holder1.iv_full_img);

        } else {
           /* Picasso.with(this.mContext).load(item.thumb_image)
                    .skipMemoryCache()
                    .resize(50,50)
                    .centerCrop()
                    .into(holder1.iv_full_img, new Callback(){
                        @Override
                        public void onSuccess() {
                            ((MenuItemViewHolder) holder).progressBar1.setVisibility(View.GONE);

                        }

                        @Override
                        public void onError() {

                        }
                    });*/
            Glide.with(this.mContext)
                    .load(al_commons[position])
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .override(300,300)
                    .placeholder(R.drawable.progress_animation)
                    .into(holder1.iv_full_img);
        }
        holder1.iv_full_img.getLayoutParams().height = (int) (DisplayMetricsHandler.getScreenWidth() * 0.45);

        holder1.itemView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemClickListener.onItemClick(view, position);
            }
        });
    }

    public int getItemCount() {
        Log.e("TAG", "Home Adpater array size:==>" + this.al_commons.length);
        return this.al_commons.length;
    }
}
