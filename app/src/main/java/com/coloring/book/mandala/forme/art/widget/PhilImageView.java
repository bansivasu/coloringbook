package com.coloring.book.mandala.forme.art.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Environment;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.coloring.book.mandala.forme.art.MainActivity;
import com.coloring.book.mandala.forme.art.Model.UndoObj;
import com.coloring.book.mandala.forme.art.R;
import com.coloring.book.mandala.forme.art.db.DataBaseHelper;
import com.coloring.book.mandala.forme.art.db.SectorsDAO;
import com.coloring.book.mandala.forme.art.share.Share;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by buz on 17.06.16.
 * Central image
 */
public class PhilImageView extends VectorImageView implements PhotoViewAttacher.OnPhotoTapListener, VectorImageView.OnImageCallbackListener, PhotoViewAttacher.OnMatrixChangedListener {

    private PhotoViewAttacher photoViewAttacher;
    private PhilImageView philImageView;

    ImageView iv_save, iv_undo, iv_redo, iv_reset;

    private Context mContext;

    private int curSector = -1;
    private Paint paint;
    public static int curColor = 0;

    private int prevColor = -1, selectorcolor = -1;

    private Matrix curMatrix;

    public PhilImageView(Context context) {
        super(context);
        this.mContext = context;
        initThis();
    }

    public PhilImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initThis();
    }

    public PhilImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initThis();
    }

    public void cleanup() {
        if (photoViewAttacher != null)
            photoViewAttacher.cleanup();
    }

    @Override
    public void loadAsset(InputStream string, ImageView iv_save, ImageView iv_redo, ImageView iv_undo, ImageView iv_reset, LinearLayout ll_main_click, LinearLayout ll_main_progress, PhilImageView centerImageView, ProgressBar progress) {
        super.loadAsset(string, iv_save, iv_redo, iv_undo, iv_reset, ll_main_click, ll_main_progress, centerImageView, progress);

        this.iv_save = iv_save;
        this.iv_redo = iv_redo;
        this.iv_undo = iv_undo;
        this.iv_reset = iv_reset;
        setSectorsDAO(new SectorsDAO(mContext, DataBaseHelper.SECTORS.SECTORS_PHIL));

        curMatrix = new Matrix();
        photoViewAttacher = new PhotoViewAttacher(philImageView);
        photoViewAttacher.getDisplayMatrix(curMatrix);
        photoViewAttacher.setMaximumScale(18);
        photoViewAttacher.setMediumScale(6);
        photoViewAttacher.setOnPhotoTapListener(philImageView);
        photoViewAttacher.setOnMatrixChangeListener(philImageView);

        paint = new Paint();
        paint.setStrokeWidth(1);
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);
    }

    @Override
    public void loadAsset(String string, ImageView iv_save, ImageView iv_redo, ImageView iv_undo, ImageView iv_reset, LinearLayout ll_main_click, LinearLayout ll_main_progress, PhilImageView centerImageView, ProgressBar progress) {
        super.loadAsset(string, iv_save, iv_redo, iv_undo, iv_reset, ll_main_click, ll_main_progress, centerImageView, progress);

        this.iv_save = iv_save;
        this.iv_redo = iv_redo;
        this.iv_undo = iv_undo;
        this.iv_reset = iv_reset;
        setSectorsDAO(new SectorsDAO(mContext, DataBaseHelper.SECTORS.SECTORS_PHIL));

        curMatrix = new Matrix();
        photoViewAttacher = new PhotoViewAttacher(philImageView);
        photoViewAttacher.getDisplayMatrix(curMatrix);
        photoViewAttacher.setMaximumScale(18);
        photoViewAttacher.setMediumScale(6);
        photoViewAttacher.setOnPhotoTapListener(philImageView);
        photoViewAttacher.setOnMatrixChangeListener(philImageView);

        paint = new Paint();
        paint.setStrokeWidth(1);
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);
    }

    @Override
    public void initThis() {
        philImageView = this;
    }


    @Override
    public void onPhotoTap(View view, float x, float y) {

        MainActivity.active_flag = true;
        if (Share.redoItems.size() > 0) {
            Share.redoItems.clear();
        }
        curSector = getSector(x, y);
        prevColor = getColorFromSector(curSector);

        if (curSector != 0xFFFFFFFF && prevColor != -16777216) {
            // curColor = Color.RED;

            if (prevColor != 0xFFFFFFFF && prevColor == Share.currentcolor) {
                curColor = Color.WHITE;
            } else {
//                curColor = MainActivity.color_picker.getSelectedColor();
                curColor = Share.currentcolor;
            }

            setSectorColor(curSector, curColor);
            updatePicture();
            philImageView.invalidate();
            if (iscolorAllWhite() == 1)
            {
                this.iv_save.setEnabled(false);
                this.iv_save.setImageDrawable(getResources().getDrawable(R.drawable.ic_save_disable));
            }
            else {
                this.iv_save.setEnabled(true);
                this.iv_save.setImageDrawable(getResources().getDrawable(R.drawable.ic_save));
            }
            UndoObj undo_obj = new UndoObj();
            undo_obj.setSector(curSector);
            undo_obj.setPrev_color(prevColor);
            undo_obj.setFill_color(curColor);
            Share.undoItems.add(undo_obj);
            Share.AllFilledData.add(undo_obj);
            this.iv_reset.setEnabled(true);
            this.iv_reset.setImageDrawable(getResources().getDrawable(R.drawable.ic_refresh));
            if (Share.undoItems.size() > 0) {
                this.iv_undo.setEnabled(true);
                this.iv_undo.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_left));
            }
            if (Share.redoItems.size() > 0) {
                this.iv_redo.setEnabled(true);
                this.iv_redo.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_right));
            }
        }
    }

    @Override
    public void onOutsidePhotoTap() {
    }

    public void undoColor() {
        if (prevColor != -1 && prevColor != curSector) {
            setSectorColor(curSector, prevColor);
            updatePicture();
        }
    }


    public Uri doShare() {
        Uri uri = null;
        try {

            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File bmpFile = File.createTempFile("BigPhil", ".png", path);
            FileOutputStream out = new FileOutputStream(bmpFile);

            Bitmap bmp = getShareBitmap(philImageView.getDrawable());

            bmp.compress(Bitmap.CompressFormat.PNG, 100, out);

            out.close();
            uri = Uri.fromFile(bmpFile);
            Toast.makeText(mContext, String.format("Extracted into: %s", bmpFile.getAbsolutePath()), Toast.LENGTH_LONG).show();

        } catch (Throwable t) {
            t.printStackTrace();
            Toast.makeText(mContext, "Error occured while extracting bitmap", Toast.LENGTH_SHORT).show();
        }
        return uri;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (paint != null) {
            paint.setColor(curColor);
            canvas.drawRect(
                    0, 0,
                    philImageView.getMeasuredWidth() - 1,
                    philImageView.getMeasuredHeight() - 1,
                    paint
            );
        }
    }

    @Override
    public void imageCallback() {
        photoViewAttacher.update();
        // curColor = getOnImageCommandsListener().getCurrentColor();
    }

    @Override
    public void onMatrixChanged(RectF rect) {
        photoViewAttacher.getDisplayMatrix(curMatrix);
    }
}