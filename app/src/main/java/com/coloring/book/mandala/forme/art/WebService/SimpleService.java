package com.coloring.book.mandala.forme.art.WebService;


import com.coloring.book.mandala.forme.art.share.Urls;

import java.io.IOException;

import retrofit.RestAdapter;

public final class SimpleService {

    private static Urls.urls SIMPLE_SERVICE;

    static {
        try {
            main();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Urls.urls get() {
        return SIMPLE_SERVICE;
    }

    public static Urls.urls post() {
        return SIMPLE_SERVICE;
    }

    public static void main(String... args) throws IOException {

        RestAdapter restAdapter;

        restAdapter = new RestAdapter.Builder()

                .setEndpoint(Urls.BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        SIMPLE_SERVICE = restAdapter.create(Urls.urls.class);

    }
}
