# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\software\New_android_setup\android-sdk_r10-windows\android-sdk-windows/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html
# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-dontskipnonpubliclibraryclasses
-dontobfuscate
-forceprocessing
-optimizationpasses 5

-keep class * extends android.app.Activity
-assumenosideeffects class android.util.Log {
    public static *** d(...);
    public static *** v(...);
}
-keep class org.apache.http.** { *;}
-keep class retrofit.** { *;}
-keep class  com.squareup.picasso.OkHttpDownloader.** { *;}
# Add any project specific keep options here:

#-dontwarn org.apache.http.**
-dontwarn org.apache.http.**
-dontwarn retrofit.**
-dontwarn com.squareup.picasso.**
-dontwarn org.apache.http.**

#For StartApp ad
-keep class com.startapp.** {
*;
}
-keepattributes Exceptions, InnerClasses, Signature, Deprecated, SourceFile, LineNumberTable, *Annotation*, EnclosingMethod
-dontwarn android.webkit.JavascriptInterface
-dontwarn com.startapp.**
#For StartApp ad