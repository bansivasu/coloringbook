package com.coloring.book.mandala.forme.art.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataBaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "regions-db";
    private static final int DATABASE_VERSION = 1;

    public static final String ID_ROW = "id";
    public static final String KEY_MAP = "map";
    public static final String COLOR_COLUMN = "color";

    public static final String TABLE_SAVE_IMAGES = "tbl_save_img";
    public static final String TABLE_PREF_DATA = "tbl_preferences";

    public static final String IMAGE_PATH = "image_path";
    public static final String ALL_DATA = "all_data";
    public static final String IMAGE_ID = "image_id";
    public static final String APP_ID = "app_id";
    public static final String IS_SAVE = "is_saved";
    public static final String IS_FAV = "is_fav";
    public static final String IMAGE_URL = "image_url";
    public static final String THUMB_URL = "thumb_url";
    public static final String IS_DATETIME= "created_at";


    public enum TABLES {
        SECTORS_TABLE("sectors");

        private final String str;

        TABLES(String str) {
            this.str = str;
        }

//        @org.jetbrains.annotations.Contract(pure = true)
        @Override
        public String toString() {
            return str;
        }
    }

    public enum SECTORS {
        SECTORS_BRUSH(0),
        SECTORS_PHIL(1);

        private final int i;

        SECTORS(int i) {
            this.i = i;
        }

        @Override
        public String toString() {
            return "" + i;
        }
    }

    public static final String CREATE_SECTORS_TABLE = "CREATE TABLE "
            + TABLES.SECTORS_TABLE + "(" + ID_ROW + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + KEY_MAP + " INTEGER, "
            + COLOR_COLUMN + " TEXT"
            + ")";

    public static final String CREATE_PREF_TABLE = "CREATE TABLE "
            + TABLE_PREF_DATA + "("
            + ID_ROW + " TEXT, "
            + ALL_DATA + " TEXT"
            + ")";

    public static final String CREATE_SAVEDATA_TABLE = "CREATE TABLE "
            + TABLE_SAVE_IMAGES + "(" + IMAGE_PATH + " TEXT, "+ IMAGE_ID + " TEXT, "
            + APP_ID + " TEXT, "+ IS_SAVE + " BOOLEAN, "
            + IS_FAV + " BOOLEAN, "+ ALL_DATA + " TEXT, "+ IMAGE_URL + " TEXT, "+ THUMB_URL + " TEXT, "
            + IS_DATETIME+" DATETIME, "+"Title TEXT)";

    private static DataBaseHelper instance;

    public static synchronized DataBaseHelper getHelper(Context context) {
        if (instance == null)
            instance = new DataBaseHelper(context);
        return instance;
    }

    private DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.e("create tables","-----------");
        sqLiteDatabase.execSQL(CREATE_SECTORS_TABLE);
        sqLiteDatabase.execSQL(CREATE_SAVEDATA_TABLE);
        sqLiteDatabase.execSQL(CREATE_PREF_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
