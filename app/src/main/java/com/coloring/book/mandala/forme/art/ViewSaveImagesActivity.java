package com.coloring.book.mandala.forme.art;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.coloring.book.mandala.forme.art.share.NetworkManager;
import com.coloring.book.mandala.forme.art.share.Share;
import com.coloring.book.mandala.forme.art.widget.ExtendedViewPager;
import com.coloring.book.mandala.forme.art.widget.TouchImageView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;

public class ViewSaveImagesActivity extends BaseActivity implements View.OnClickListener {

    //widgets
    public static ExtendedViewPager mViewPager;
    private ImageView iv_share_image, iv_facebook, iv_instagram, iv_whatsapp, iv_email;
    private TextView tv_title;
    private LinearLayout ll_no_photos;
    private RelativeLayout rll_pager;

    //variables
    private ImageView iv_item_del;
    private TouchImageAdapter imageAdapter;
    private AdView mAdView;
    private ArrayList<File> fav_list = new ArrayList<>();
    private Boolean flag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_save_images);

        mViewPager = (ExtendedViewPager) findViewById(R.id.viewpager);
        tv_title = (TextView) findViewById(R.id.tv_title);
        ll_no_photos = (LinearLayout) findViewById(R.id.ll_no_photos);
        rll_pager = (RelativeLayout) findViewById(R.id.rll_pager);

        setToolbar();
        setListner();
        // setAdView();

        getData();
        Log.e("fav_list","fav_list"+fav_list.size());
        imageAdapter = new TouchImageAdapter(this, fav_list);
        mViewPager.setAdapter(imageAdapter);
        mViewPager.setCurrentItem(Share.selectGalleryImgPos);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                try {
                    tv_title.setText("" + String.format("%02d", (position + 1)) + "/" + String.format("%02d", imageAdapter.getCount()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        flag = true;
    }

    private void setListner() {
        iv_share_image.setOnClickListener(this);
        iv_facebook.setOnClickListener(this);
        iv_instagram.setOnClickListener(this);
        iv_email.setOnClickListener(this);
        iv_whatsapp.setOnClickListener(this);
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        iv_share_image = (ImageView) findViewById(R.id.iv_share_image);
        iv_whatsapp = (ImageView) findViewById(R.id.iv_whatsapp);
        iv_instagram = (ImageView) findViewById(R.id.iv_instagram);
        iv_facebook = (ImageView) findViewById(R.id.iv_facebook);
        iv_email = (ImageView) findViewById(R.id.iv_email);
        toolbar.setTitleTextColor(Color.WHITE);
        tv_title = (TextView) toolbar.findViewById(R.id.tv_title);
        iv_item_del = (ImageView) toolbar.findViewById(R.id.iv_item_del);

        toolbar.findViewById(R.id.iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        iv_item_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    AlertDialog.Builder alert = new AlertDialog.Builder(ViewSaveImagesActivity.this);
                    alert.setMessage(getResources().getString(R.string.msg_delete));
                    alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();

                            Share.selectGalleryImgPos = mViewPager.getCurrentItem();
                            if (fav_list.size() > 0) {
                                new File(fav_list.get(Share.selectGalleryImgPos).getPath()).delete();
                                fav_list.remove(mViewPager.getCurrentItem());
                            }
                            //TODO : added
                            if (fav_list.size() == 0) {
//                                finish();
                                iv_item_del.setEnabled(false);
                                iv_item_del.setImageDrawable(getResources().getDrawable(R.drawable.ic_delete_desable));
                                tv_title.setText("Photos");
                                ll_no_photos.setVisibility(View.VISIBLE);
                                rll_pager.setVisibility(View.GONE);
                            } else {
                                if (fav_list.size() > 0) {
                                    /*imageAdapter = new TouchImageAdapter(ViewSaveImagesActivity.this, fav_list);
                                    mViewPager.removeAllViews();
                                    mViewPager.setAdapter(imageAdapter);*/
                                    if (imageAdapter != null)
                                    imageAdapter.notifyDataSetChanged();
                                    mViewPager.setCurrentItem(Share.selectGalleryImgPos);
                                }
                            }
                        }
                    });
                    alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });
                    alert.show();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        setSupportActionBar(toolbar);
    }

    private void setAdView() {
        try {
            mAdView = (AdView) findViewById(R.id.adView);
            if (NetworkManager.isInternetConnected(getApplicationContext())) {
                //TODO : Ads
                AdRequest adRequest = new AdRequest.Builder()
                        .addTestDevice(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID))
                        .addTestDevice("E19949FB5E7C5A28C30A875934AC8181") //SWIPE
                        .addTestDevice("41E9C9F5D1F985FB36C9760EFC8F3916") //Lenovo
                        .addTestDevice("CEB37F370ECF287C057F70BBC902632E")  //Coolpad
                        .addTestDevice("51A49E7B1B359D1999E5C85CE4F54978") //XOLO
                        .addTestDevice("F9EBC1840023CB004A83005514278635")  //MI 6otu (No SIM)
                        .addTestDevice("2442EC754FEF046014B26ACCEEAE9C23") // Micromax New
                        .addTestDevice("413FAED40213710754F4D30AC4F60355")  //INTEX
                        .addTestDevice("A7A19E06342F7D3868ABA7863D707BD7") //Samsung Tab
                        .addTestDevice("F9EB72307322A818A3399B51B00A765D") //LAVA
                        .addTestDevice("0E45853B0874EAA6418891AD964CC470") //X-ZIOX
                        .addTestDevice("3C8E4AA9C3802D60B83603426D16E430") //Celkon
                        .addTestDevice("89FAEE279F58CCC3FA1ABC0904E1FCBE") //Karbonn
                        .addTestDevice("74527FD0DD7B0489CFB68BAED192733D") //Nexus TAB
                        .addTestDevice("26DFCD14E05E54F19609B18400F672A0") //Samsung j2
                        .addTestDevice("E56855A0C493CEF11A7098FE6EA840CB") //Videocon
                        .addTestDevice("390FED1AE343E9FF9D644C4085C3868E") //jivi
                        .addTestDevice("ACFC7B7082B3F3FD4E0AC8E92EA10D53") //MI Tab
                        .addTestDevice("863D8BAE88E209F38FF3C94A0403C776") //Samsung old
                        .addTestDevice("85F8E335F3BBDFA2782587EEA19688D2") //Samsung new
                        .addTestDevice("517048997101BE4535828AC2360582C2") //motorola
                        .addTestDevice("8BB4BCB27396AB8ED222B7F902E13420") //micromax old
                        .addTestDevice("7F3924D7AB776DC5FF2D314CCFEE0EE0")//Gionee
                        .addTestDevice("F7803FE72A2748F6028D87DC36D7C574") //Mi Chhotu JIO
                        .addTestDevice("36FE3612E964A00DC40A18301E964202") // nokia
                        .build();
                mAdView.loadAd(adRequest);
                mAdView.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        mAdView.setVisibility(View.VISIBLE);
                    }
                });
            } else {
                mAdView.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initShareIntent(View view) {
        try {
//            Uri uri = Uri.fromFile(new File(img_path));
           /* if (GlobalData.is_button_click) {
                AccountRedirectActivity.get_url(ViewSaveImagesActivity.this);
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
       /* getData();
        if (fav_list.size() > 0) {
            if (imageAdapter != null)
            {
                imageAdapter.notifyDataSetChanged();
            }
        }*/
    }

    @Override
    public void onClick(View v) {
        if (v == iv_share_image) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
            intent.setType("image/jpeg");
            intent.putExtra(Intent.EXTRA_TEXT, "Make more pics with app link \n https://play.google.com/store/apps/details?id=" + getPackageName());
            if (fav_list.size() > 0) {
                intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(fav_list.get(mViewPager.getCurrentItem()).getPath())));
            }
            startActivity(Intent.createChooser(intent, "Share Picture"));
        } else if (v == iv_facebook) {
            shareInFb();
        } else if (v == iv_email) {
            shareInEmail();
        } else if (v == iv_instagram) {
            shareInInstagram();
        } else if (v == iv_whatsapp) {
            shareInWhatsapp();
        }
    }

    private void shareInFb() {

        if (appInstalledOrNot("com.facebook.katana")) {

            Intent share = new Intent(Intent.ACTION_SEND);
            share.setPackage("com.facebook.katana");
            if (fav_list.size() > 0) {
                share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(fav_list.get(mViewPager.getCurrentItem()).getPath())));
            }
//            share.putExtra(Intent.EXTRA_TEXT, "Make more pics with app link \n https://play.google.com/store/apps/details?id=" + FullScreenImageActivity.this.getPackageName());
            share.setType("image/jpeg");
            startActivity(Intent.createChooser(share, "Share Picture"));
        } else
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.facebook.katana")));
    }

    private void shareInEmail() {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
        if (fav_list.size() > 0) {
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(fav_list.get(mViewPager.getCurrentItem()).getPath())));
        }
        intent.putExtra(Intent.EXTRA_TEXT, "Make more pics with app link \n https://play.google.com/store/apps/details?id=" + getPackageName());
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(Intent.createChooser(intent, "Share Picture"));
        }
    }

    private void shareInInstagram() {

        if (appInstalledOrNot("com.instagram.android")) {

            Intent share = new Intent(Intent.ACTION_SEND);
            share.setPackage("com.instagram.android");
            if (fav_list.size() > 0) {
                share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(fav_list.get(mViewPager.getCurrentItem()).getPath())));
            }
            share.putExtra(Intent.EXTRA_TEXT, "Make more pics with app link \n https://play.google.com/store/apps/details?id=" + getPackageName());
            share.setType("image/jpeg");
            share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(Intent.createChooser(share, "Share Picture"));

        } else {

            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.instagram.android")));
            } catch (Exception e) {
                e.printStackTrace();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.instagram.android")));
            }
        }
    }

    private void shareInWhatsapp() {
        try {
            if (appInstalledOrNot("com.whatsapp")) {
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setPackage("com.whatsapp");
                share.setType("image/jpeg");
                share.putExtra(Intent.EXTRA_TEXT, "Make more pics with app link \n https://play.google.com/store/apps/details?id=" + getPackageName());
                if (fav_list.size() > 0) {
                    share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(fav_list.get(mViewPager.getCurrentItem()).getPath())));
                }
                startActivity(Intent.createChooser(share, "Select"));
            } else
                Toast.makeText(ViewSaveImagesActivity.this, "Whatsapp have not been installed", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    public void getData() {
        File path = new File(Share.IMAGE_PATH, "");
        File[] allFiles;
        fav_list.clear();
        if (path.exists()) {

            allFiles = path.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return (name.endsWith(".jpg") || name.endsWith(".jpeg") || name.endsWith(".png"));
                }
            });
            if (allFiles != null) {
                if (allFiles.length > 0) {
                    for (int i = 0; i < allFiles.length; i++) {
                        fav_list.add(allFiles[i]);
                        Collections.sort(fav_list, Collections.reverseOrder());
                    }
                }
            }
        }
        if (fav_list.size() > 0) {
            iv_item_del.setEnabled(true);
            iv_item_del.setImageDrawable(getResources().getDrawable(R.drawable.ic_delete_big));
            ll_no_photos.setVisibility(View.GONE);
            rll_pager.setVisibility(View.VISIBLE);
        } else {
            iv_item_del.setEnabled(false);
            iv_item_del.setImageDrawable(getResources().getDrawable(R.drawable.ic_delete_desable));
            tv_title.setText("Photos");
            ll_no_photos.setVisibility(View.VISIBLE);
            rll_pager.setVisibility(View.GONE);
        }
    }

    class TouchImageAdapter extends PagerAdapter {

        private Activity activity;
        private ArrayList<File> saveList = new ArrayList<File>();

        private TouchImageAdapter(Activity activity, ArrayList<File> saveList) {
            this.activity = activity;
            this.saveList = saveList;
        }

        @Override
        public int getCount() {
            return saveList.size();
        }

        @Override
        public View instantiateItem(ViewGroup container, int position) {

            TouchImageView img = new TouchImageView(container.getContext());
//            img.setScaleType(ImageView.ScaleType.FIT_XY);
            try {
                Log.e("path---", "path---" + saveList.get(position).getAbsolutePath());

                Glide.with(ViewSaveImagesActivity.this)
                        .load(saveList.get(position))
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .placeholder(R.drawable.progress)
                        .into(img);

                container.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return img;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        System.gc();
//        Runtime.getRuntime().gc();
    }

    @Override
    public void onBackPressed() {
        if (flag) {
            try {
//            imageAdapter = null;
//            fav_list.clear();
//            fav_list = null;

               /* ViewSaveImagesActivity.mViewPager.removeAllViews();
                ViewSaveImagesActivity.mViewPager.removeAllViewsInLayout();
                ViewSaveImagesActivity.mViewPager.destroyDrawingCache();*/
//            ViewSaveImagesActivity.mViewPager = null;
//            System.gc();
            } catch (Exception e) {
                e.printStackTrace();
            }
            finish();
        }
    }
}
