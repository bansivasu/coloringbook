package com.coloring.book.mandala.forme.art.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coloring.book.mandala.forme.art.Model.DrawerItem;
import com.coloring.book.mandala.forme.art.R;
import com.coloring.book.mandala.forme.art.share.Share;

import java.util.List;

public class CustomDrawerAdapter extends ArrayAdapter<DrawerItem> {
    Context context;
    List<DrawerItem> drawerItemList;
    int layoutResID;

    private static class DrawerItemHolder {
        TextView ItemName;
        ImageView icon, iv_arrow;
        LinearLayout ll_main_drawer;

        private DrawerItemHolder() {
        }
    }

    public CustomDrawerAdapter(Context context, int layoutResourceID, List<DrawerItem> listItems) {
        super(context, layoutResourceID, listItems);
        this.context = context;
        drawerItemList = listItems;
        layoutResID = layoutResourceID;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        DrawerItemHolder drawerHolder;
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            drawerHolder = new DrawerItemHolder();
            view = inflater.inflate(layoutResID, parent, false);
            drawerHolder.ItemName = (TextView) view.findViewById(R.id.drawer_itemName);
            drawerHolder.icon = (ImageView) view.findViewById(R.id.drawer_icon);
            drawerHolder.ll_main_drawer = (LinearLayout) view.findViewById(R.id.ll_main_drawer);
            drawerHolder.iv_arrow = (ImageView) view.findViewById(R.id.iv_arrow);
            view.setTag(drawerHolder);
        } else {
            drawerHolder = (DrawerItemHolder) view.getTag();
        }

        DrawerItem dItem = (DrawerItem) drawerItemList.get(position);
        drawerHolder.icon.setImageDrawable(view.getResources().getDrawable(dItem.getImgResID()));
        drawerHolder.ItemName.setText(dItem.getItemName());
        if (Share.SELECTED == position) {
            drawerHolder.ll_main_drawer.setBackgroundColor(context.getResources().getColor(R.color.light_Grey));
            //drawerHolder.imgIcon.setImageResource(navDrawerItems.get(position).getIconwhite());
            drawerHolder.ItemName.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        } else {
            drawerHolder.ll_main_drawer.setBackgroundColor(context.getResources().getColor(R.color.White));
            //drawerHolder.imgIcon.setImageResource(navDrawerItems.get(position).getIconwhite());
            drawerHolder.ItemName.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        }

        return view;
    }
}
