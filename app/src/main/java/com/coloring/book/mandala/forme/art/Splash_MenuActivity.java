package com.coloring.book.mandala.forme.art;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.coloring.book.mandala.forme.art.share.Share;
import com.google.android.gms.ads.AdListener;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

public class Splash_MenuActivity extends Activity implements View.OnClickListener {

    // widgets
    private LinearLayout splash_photos, splash_start, splash_free, splash_more;
    private ImageView iv_logo, iv_ntvads;

    // variable
    private List<String> listPermissionsNeeded = new ArrayList<>();
    public boolean isInForeGround;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_menu_splash);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        initView();
        setCustomNativeAd();
        setListners();

        if (Share.is_more_apps == false) {
            splash_free.setEnabled(false);
            splash_more.setEnabled(false);
            iv_ntvads.setVisibility(View.GONE);

        } else {
            splash_free.setEnabled(true);
            iv_ntvads.setVisibility(View.VISIBLE);
            splash_more.setEnabled(true);
        }
    }

    private void initView() {
        splash_photos = (LinearLayout) findViewById(R.id.splash_photos);
        splash_start = (LinearLayout) findViewById(R.id.splash_start);
        splash_free = (LinearLayout) findViewById(R.id.splash_free);
        splash_more = (LinearLayout) findViewById(R.id.splash_app_center);
        iv_logo = (ImageView) findViewById(R.id.iv_logo);
        iv_ntvads = (ImageView) findViewById(R.id.iv_ntvads);
    }

    private void setCustomNativeAd() {
        Glide.with(Splash_MenuActivity.this).load(Share.ntv_img)
                .into(iv_ntvads);

        iv_ntvads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Share.ntv_inglink));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Splash_MenuActivity.this.startActivity(intent);
            }
        });
    }

    private void setListners() {
        splash_photos.setOnClickListener(this);
        splash_start.setOnClickListener(this);
        splash_free.setOnClickListener(this);
        splash_more.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        Share.SELECTED = 0;
        isInForeGround = true;
        if (ColoringApplication.getInstance() != null && !ColoringApplication.getInstance().isLoaded())
            ColoringApplication.getInstance().LoadAds();
    }

    @Override
    protected void onStop() {
        super.onStop();
        isInForeGround = false;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isInForeGround = false;
       // System.gc();
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        if (intent == null) {
            intent = new Intent();
        }
        super.startActivityForResult(intent, requestCode);
    }

    @Override
    public void onClick(View v) {
        if (v == splash_photos) {
            Share.selectGalleryImgPos = 0;
            if (!ColoringApplication.getInstance().requestNewInterstitial()) {
                Intent go2Mainact = new Intent(Splash_MenuActivity.this, ImageSlideAllActivity.class);
                go2Mainact.putExtra("is_main", "2");
                startActivity(go2Mainact);
            } else {
                ColoringApplication.getInstance().mInterstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();

                            ColoringApplication.getInstance().mInterstitialAd.setAdListener(null);
                            ColoringApplication.getInstance().mInterstitialAd = null;
                            ColoringApplication.getInstance().ins_adRequest = null;
                            ColoringApplication.getInstance().LoadAds();

                            Intent go2Mainact = new Intent(Splash_MenuActivity.this, ImageSlideAllActivity.class);
                        go2Mainact.putExtra("is_main", "2");
                            startActivity(go2Mainact);
                    }

                    @Override
                    public void onAdFailedToLoad(int i) {
                        super.onAdFailedToLoad(i);
                    }

                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                    }
                });
            }
        } else if (v == splash_start) {
            if (!ColoringApplication.getInstance().requestNewInterstitial()) {
                Intent go2Mainact = new Intent(Splash_MenuActivity.this, HomeActivity.class);
                startActivity(go2Mainact);
            } else {
                ColoringApplication.getInstance().mInterstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                           ColoringApplication.getInstance().mInterstitialAd.setAdListener(null);
                            ColoringApplication.getInstance().mInterstitialAd = null;
                            ColoringApplication.getInstance().ins_adRequest = null;
                            ColoringApplication.getInstance().LoadAds();

                            Intent go2Mainact = new Intent(Splash_MenuActivity.this, HomeActivity.class);
                            startActivity(go2Mainact);

                    }

                    @Override
                    public void onAdFailedToLoad(int i) {
                        super.onAdFailedToLoad(i);
                    }

                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                    }
                });
            }
        } else if (v == splash_free) {
            AccountRedirectActivity.get_url(Splash_MenuActivity.this);
        } else if (v == splash_more) {
            if (Share.al_app_center_data.size() > 0 || Share.al_app_center_home_data.size() > 0) {
                Intent go2appcenter = new Intent(Splash_MenuActivity.this, HomePageActivity.class);
                go2appcenter.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(go2appcenter);
                this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (!ColoringApplication.getInstance().requestNewInterstitial()) {
            finish();

        } else {
            ColoringApplication.getInstance().mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                      ColoringApplication.getInstance().mInterstitialAd.setAdListener(null);
                        ColoringApplication.getInstance().mInterstitialAd = null;
                        ColoringApplication.getInstance().ins_adRequest = null;
                        ColoringApplication.getInstance().LoadAds();
                        finish();

                }

                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                }
            });
        }
    }
}
