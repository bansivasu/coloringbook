package com.coloring.book.mandala.forme.art;

import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.coloring.book.mandala.forme.art.Adapter.MyCustomPagerAdapter;
import com.coloring.book.mandala.forme.art.Model.MainStoreObj;
import com.coloring.book.mandala.forme.art.Model.SVG_obj;
import com.coloring.book.mandala.forme.art.Model.UndoObj;
import com.coloring.book.mandala.forme.art.db.DataBaseHelper;
import com.coloring.book.mandala.forme.art.db.SectorsDAO;
import com.coloring.book.mandala.forme.art.share.DisplayMetricsHandler;
import com.coloring.book.mandala.forme.art.share.Share;
import com.coloring.book.mandala.forme.art.widget.BrushImageView;
import com.coloring.book.mandala.forme.art.widget.PhilImageView;
import com.coloring.book.mandala.forme.art.widget.VectorImageView;
import com.desmond.asyncmanager.AsyncManager;
import com.desmond.asyncmanager.BackgroundTask;
import com.desmond.asyncmanager.BackgroundTask;
import com.desmond.asyncmanager.TaskRunnable;
import com.google.android.gms.ads.AdListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    public ImageView iv_save, iv_reset, iv_undo, iv_redo, iv_first_color, iv_second_color, iv_third_color;
    public LinearLayout ll_main_progress, ll_main_click;
    public PhilImageView centerImageView;
    public ProgressBar progress;
    public static Boolean active_flag = false;
    public ToggleButton toggle_fav;
//    public static Bitmap bitmap;

    private BrushImageView brushImageView;
    private ImageView iv_open_menu, iv_back, image;
    private ViewPager viewPager;
    private MyCustomPagerAdapter myCustomPagerAdapter;
    private Boolean click_flag = false, is_saved = false, isInForeGround, is_touched = false, is_from_fav = false;
    private SectorsDAO sd;
    private String file_path, image_url, thumb_url, json = "";
    private InputStream inputStream;
    private LinearLayout ll_retry;
    private LinearLayout toolbar;
    private TextView tv_title;
    private Bitmap bm;
    private AsyncTask httptask;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Share.screenHeight > 2000 && Share.screenWidth > 1500) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        setContentView(R.layout.activity_main);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Log.e("appid", "imageid" + Share.app_id + "///" + Share.image_id);
        active_flag = false;
        findViews();
        setListners();
        InitViewAction();
    }

    private void findViews() {
        Log.e("screen", "screen" + Share.screenHeight + "//" + Share.screenWidth);

        iv_redo = (ImageView) findViewById(R.id.iv_redo);
        iv_undo = (ImageView) findViewById(R.id.iv_undo);
        iv_first_color = (ImageView) findViewById(R.id.iv_first_color);
        iv_second_color = (ImageView) findViewById(R.id.iv_second_color);
        iv_third_color = (ImageView) findViewById(R.id.iv_third_color);
        iv_reset = (ImageView) findViewById(R.id.iv_reset);
        iv_open_menu = (ImageView) findViewById(R.id.iv_open_menu);
        centerImageView = (PhilImageView) findViewById(R.id.imageView_center);
        progress = (ProgressBar) findViewById(R.id.progress);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        ll_main_progress = (LinearLayout) findViewById(R.id.ll_main_progress);
        ll_retry = (LinearLayout) findViewById(R.id.ll_retry);
        toggle_fav = (ToggleButton) findViewById(R.id.toggle_fav);
        iv_save = (ImageView) findViewById(R.id.iv_save);
        toolbar = (LinearLayout) findViewById(R.id.toolbar);
        ll_main_click = (LinearLayout) findViewById(R.id.ll_main_click);
        tv_title = (TextView) findViewById(R.id.tv_title);
        iv_save.setEnabled(false);
        UndoRedoenable(false, "reset");
    }

    private void setListners() {

        iv_redo.setOnClickListener(this);
        iv_undo.setOnClickListener(this);
        iv_open_menu.setOnClickListener(this);
        iv_reset.setOnClickListener(this);
        iv_first_color.setOnClickListener(this);
        iv_second_color.setOnClickListener(this);
        iv_third_color.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        iv_save.setOnClickListener(this);
        ll_retry.setOnClickListener(this);
        ll_main_click.setOnClickListener(this);
    }

    private void InitViewAction() {

        if (Share.screenHeight > 2000 && Share.screenWidth > 1500) {
            toolbar.getLayoutParams().height = (int) (Share.screenHeight * 0.1);
            iv_back.getLayoutParams().width = (int) (Share.screenHeight * 0.06);
            iv_back.getLayoutParams().height = (int) (Share.screenHeight * 0.06);
        }

        Share.currentcolor = Share.getBackgroundColor(iv_first_color);

        ViewTreeObserver vto = iv_save.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                iv_save.getViewTreeObserver().removeOnPreDrawListener(this);
                toggle_fav.getLayoutParams().height = iv_save.getMeasuredHeight();
                toggle_fav.getLayoutParams().width = iv_save.getMeasuredWidth();
                return true;
            }
        });

        // Initialize database
        sd = new SectorsDAO(getApplicationContext(), DataBaseHelper.SECTORS.SECTORS_PHIL);

        // get data from bundle
        Intent i = getIntent();
        if (i != null) {
            image_url = i.getStringExtra("imageurl");
            thumb_url = i.getStringExtra("thumburl");
            Log.e("url","url"+thumb_url);
            is_from_fav = i.getBooleanExtra("is_fav", false);
            String title = i.getStringExtra("title");
            // tv_title.setText(title);
        }
       // tv_title.setText(Share.title);

        // Toggle button listner
        if (sd.is_favourite(Share.app_id, Share.image_id)) {
            toggle_fav.setChecked(true);
        } else {
            toggle_fav.setChecked(false);
        }

        // Setup viewpager for colorpicker.
        viewPager.getLayoutParams().height = (int) (DisplayMetricsHandler.getScreenWidth() * 0.4);
        viewPager.getLayoutParams().width = (int) (DisplayMetricsHandler.getScreenWidth());

        int images[] = {R.drawable.colorone, R.drawable.colortwo, R.drawable.colorthree, R.drawable.colorfour,
                R.drawable.colorfour, R.drawable.colorsix, R.drawable.colorseven};
        myCustomPagerAdapter = new MyCustomPagerAdapter(MainActivity.this, images, iv_first_color, iv_second_color, iv_third_color);
        viewPager.setAdapter(myCustomPagerAdapter);
        viewPager.setCurrentItem(0);
        inputStream = null;

        viewPager.setOnTouchListener(new OnTouchListener() {
            private float pointX;
            private float pointY;
            private int tolerance = 30;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        return false; //This is important, if you return TRUE the action of swipe will not take place.
                    case MotionEvent.ACTION_DOWN:
                        pointX = event.getX();
                        pointY = event.getY();
                        is_touched = false;
                        break;
                    case MotionEvent.ACTION_UP:
                        boolean sameX = pointX + tolerance > event.getX() && pointX - tolerance < event.getX();
                        boolean sameY = pointY + tolerance > event.getY() && pointY - tolerance < event.getY();
                        if (sameX && sameY) {
                            is_touched = true;
                        } else {
                            is_touched = false;
                        }
                }
                return false;
            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                View v = viewPager.findViewWithTag("myview" + position);
                if (v != null) {
                    image = (ImageView) v.findViewById(R.id.iv_color_picker);
                    image.setDrawingCacheEnabled(true);
                    image.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);

//                    bitmap = ((BitmapDrawable) image.getDrawable()).getBitmap();
                    image.setOnTouchListener(new OnTouchListener() {

                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            switch (event.getActionMasked()) {
                                case MotionEvent.ACTION_DOWN:
                                    if (is_touched) {
                                        active_flag = true;
                                        int x = (int) event.getX();
                                        int y = (int) event.getY();

                                        final Bitmap drawingCache = image.getDrawingCache();
                                        int color = Share.getPixelAtPoint(drawingCache, x, y);
                                        if (color != Color.TRANSPARENT && color != Color.WHITE  && color != Color.BLACK) {
                                            Share.thirdcolor = Share.secondcolor;
                                            Share.secondcolor = Share.currentcolor;
                                            Share.currentcolor = Share.firstcolor = color;
                                            iv_third_color.setBackgroundColor(Share.thirdcolor);
                                            iv_second_color.setBackgroundColor(Share.secondcolor);
                                            iv_first_color.setBackgroundColor(color);
                                            iv_first_color.performClick();
                                        }
                                    }
                                    break;
                            }
                            return false;
                        }
                    });
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        /*new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {*/
                Log.d("UI thread", "I am the UI thread"+Share.image_id);
                centerImageView.loadAsset(Share.image_id+".svg", iv_save, iv_redo, iv_undo, iv_reset, ll_main_click, ll_main_progress, centerImageView, progress);
                centerImageView.setOnImageCommandsListener(brushImageView);
                centerImageView.setOnImageCallbackListener(centerImageView);
            /*}
        });*/

        // Load Svg file
        //httptask = new HttpImageRequestTask().execute();
    }

    private void setViewpagerData(String item) {

        View v = viewPager.findViewWithTag(item);
        if (v == null)
            v = viewPager.getChildAt(0);
        if (v != null) {
            image = (ImageView) v.findViewById(R.id.iv_color_picker);
            image.setDrawingCacheEnabled(true);
            image.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);

//            bitmap = ((BitmapDrawable) image.getDrawable()).getBitmap();

            //  final Bitmap bitmap = ((BitmapDrawable) MainActivity.image.getDrawable()).getBitmap();
            image.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    final int action = event.getActionMasked();
                    switch (event.getActionMasked()) {
                        case MotionEvent.ACTION_DOWN:
                            int x = (int) event.getX();
                            int y = (int) event.getY();

                            final Bitmap drawingCache = image.getDrawingCache();
                            int color = Share.getPixelAtPoint(drawingCache, x, y);
                            if (color != Color.TRANSPARENT && color != Color.WHITE) {
                                Share.currentcolor = color;
                                iv_third_color.setBackgroundColor(Share.getBackgroundColor(iv_second_color));
                                iv_second_color.setBackgroundColor(Share.getBackgroundColor(iv_first_color));
                                iv_first_color.setBackgroundColor(color);
                                iv_first_color.performClick();
                            }
                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                    }
                    return false;
                }
            });
        }
    }

    private class HttpImageRequestTask extends AsyncTask<Void, Void, Drawable> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setVisibility(View.VISIBLE);
            ll_main_click.setVisibility(View.VISIBLE);
        }

        @Override
        protected Drawable doInBackground(Void... params) {
            try {
                final URL url = new URL(image_url);
                //              final URL url = new URL("http://vasundharaapps.com/151161238813418.svg");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setConnectTimeout(5000);
                inputStream = null;
                inputStream = urlConnection.getInputStream();
                return null;
            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        centerImageView.clearAll();
                        ll_retry.setVisibility(View.VISIBLE);
                        progress.setVisibility(View.GONE);
                        ll_main_click.setVisibility(View.GONE);
                    }
                });
                cancel(true);
                Log.e("MainActivity", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Drawable drawable) {
            // Update the view

            if (inputStream != null) {
                try {
//                    centerImageView.setEnabled(false);
                    centerImageView.loadAsset(inputStream, iv_save, iv_redo, iv_undo, iv_reset, ll_main_click, ll_main_progress, centerImageView, progress);
                    centerImageView.setOnImageCommandsListener(brushImageView);
                    centerImageView.setOnImageCallbackListener(centerImageView);
                } catch (Exception e) {
                    centerImageView.clearAll();
                    ll_retry.setVisibility(View.VISIBLE);
                    progress.setVisibility(View.GONE);
                    ll_main_click.setVisibility(View.GONE);
                }
            } else {
                ll_retry.setVisibility(View.VISIBLE);
                progress.setVisibility(View.GONE);
                ll_main_click.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Something went to wrong!", Toast.LENGTH_SHORT);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isInForeGround = true;
        if (!ColoringApplication.getInstance().isLoaded())
            ColoringApplication.getInstance().LoadAds();
        if (myCustomPagerAdapter != null) {
            myCustomPagerAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onDestroy() {
        isInForeGround = false;
        centerImageView.cleanup();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        if (this.httptask != null && this.httptask.getStatus() == AsyncTask.Status.RUNNING)
            this.httptask.cancel(true);
        super.onStop();
        isInForeGround = false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public class save_no_data extends AsyncTask<Bitmap, Void, String> {

        @Override
        protected String doInBackground(Bitmap... bitmaps) {
            sd.savePathData(null, Share.image_id, Share.app_id, is_saved, toggle_fav.isChecked(), thumb_url, null, image_url);
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            ll_main_progress.setVisibility(View.GONE);
            ll_main_click.setVisibility(View.GONE);
            iv_back.setEnabled(true);
            if (bm != null && !bm.isRecycled())
                bm.recycle();
            /*if (bitmap != null && !bitmap.isRecycled())
                bitmap.recycle();*/
            Intent intent = new Intent(MainActivity.this, DrawMoreSingleActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("title", tv_title.getText().toString());
            startActivity(intent);
//           finish();
        }

    }

    public class save_data extends AsyncTask<Bitmap, Void, String> {

        Boolean b = false;

        @Override
        protected String doInBackground(Bitmap... bitmaps) {
            //   b = savedataofFile(Share.app_id, Share.image_id, bm);
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.setVisibility(View.GONE);
            iv_save.setEnabled(true);
            iv_back.setEnabled(true);
            ll_main_click.setVisibility(View.GONE);
            ll_main_progress.setVisibility(View.GONE);
            if (b) {
                Toast.makeText(MainActivity.this, "" + getResources().getString(R.string.saved_successfully), Toast.LENGTH_SHORT).show();
            }
            if (is_saved) {
                Share.selectGalleryImgPos = 0;
                if (b) {
                    // if (!ColoringApplication.getInstance().requestNewInterstitial()) {
                    Intent go2save = new Intent(MainActivity.this, ImageSlideAllActivity.class);
                    startActivity(go2save);
                    /*} else {
                        ColoringApplication.getInstance().mInterstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();

                                if (isInForeGround) {
                                    ColoringApplication.getInstance().mInterstitialAd.setAdListener(null);
                                    ColoringApplication.getInstance().mInterstitialAd = null;
                                    ColoringApplication.getInstance().ins_adRequest = null;
                                    ColoringApplication.getInstance().LoadAds();

                                    Intent go2save = new Intent(MainActivity.this, ViewSaveImagesActivity.class);
                                    startActivity(go2save);
                                }
                            }

                            @Override
                            public void onAdFailedToLoad(int i) {
                                super.onAdFailedToLoad(i);
                            }

                            @Override
                            public void onAdLoaded() {
                                super.onAdLoaded();
                            }
                        });
                    }*/
                }
            } else {
               /* if (bitmap != null && !bitmap.isRecycled())
                    bitmap.recycle();*/
                if (bm != null && !bm.isRecycled())
                    bm.recycle();
                Intent intent = new Intent(MainActivity.this, DrawMoreSingleActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("title", tv_title.getText().toString());
                startActivity(intent);
//                finish();
                /*Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("title", HomeActivity.tv_menu_title.getText());
                startActivity(intent);*/
            }

//            System.gc();
//            Runtime.getRuntime().gc();
        }

    }

    private Boolean savedataofFile(final String app_id, final String image_id, Bitmap b, Boolean check) {

        try {
            if (checkAndRequestPermissions(1)) {
               /* Share.AllFilledData.clear();
                Share.AllFilledData.addAll(Share.undoItems);
                MainStoreObj main_obj = new MainStoreObj();
                main_obj.setImage_id(Share.image_id);
                main_obj.setApp_id(Share.app_id);
                main_obj.setZooming_scale(centerImageView.getMatrix());
                main_obj.setUndo_list(Share.undoItems);
                main_obj.setRedo_list(Share.redoItems);
                main_obj.setPath(file_path);
                main_obj.setIs_fav(false);
                main_obj.setIs_saved(false);

                ArrayList<SVG_obj> sub_list = new ArrayList<>();
                for (int i = 0; i < Share.AllFilledData.size(); i++) {

                    SVG_obj sub_obj = new SVG_obj();
                    sub_obj.setSector(Share.AllFilledData.get(i).getSector());
                    sub_obj.setFill_color(Share.AllFilledData.get(i).getFill_color());
                    sub_list.add(sub_obj);
                }
                main_obj.setObject_list(sub_list);

                String json ="";
                try {
                   *//* Gson gson = new Gson();
                    json = gson.toJson(main_obj);*//*
                }
                catch (Exception e)
                {
                    Log.e("exception","exception"+e.getMessage());
                }*/
                file_path = null;
                String str = null;

                if (is_saved) {
                    str = Share.saveToHiddenPhoto(getApplicationContext(), b, app_id, image_id);
                    Uri uri = Uri.parse(str);
                    final String savedpath = uri.getPath();

                    if (savedpath.length() != 0) {
                        file_path = savedpath;
                        str = Share.saveToPhoto(b, app_id, image_id);
                        Uri uri1 = Uri.parse(str);
                        String savedpath1 = uri1.getPath();

                        if (savedpath1.length() != 0) {
                            sd.savePathData(savedpath, image_id, app_id, is_saved, check, thumb_url, json, image_url);
                            return true;
                        }
                    }
                } else {
                    Log.e("-----", "----");
                    str = Share.saveToHiddenPhoto(getApplicationContext(), b, app_id, image_id);
                    Uri uri = Uri.parse(str);
                    final String savedpath = uri.getPath();

                    if (savedpath.length() != 0) {
                        file_path = savedpath;
                        sd.savePathData(savedpath, image_id, app_id, is_saved, toggle_fav.isChecked(), thumb_url, json, image_url);
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static byte[] serializeObject(Object o) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            ObjectOutput out = new ObjectOutputStream(bos);
            out.writeObject(o);
            out.close();

            // Get the bytes of the serialized object
            byte[] buf = bos.toByteArray();

            return buf;
        } catch (IOException ioe) {
            Log.e("serializeObject", "error", ioe);
            return null;
        }
    }

    public static Object deserializeObject(byte[] b) {
        try {
            ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(b));
            Object object = in.readObject();
            in.close();

            return object;
        } catch (ClassNotFoundException cnfe) {
            Log.e("deserializeObject", "class not found error", cnfe);

            return null;
        } catch (IOException ioe) {
            Log.e("deserializeObject", "io error", ioe);

            return null;
        }
    }

    private boolean checkAndRequestPermissions(int code) {

        if (ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE},
                    code);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode ==0)
        {
            iv_back.performClick();
        }

        if (requestCode ==1)
        {
            iv_save.performClick();
        }
    }


    @Override
    public void onClick(View view) {

        if (view == iv_redo) {

            if (Share.redoItems.size() > 0) {
                active_flag = true;
                UndoObj obj = Share.redoItems.get(Share.redoItems.size() - 1);
                int curSector = obj.getSector();
                int prevColor = obj.getFill_color();
                int curColor = obj.getPrev_color();
                Log.e("colllll", "colll" + prevColor);
                if (curSector != 0xFFFFFFFF) {
                    // curColor = Color.RED;
                   /* if (curColor < 0) {
                        curColor = Color.RED;
                    }
                    if (prevColor == curColor) {
                        curColor = Color.WHITE;
                    }*/

                    UndoObj undo_obj = new UndoObj();
                    undo_obj.setSector(curSector);
                    undo_obj.setPrev_color(prevColor);
                    undo_obj.setFill_color(curColor);
                    Share.undoItems.add(undo_obj);
                    UndoRedoenable(true, "undo");
                    if (Share.redoItems.size() == 1) {
                        UndoRedoenable(false, "redo");
                    }
                    centerImageView.setSectorColor(curSector, curColor);
                    Share.redoItems.remove(Share.redoItems.size() - 1);
                    centerImageView.updatePicture();
                    centerImageView.invalidate();
                }
            }
        } else if (view == iv_undo) {

            if (Share.undoItems.size() > 0) {
                active_flag = true;
                UndoObj obj = Share.undoItems.get(Share.undoItems.size() - 1);
                int curSector = obj.getSector();
                int prevColor = obj.getFill_color();
                int curColor = obj.getPrev_color();
                Log.e("colllll", "colll" + prevColor);
                if (curSector != 0xFFFFFFFF) {
                    // curColor = Color.RED;

              /*  if (prevColor != Color.WHITE && prevColor == curColor) {
                    curColor = Color.WHITE;
                }*/

                    UndoObj undo_obj = new UndoObj();
                    undo_obj.setSector(curSector);
                    undo_obj.setPrev_color(prevColor);
                    undo_obj.setFill_color(curColor);
                    Share.redoItems.add(undo_obj);
                    UndoRedoenable(true, "redo");
                    centerImageView.setSectorColor(curSector, curColor);
                    if (Share.undoItems.size() == 1) {
                        UndoRedoenable(false, "undo");
                    }
                    Share.undoItems.remove(Share.undoItems.size() - 1);
                    centerImageView.updatePicture();
                    centerImageView.invalidate();
                }
            }

        } else if (view == iv_reset) {

            final Dialog dialog = new Dialog(MainActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog);

            TextView tv_dialog_title = (TextView) dialog.findViewById(R.id.txt_msg);
            TextView btn_yes = (TextView) dialog.findViewById(R.id.tv_yes);
            TextView btn_no = (TextView) dialog.findViewById(R.id.tv_no);

            tv_dialog_title.setText("Do you really want to clear?");

            btn_yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    active_flag = false;
                    ll_main_progress.setVisibility(View.VISIBLE);
                    ll_main_click.setVisibility(View.VISIBLE);
//                                    MainActivity.centerImageView.setEnabled(false);
                    UndoRedoenable(false, "reset");
                    UndoRedoenable(false, "undo");
                    UndoRedoenable(false, "redo");
                    centerImageView.clearAll();
                    iv_save.setEnabled(false);
                    iv_save.setImageDrawable(getResources().getDrawable(R.drawable.ic_save_disable));

                }
            });

            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();

                }
            });
            dialog.show();
        } else if (view == iv_open_menu) {
            if (progress.getVisibility() == View.GONE) {
                if (!click_flag) {
                    iv_open_menu.setRotation(180);
                    Log.e("if", "if");
                    click_flag = true;
                    ValueAnimator anim = ValueAnimator.ofInt(viewPager.getHeight(), 0);
                    anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        @Override
                        public void onAnimationUpdate(ValueAnimator valueAnimator) {
                            int val = (Integer) valueAnimator.getAnimatedValue();
                            ViewGroup.LayoutParams layoutParams = viewPager.getLayoutParams();
                            layoutParams.height = val;
                            viewPager.setLayoutParams(layoutParams);
                        }
                    });
                    anim.setDuration(500);
                    anim.start();

                } else {
                    iv_open_menu.setRotation(0);
                    Log.e("else", "else");
                    click_flag = false;
                    ValueAnimator anim = ValueAnimator.ofInt(0, (int) (DisplayMetricsHandler.getScreenWidth() * 0.4));
                    anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        @Override
                        public void onAnimationUpdate(ValueAnimator valueAnimator) {
                            int val = (Integer) valueAnimator.getAnimatedValue();
                            ViewGroup.LayoutParams layoutParams = viewPager.getLayoutParams();
                            layoutParams.height = val;
                            viewPager.setLayoutParams(layoutParams);
                        }
                    });
                    anim.setDuration(500);
                    anim.start();
                    viewPager.setVisibility(View.VISIBLE);
                }
            }
        } else if (view == iv_first_color) {
            /*int m = Share.getBackgroundColor(iv_first_color);
            if (m != 0) {*/
                Share.currentcolor = Share.firstcolor;
                iv_first_color.setImageDrawable(getResources().getDrawable(R.drawable.ic_color_slct));
                iv_second_color.setImageDrawable(getResources().getDrawable(R.drawable.ic_color));
                iv_third_color.setImageDrawable(getResources().getDrawable(R.drawable.ic_color));
//            }

        } else if (view == iv_second_color) {
            /*int m = Share.getBackgroundColor(iv_second_color);
            if (m != 0) {*/
                Share.currentcolor = Share.secondcolor;
                iv_second_color.setImageDrawable(getResources().getDrawable(R.drawable.ic_color_slct));
                iv_first_color.setImageDrawable(getResources().getDrawable(R.drawable.ic_color));
                iv_third_color.setImageDrawable(getResources().getDrawable(R.drawable.ic_color));
//            }
        } else if (view == iv_third_color) {
            /*int m = Share.getBackgroundColor(iv_third_color);
            if (m != 0) {*/
                Share.currentcolor = Share.thirdcolor;
                iv_third_color.setImageDrawable(getResources().getDrawable(R.drawable.ic_color_slct));
                iv_second_color.setImageDrawable(getResources().getDrawable(R.drawable.ic_color));
                iv_first_color.setImageDrawable(getResources().getDrawable(R.drawable.ic_color));
//            }
        } else if (view == iv_back) {
            if (checkAndRequestPermissions(0)) {
                try {
                    if (progress != null && progress.getVisibility() == View.GONE) {
                        is_saved = false;
                        if (active_flag) {
                            iv_back.setEnabled(false);
                            final Dialog dialog = new Dialog(MainActivity.this);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setCancelable(false);
                            dialog.setContentView(R.layout.dialog);

                            TextView tv_dialog_title = (TextView) dialog.findViewById(R.id.txt_msg);
                            TextView btn_yes = (TextView) dialog.findViewById(R.id.tv_yes);
                            TextView btn_no = (TextView) dialog.findViewById(R.id.tv_no);

                            tv_dialog_title.setText("Do you want to save this color?");

                            btn_yes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();
                                    ll_main_progress.setVisibility(View.VISIBLE);
                                    ll_main_click.setVisibility(View.VISIBLE);
                                    Share.is_refreshable = true;

                                    bm = centerImageView.getShareBitmap(centerImageView.getDrawable());

                                    new SaveImageTask().execute(toggle_fav.isChecked());
                                 }
                                });

                            btn_no.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();
                                    new NoSaveImageTask().execute(toggle_fav.isChecked());

                                }
                            });
                            dialog.show();
                        } else {
                            ll_main_progress.setVisibility(View.VISIBLE);
                            ll_main_click.setVisibility(View.VISIBLE);
                            iv_back.setEnabled(false);
                            new NoSaveImageTask().execute(toggle_fav.isChecked());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } else if (view == iv_save) {
            Log.e("click","click---->"+ centerImageView.iscolorAllWhite());
            try {
                if (iv_save.isEnabled()) {
                if (centerImageView.iscolorAllWhite() > 1) {
                    int k = 0;
                    if (VectorImageView.sectorsColors != null && VectorImageView.sectorsColors.size() > 0) {
                        for (int i = 0; i < VectorImageView.sectorsColors.size(); i++) {
                            Log.e("color", "color---->" + VectorImageView.sectorsColors.get(i));
                            if (VectorImageView.sectorsColors.get(i) != -1) {
                                k++;
                            }
                        }
                    }
                    if (k > 0) {

                        ll_main_progress.setVisibility(View.VISIBLE);
                        ll_main_click.setVisibility(View.VISIBLE);
                        iv_save.setEnabled(false);
                        is_saved = true;
                        active_flag = false;
                        bm = centerImageView.getShareBitmap(centerImageView.getDrawable());

                        new SaveImageTask().execute(toggle_fav.isChecked());
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(),"Please fill color",Toast.LENGTH_SHORT).show();
                }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (view == ll_retry) {
            ll_retry.setVisibility(View.GONE);
         //   new HttpImageRequestTask().execute();
        }
    }

    private class NoSaveImageTask extends AsyncTask<Boolean, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Boolean... strings) {
            sd.savePathData(null, Share.image_id, Share.app_id, is_saved, strings[0], thumb_url, null, image_url);
            return false;
        }

        protected void onPostExecute(Boolean result) {
            clearUnusedData();
            if (!is_from_fav) {
                /*Intent intent = new Intent(MainActivity.this, DrawMoreSingleActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("title", tv_title.getText().toString());
                startActivity(intent);*/
                finish();
            } else {
                finish();
            }
        }
    }

    private class SaveImageTask extends AsyncTask<Boolean, Void, Boolean> {

        protected void onPostExecute(Boolean result) {

            progress.setVisibility(View.GONE);
            iv_save.setEnabled(true);
            iv_back.setEnabled(true);
            ll_main_click.setVisibility(View.GONE);
            ll_main_progress.setVisibility(View.GONE);
            if (result) {
                Toast.makeText(MainActivity.this, "" + getResources().getString(R.string.saved_successfully), Toast.LENGTH_SHORT).show();
            }

            Share.is_refreshable = true;
            Share.selectGalleryImgPos = 0;
            if (result) {
                if (is_saved) {

                    if (!ColoringApplication.getInstance().requestNewInterstitial()) {
                        Intent go2save = new Intent(MainActivity.this, ImageSlideAllActivity.class);
                        go2save.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        if (!is_from_fav)
                        go2save.putExtra("is_main", "1");
                        clearUnusedData();
                        startActivity(go2save);
                        finish();
                    } else {
                        ColoringApplication.getInstance().mInterstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();

                                if (isInForeGround) {
                                    ColoringApplication.getInstance().mInterstitialAd.setAdListener(null);
                                    ColoringApplication.getInstance().mInterstitialAd = null;
                                    ColoringApplication.getInstance().ins_adRequest = null;
                                    ColoringApplication.getInstance().LoadAds();

                                    Intent go2save = new Intent(MainActivity.this, ImageSlideAllActivity.class);
                                    if (!is_from_fav)
                                    go2save.putExtra("is_main", "1");
                                    go2save.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    clearUnusedData();
                                    startActivity(go2save);
                                    finish();
                                }
                            }

                            @Override
                            public void onAdFailedToLoad(int i) {
                                super.onAdFailedToLoad(i);
                            }

                            @Override
                            public void onAdLoaded() {
                                super.onAdLoaded();
                            }
                        });
                    }
                }
                else {
                    if (!is_from_fav) {
                       /* Intent intent = new Intent(MainActivity.this, DrawMoreSingleActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("title", tv_title.getText().toString());
                        startActivity(intent);*/
                        finish();
                    } else {
                        finish();
                    }
                }
            }
        }

        @Override
        protected Boolean doInBackground(Boolean... strings) {

            Share.AllFilledData.clear();
            Share.AllFilledData.addAll(Share.undoItems);
            MainStoreObj main_obj = new MainStoreObj();
            main_obj.setImage_id(Share.image_id);
            main_obj.setApp_id(Share.app_id);
            main_obj.setZooming_scale(null);
            main_obj.setUndo_list(Share.undoItems);
            main_obj.setRedo_list(Share.redoItems);
            main_obj.setPath("");
            main_obj.setIs_fav(false);
            main_obj.setIs_saved(false);

            ArrayList<SVG_obj> sub_list = new ArrayList<>();
            for (int i = 0; i < Share.AllFilledData.size(); i++) {

                SVG_obj sub_obj = new SVG_obj();
                sub_obj.setSector(Share.AllFilledData.get(i).getSector());
                sub_obj.setFill_color(Share.AllFilledData.get(i).getFill_color());
                sub_list.add(sub_obj);
            }
            main_obj.setObject_list(sub_list);
            Gson gson = new Gson();
            json = gson.toJson(main_obj).toString();
            Share.AllFilledData.clear();

            Boolean b = savedataofFile(Share.app_id, Share.image_id, bm, strings[0]);
            return b;
        }
    }

    private void clearUnusedData() {
        try {
            sd = null;
          /*  MainActivity.iv_save =  MainActivity.iv_reset = MainActivity.iv_redo = MainActivity.iv_undo
                    = MainActivity.iv_first_color = MainActivity.iv_second_color= MainActivity.iv_third_color = image =null;
            MainActivity.ll_main_progress = MainActivity.ll_main_click= null;*/
            centerImageView.cleanup();
//            MainActivity.centerImageView = null;
            /*MainActivity.progress = null;
            MainActivity.active_flag = null;
            MainActivity.toggle_fav = null;*/

            myCustomPagerAdapter = null;
            inputStream = null;
            if (bm != null && !bm.isRecycled())
                bm.recycle();

            viewPager.removeAllViews();
            viewPager.removeAllViewsInLayout();
            viewPager.destroyDrawingCache();
            System.gc();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void UndoRedoenable(Boolean is_enable, String btn) {

        switch (btn) {
            case "reset":
                if (is_enable) {
                    iv_reset.setEnabled(true);
                    iv_reset.setImageDrawable(getResources().getDrawable(R.drawable.ic_refresh));
                } else {
                    iv_reset.setEnabled(false);
                    iv_reset.setImageDrawable(getResources().getDrawable(R.drawable.ic_refresh_disable));

                }
                break;
            case "undo":
                if (is_enable) {
                    iv_undo.setEnabled(true);
                    iv_undo.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_left));
                } else {
                    iv_undo.setEnabled(false);
                    iv_undo.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_left_desable));

                }
                break;
            case "redo":
                if (is_enable) {
                    iv_redo.setEnabled(true);
                    iv_redo.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_right));
                } else {
                    iv_redo.setEnabled(false);
                    iv_redo.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_right_desable));

                }
                break;
        }
    }

    public String saveToPhoto(Bitmap bitmapImage, String app_id, String image_id) {
        try {
            File directory = new File(Share.IMAGE_PATH);
            if (!directory.exists())
                directory.mkdir();
//            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String file_name = "colorimage_" + app_id + "_" + image_id;
            File mypath = new File(directory, file_name + ".jpeg");
            mypath.deleteOnExit();
            mypath.createNewFile();
            Log.e("TAG", "" + mypath);
            // Create imageDir
            if (mypath.exists()) {
                FileOutputStream fos = new FileOutputStream(mypath);
                // Use the compress method on the BitMap object to write image to the OutputStream
                bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.flush();
                fos.close();
            } else {
                Log.e("TAG", "saveToInternalStorage Not Saved  Image------------------------------------------------------->");
            }

            return directory.getPath() + "/" + file_name + ".jpeg";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Bitmap getScreenShot(View view) {
        View screenView = view.getRootView();
        view.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);
        return bitmap;
    }

    @Override
    public void onBackPressed() {
        if (iv_back != null && iv_back.isEnabled())
            iv_back.performClick();
    }
}
