package com.coloring.book.mandala.forme.art;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coloring.book.mandala.forme.art.Adapter.MoreappAdapter;
import com.coloring.book.mandala.forme.art.Model.moreAppObject;
import com.coloring.book.mandala.forme.art.WebService.SimpleService;
import com.coloring.book.mandala.forme.art.share.NetworkManager;
import com.coloring.book.mandala.forme.art.share.Share;
import com.coloring.book.mandala.forme.art.share.SharedPrefs;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
//import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DrawMoreSingleActivity extends AppCompatActivity implements View.OnClickListener {

    // widgets
    Toolbar toolbar_draw_more_single;
    TextView tv_coming;
    private ImageView iv_more_app;
    private LinearLayout ll_no_internet_home, ll_moreapps;
    private RecyclerView recyclerViewHome;
    private TextView tv_retry_home, tv_menu_titleDrawMore;
    private ProgressDialog mProgressDialog;
    private AdView mAdView;

    // variables
    private MoreappAdapter moreappAdapter;
    private Boolean isInForeGround;
    private String title = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_home);

        initViews();
        setListeners();
        init();
        setAdView();
        getAllData();
    }

    private void initViews() {
        iv_more_app = (ImageView) findViewById(R.id.iv_more_app);
        tv_coming = (TextView) findViewById(R.id.tv_coming);
        toolbar_draw_more_single = (Toolbar) findViewById(R.id.toolbar_draw_more_single);
        tv_menu_titleDrawMore = (TextView) toolbar_draw_more_single.findViewById(R.id.tv_menu_titleDrawMore);
        recyclerViewHome = (RecyclerView) findViewById(R.id.recyclerViewHome);
        ll_no_internet_home = (LinearLayout) findViewById(R.id.ll_no_internet_home);
        tv_retry_home = (TextView) findViewById(R.id.tv_retry_home);
        ll_moreapps = (LinearLayout) findViewById(R.id.ll_moreapps);

        recyclerViewHome.setLayoutManager(new LinearLayoutManager(DrawMoreSingleActivity.this));
    }

    private void setListeners() {
        iv_more_app.setOnClickListener(this);
        ll_no_internet_home.setOnClickListener(this);
    }

    private void init() {

        setSupportActionBar(toolbar_draw_more_single);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_header_back);

        Intent intent = getIntent();
        if (intent != null)
            title = intent.getStringExtra("title");

        moreappAdapter = new MoreappAdapter(DrawMoreSingleActivity.this, Share.moreapp_datalist, new MoreappAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int i, final String name) {
//                HomeActivity.tv_menu_title.setText(moreapp_datalist.get(i).getName());
                // if (NetworkManager.isInternetConnected(getActivity())) {

                if (!ColoringApplication.getInstance().requestNewInterstitial()) {

                    tv_menu_titleDrawMore.setText(name);
                    ll_moreapps.setVisibility(View.GONE);
                    ll_no_internet_home.setVisibility(View.GONE);
                    tv_coming.setVisibility(View.VISIBLE);

                } else {
                    ColoringApplication.getInstance().mInterstitialAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdClosed() {
                            super.onAdClosed();

                            ColoringApplication.getInstance().mInterstitialAd.setAdListener(null);
                            ColoringApplication.getInstance().mInterstitialAd = null;
                            ColoringApplication.getInstance().ins_adRequest = null;
                            ColoringApplication.getInstance().LoadAds();

                            tv_menu_titleDrawMore.setText(name);
                            ll_moreapps.setVisibility(View.GONE);
                            ll_no_internet_home.setVisibility(View.GONE);
                            tv_coming.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAdFailedToLoad(int i) {
                            super.onAdFailedToLoad(i);
                        }

                        @Override
                        public void onAdLoaded() {
                            super.onAdLoaded();
                        }
                    });
                }
                /*} else {
                    ll_moreapps.setVisibility(View.GONE);
                    ll_no_internet_home.setVisibility(View.VISIBLE);
                }*/
            }
        });
        recyclerViewHome.setAdapter(moreappAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
           /* case R.id.share:
                initShareIntent();
                break;*/
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getAllData() {
        if (NetworkManager.isInternetConnected(DrawMoreSingleActivity.this)) {
            showProgressDialog();
            LoadData();
        } else {
            String data = SharedPrefs.getString(DrawMoreSingleActivity.this,"offline_data");
            if (Share.moreapp_datalist.size() == 0 && data.equalsIgnoreCase("")) {

                ll_moreapps.setVisibility(View.GONE);
                ll_no_internet_home.setVisibility(View.VISIBLE);
            }
            else {
                Log.e("datalist","datalist"+data);
                Gson maingson = new Gson();
                Type type = new TypeToken<ArrayList<moreAppObject.dataobj>>() {}.getType();
                ArrayList<moreAppObject.dataobj> arrayList = maingson.fromJson(data, type);

                if (arrayList != null && arrayList.size()>0) {
                    Share.moreapp_datalist.addAll(arrayList);
                    if (moreappAdapter != null)
                        moreappAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    private void setAdView() {
        try {
            mAdView = (AdView) findViewById(R.id.adView);
            if (NetworkManager.isInternetConnected(getApplicationContext())) {
                //TODO : Ads
                AdRequest adRequest = new AdRequest.Builder()
                        .addTestDevice(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID))
                        .addTestDevice("67F459F428BA080AC0E5D48751A42AD7")  //sumsung office
                        .addTestDevice("03E2207FBED3E8811B414918D8077E25")
                        .addTestDevice("74527FD0DD7B0489CFB68BAED192733D")
                        .addTestDevice("7D27AE6CC478DBF13BAEFEB9F873562B")
                        .addTestDevice("E65765D74A642A5F0993F9107AE0B307")
                        .addTestDevice("86021572C8EFA2DD0DB69DB2BA2CA050")
                        .addTestDevice("EC0086E4DD57398BD70018389A92BB9A")
                        .addTestDevice("53E4CD6C36D8A29795391C24C02724F8")
                        .addTestDevice("790037035108AEA31323422EBA149D03")
                        .addTestDevice("3A9619098ED320FC729B6ED2972C7536")
                        .addTestDevice("7377159F8453DCC60F4109F19FA52FFE")
                        .addTestDevice("6951A7DFDC016130A7C94F5568794431")
                        .addTestDevice("AE74B0C567C2121A613144D22D3B0554")
                        .addTestDevice("36FE3612E964A00DC40A18301E964202") // nokia

                        .build();
                mAdView.loadAd(adRequest);
                mAdView.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        mAdView.setVisibility(View.VISIBLE);
                    }
                });
            } else {
                mAdView.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // ---- call from another methos ----//

    protected void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(DrawMoreSingleActivity.this, BuildConfig.FLAVOR, getResources().getString(R.string.loading), false, false);
        }
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    protected void hideDialog() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        } catch (IllegalArgumentException e) {
        }
    }

    private void LoadData() {

        if (NetworkManager.isInternetConnected(DrawMoreSingleActivity.this.getApplicationContext())) {
            SimpleService.get().getmoreAppObj(new Callback<moreAppObject>() {
                @Override
                public void success(moreAppObject mainobj, Response response) {
                    Log.e("response", "response" + mainobj.data);
                    ll_moreapps.setVisibility(View.VISIBLE);
                    ll_no_internet_home.setVisibility(View.GONE);
                    if (Share.moreapp_datalist != null) {
                        Share.moreapp_datalist.clear();

                        for (int k = 0; k < mainobj.data.size(); k++) {
                            if (!mainobj.data.get(k).getName().equalsIgnoreCase("Mandala"))
                            Share.moreapp_datalist.add(mainobj.data.get(k));
                        }

                        Gson gson = new Gson();
                        String json = gson.toJson(Share.moreapp_datalist).toString();
                        SharedPrefs.save(DrawMoreSingleActivity.this,"offline_data",json);

                        if (moreappAdapter != null)
                            moreappAdapter.notifyDataSetChanged();
                    }
                    hideDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    hideDialog();
                    Log.e("error", "error" + error);
                    ll_moreapps.setVisibility(View.GONE);
                    ll_no_internet_home.setVisibility(View.VISIBLE);
//                    showToastMsg(activity.getResources().getString(R.string.exception_msg));
                }
            });
        } else {
            ll_moreapps.setVisibility(View.GONE);
            ll_no_internet_home.setVisibility(View.VISIBLE);
            hideDialog();
        }
    }

    @Override
    public void onDestroy() {
        isInForeGround = false;
        super.onDestroy();
       /* System.gc();
        Runtime.getRuntime().gc();*/
    }

    public void onClick(View v) {

        if (v == iv_more_app){
            if (Share.is_more_apps) {
                AccountRedirectActivity.get_url(DrawMoreSingleActivity.this);
            }
        }
        switch (v.getId()) {
            case R.id.ll_no_internet_home /*2131558584*/:
                ll_no_internet_home.setVisibility(View.GONE);
                showProgressDialog();
                LoadData();
            default:
        }
    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
        if (tv_coming.getVisibility() == View.VISIBLE)
        {
            tv_menu_titleDrawMore.setText("Coloring Book");
            tv_coming.setVisibility(View.GONE);
            ll_moreapps.setVisibility(View.VISIBLE);
            ll_no_internet_home.setVisibility(View.GONE);
        }
        else {
            Intent go2home = new Intent(DrawMoreSingleActivity.this, HomeActivity.class);
            go2home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(go2home);
            finish();
        }
    }

    public void onResume() {
        super.onResume();

        Log.e("resume", "resume");
        /*if (Share.is_items.equalsIgnoreCase("0")) {
            if (moreappAdapter != null)
                moreappAdapter.notifyDataSetChanged();
        } else if (Share.is_items.equalsIgnoreCase("mandala1")) {
            if (itemsAdapter != null)
                itemsAdapter.notifyDataSetChanged();
        } else if (Share.is_items.equalsIgnoreCase("mandala2")) {
            if (favAdapter != null)
                favAdapter.notifyDataSetChanged();
        }*/
        isInForeGround = true;
        if (ColoringApplication.getInstance() != null && !ColoringApplication.getInstance().isLoaded())
            ColoringApplication.getInstance().LoadAds();
         /* try {
            showProgressDialog();
            Log.e("data", "data" + Share.is_items);
            if (!Share.is_items) {
                ll_items.setVisibility(View.GONE);
                ll_moreapps.setVisibility(View.VISIBLE);
                ll_no_internet_home.setVisibility(View.GONE);
                LoadData();
            } else {
                ll_items.setVisibility(View.VISIBLE);
                ll_moreapps.setVisibility(View.GONE);
                ll_no_internet_home.setVisibility(View.GONE);
                LoadData_items();
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }*/
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        isInForeGround = false;
    }

}