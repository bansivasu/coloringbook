package com.coloring.book.mandala.forme.art.Model;

import java.util.ArrayList;

/**
 * Created by Bansi on 20-02-2018.
 */

public class NotiModel {
    private ArrayList<AdModel> list = new ArrayList<>();

    public ArrayList<AdModel> getList() {
        return list;
    }

    public void setList(ArrayList<AdModel> list) {
        this.list = list;
    }
}
