package com.coloring.book.mandala.forme.art.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.coloring.book.mandala.forme.art.MainActivity;
import com.coloring.book.mandala.forme.art.Model.MainStoreObj;
import com.coloring.book.mandala.forme.art.Model.SVG_obj;
import com.coloring.book.mandala.forme.art.Model.UndoObj;
import com.coloring.book.mandala.forme.art.R;
import com.coloring.book.mandala.forme.art.db.DataBaseHelper;
import com.coloring.book.mandala.forme.art.db.SectorsDAO;
import com.coloring.book.mandala.forme.art.share.Share;
import com.coloring.book.mandala.forme.art.share.SharedPrefs;
import com.google.gson.Gson;
import com.pixplicity.sharp.OnSvgElementListener;
import com.pixplicity.sharp.Sharp;
import com.pixplicity.sharp.SharpDrawable;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Rius on 29.03.17.
 * VectorImageView class
 */
public abstract class VectorImageView extends AppCompatImageView implements OnSvgElementListener {

    private Context context;
    private LinearLayout ll_main_progress, ll_main_click;
    private PictureDrawable sharpDrawable;
    private ProgressBar progress;

    private VectorImageView vectorImageView;
    private PhilImageView centerImageView;

    private OnImageCommandsListener onImageCommandsListener;
    private OnImageCallbackListener onImageCallbackListener;

    private ImageView iv_save,iv_undo,iv_redo, iv_reset;

    private Bitmap bitmapMap;

    private int actW;
    private int actH;

    private ArrayList<Boolean> sectorsFlags;

    public static ArrayList<Integer> sectorsColors;
    private ArrayList<Integer> sectorsTags;
    private ArrayList<Path> sectorsPaths;

    private ArrayList<Integer> bckgSectorsColors;
    private ArrayList<Path> bckgSectorsPaths;

    private ArrayList<Float> brushSectors;

    private SectorsDAO sd;
    SharedPrefs spref;

    private boolean isEmptyDB = false;

    public VectorImageView(Context context) {
        super(context);
        vectorImageView = this;
        vectorImageView.context = context;
    }

    public VectorImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        vectorImageView = this;
        vectorImageView.context = context;
    }

    public VectorImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        vectorImageView = this;
        vectorImageView.context = context;
    }

    public void loadAsset(String string, ImageView iv_save, ImageView iv_redo, ImageView iv_undo, ImageView iv_reset, LinearLayout ll_main_click, LinearLayout ll_main_progress, PhilImageView centerImageView, ProgressBar progress) {

        this.iv_save = iv_save;
        this.iv_redo = iv_redo;
        this.iv_undo = iv_undo;
        this.iv_reset = iv_reset;
        this.ll_main_click = ll_main_click;
        this.ll_main_progress = ll_main_progress;
        this.centerImageView = centerImageView;
        this.progress = progress;

        sectorsFlags = new ArrayList<>();
        sectorsPaths = new ArrayList<>();
        bckgSectorsPaths = new ArrayList<>();
        bckgSectorsColors = new ArrayList<>();
        brushSectors = new ArrayList<>();
        spref = new SharedPrefs();

        try {
            Sharp mSharp = Sharp.loadAsset(context.getAssets(), string);
            mSharp.setOnElementListener(vectorImageView);

            mSharp.getDrawable(vectorImageView, new Sharp.DrawableCallback() {
                @Override
                public void onDrawableReady(SharpDrawable sd) {
                    sharpDrawable = sd;
                    vectorImageView.setImageDrawable(sharpDrawable);

                    if (onImageCallbackListener != null)
                        onImageCallbackListener.imageCallback();

                    createMap();
                    updatePicture();
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public void loadAsset(String string){

        try {
            Sharp mSharp = Sharp.loadAsset(context.getAssets(), string);
            mSharp.setOnElementListener(vectorImageView);

            mSharp.getDrawable(vectorImageView, new Sharp.DrawableCallback() {
                @Override
                public void onDrawableReady(SharpDrawable sd) {
                    sharpDrawable = sd;
                    vectorImageView.setImageDrawable(sharpDrawable);

                    if (onImageCallbackListener != null)
                        onImageCallbackListener.imageCallback();

                    createMap();
                    updatePicture();
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void loadAsset(InputStream string, ImageView iv_save, ImageView iv_redo, ImageView iv_undo, ImageView iv_reset, LinearLayout ll_main_click, LinearLayout ll_main_progress, PhilImageView centerImageView, ProgressBar progress) {

        this.iv_save = iv_save;
        this.iv_redo = iv_redo;
        this.iv_undo = iv_undo;
        this.iv_reset = iv_reset;
        this.ll_main_click = ll_main_click;
        this.ll_main_progress = ll_main_progress;
        this.centerImageView = centerImageView;
        this.progress = progress;

        sectorsFlags = new ArrayList();
        sectorsPaths = new ArrayList();
        bckgSectorsPaths = new ArrayList();
        bckgSectorsColors = new ArrayList();
        brushSectors = new ArrayList();

        try {
            Log.e("---------------", "-----" + Sharp.loadInputStream(string));
            Sharp mSharp = Sharp.loadInputStream(string);
            Log.e("---------------", "-----" + mSharp);
            mSharp.setOnElementListener(vectorImageView);
            // mSharp.getDrawable(vectorImageView);
            mSharp.getDrawable(vectorImageView, new Sharp.DrawableCallback() {
                @Override
                public void onDrawableReady(SharpDrawable sd) {
                    sharpDrawable = sd;
                    vectorImageView.setImageDrawable(sharpDrawable);

                    if (onImageCallbackListener != null)
                        onImageCallbackListener.imageCallback();

                    createMap();
                    updatePicture();
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private int sectorId = 0, k = 0;

    @Override
    public void onSvgStart(@NonNull Canvas canvas, @Nullable RectF bounds) {
        sectorId = 0;
        k = 0;
        //sectorsColors = sectorsDAO.getSectors();
        sectorsColors = new ArrayList<>();
        sectorsTags = new ArrayList<>();
       /* if (!sectorsColors.isEmpty())
            sectorsColors.clear();*/
        isEmptyDB = true;
        bckgSectorsPaths.clear();
        bckgSectorsColors.clear();
        sectorsPaths.clear();
        brushSectors.clear();
        if (sd == null)
            sd = new SectorsDAO(context, DataBaseHelper.SECTORS.SECTORS_PHIL);
    }

    @Override
    public void onSvgEnd(@NonNull Canvas canvas, @Nullable RectF bounds) {
        if (isEmptyDB) {
            AddSectorsTask task = new AddSectorsTask((Activity) context);
            task.execute((Void) null);
            isEmptyDB = false;
        }
    }

    @Override
    public <T> T onSvgElement(@Nullable String id, @NonNull T element, @Nullable RectF
            elementBounds, @NonNull Canvas canvas, @Nullable RectF canvasBounds, @Nullable Paint paint) {

        if (paint != null && (element instanceof Path)) {
            int color = 0;
            Path p = (Path) element;
            Log.e("path", "path" + p.getClass().getName() + "///" + id);
            sectorsFlags.add(true);
            sectorsPaths.add((Path) element);

            if (onImageCommandsListener == null) {
                float elB = elementBounds != null ? elementBounds.left : -1;
                float canB = canvasBounds != null ? canvasBounds.width() : -1;
                brushSectors.add(elB / canB);
            }

            if (isEmptyDB) {
                k++;
                Log.e("index", "index" + k);

                if (id != null && id.equalsIgnoreCase("XMLID_1_")) {
                    Log.e("-------", "----");
                    color = Color.BLACK;
                } else {
                    color = Color.WHITE;
                }
                sectorsColors.add(color);
            } else {
                int m = sectorId++;
                Log.e("siiiize", "siizeee" + sectorsColors.size());
                if (sectorsColors.size() < m)
                    color = sectorsColors.get(m);
            }
            paint.setColor(color);
        }
        return null;
    }

    @Override
    public <T> void onSvgElementDrawn(@Nullable String id, @NonNull T element, @NonNull Canvas
            canvas, @Nullable Paint paint) {
    }

    int getSector(float x, float y) {
        int lX = Math.round(x * actW);
        int lY = Math.round(y * actH);
        int curSector;
        if (lX >= 0 && lY < bitmapMap.getHeight() && lX < bitmapMap.getWidth() && lY >= 0) {
            curSector = ((bitmapMap.getPixel(lX, lY) << 16) >>> 16) - 1;
            return curSector;
        }
        curSector = 0xFFFFFFFF;
        return curSector;
    }

    int getSector(final ImageView imageView, float x, float y) {

        float paddingEventX = x / imageView.getWidth();

        int sectorId = -1;
        for (float fl : brushSectors) {
            if (paddingEventX < fl) break;
            sectorId++;
        }

        return sectorId;
    }

    public void setSectorColor(int i, int c) {
        if (sectorsColors != null && c != sectorsColors.get(i)) {
            sectorsColors.set(i, c);
            sd.update(i, c);
        }
    }

    int getColorFromSector(int i) {
        if (i == 0xFFFFFFFF) return sectorsColors.get(0);
        return sectorsColors.get(i);
    }

    int getSizeSectors() {
        return sectorsColors.size();
    }

    public void setOnImageCommandsListener(OnImageCommandsListener onImageCommandsListener) {
        vectorImageView.onImageCommandsListener = onImageCommandsListener;
    }

    OnImageCommandsListener getOnImageCommandsListener() {
        return vectorImageView.onImageCommandsListener;
    }

    interface OnImageCommandsListener {
        int getCurrentColor();
    }

    public void setOnImageCallbackListener(OnImageCallbackListener onImageCallbackListener) {
        this.onImageCallbackListener = onImageCallbackListener;
    }

    interface OnImageCallbackListener {
        void imageCallback();
    }

    private void createMap() {

        actW = sharpDrawable.getPicture().getWidth();

        if (onImageCallbackListener != null) {

            actH = sharpDrawable.getPicture().getHeight();

            Paint paint = new Paint();
            paint.setAntiAlias(false);

            Canvas canvas = sharpDrawable.getPicture().beginRecording(actW, actH);

            for (int i = 0; i < sectorsPaths.size(); i++) {
                paint.setColor(i + 1);
                paint.setAlpha(0xFF);
                canvas.drawPath(sectorsPaths.get(i), paint);
            }

            sharpDrawable.getPicture().endRecording();

            bitmapMap = Bitmap.createBitmap(actW, actH, Bitmap.Config.ARGB_8888);
            bitmapMap.eraseColor(0x00000000);

            Canvas bitmapCanvas = new Canvas(bitmapMap);
            sharpDrawable.draw(bitmapCanvas);
        }
    }

    public abstract void initThis();

    private class AddSectorsTask extends AsyncTask<Void, Void, Long> {

        private final WeakReference<Activity> activityWeakRef;


        public AddSectorsTask(Activity context) {
            this.activityWeakRef = new WeakReference<>(context);
        }

        @Override
        protected Long doInBackground(Void... voids) {
            Log.e("======", "=====" + sectorsColors.size());
            if (sd == null)
                sd = new SectorsDAO(context, DataBaseHelper.SECTORS.SECTORS_PHIL);
            long m = sd.init(sectorsColors);
            return m;
        }

        @Override
        protected void onPostExecute(Long aLong) {

            Gson maingson = new Gson();
            /*String mainjson = SharedPrefs.getString(getContext(), "svg_data_" + Share.app_id + "_" +
                    Share.image_id);*/
//            String mainjson = MainActivity.sPref.getString(context,"svg_"+Share.image_id+"_"+Share.app_id, null);

            String mainjson = sd.getAllData("svg_"+Share.image_id+"_"+Share.app_id);
            MainStoreObj StoreObj = maingson.fromJson(mainjson, MainStoreObj.class);

            if (StoreObj != null) {

                Share.undoItems.addAll(StoreObj.getUndo_list());
                Share.redoItems.addAll(StoreObj.getRedo_list());
                for (int i = 0; i < StoreObj.getObject_list().size(); i++) {

                    UndoObj obj = new UndoObj();
                    obj.setSector(StoreObj.getObject_list().get(i).getSector());
                    obj.setFill_color(StoreObj.getObject_list().get(i).getFill_color());
                    Share.AllFilledData.add(obj);
                    obj = null;
                    int curSector = StoreObj.getObject_list().get(i).getSector();

                    if (sectorsColors != null) {
                        sectorsColors.set(curSector, StoreObj.getObject_list().get(i).getFill_color());
                        sd.update(curSector, StoreObj.getObject_list().get(i).getFill_color());
                    }
                    updatePicture();
                    centerImageView.invalidate();
                }
            }

            if (Share.AllFilledData.size() > 0) {
                iv_reset.setEnabled(true);
                iv_reset.setImageDrawable(getResources().getDrawable(R.drawable.ic_refresh));
                if (iscolorAllWhite() > 1) {
                    iv_save.setEnabled(true);
                    iv_save.setImageDrawable(getResources().getDrawable(R.drawable.ic_save));
                }
            }
            if (Share.undoItems.size() > 0) {
                iv_undo.setEnabled(true);
                iv_undo.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_left));
            }
            if (Share.redoItems.size() > 0) {
                iv_redo.setEnabled(true);
                iv_redo.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_right));
            }
//            MainActivity.centerImageView.setEnabled(true);
            progress.setVisibility(View.GONE);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable(){
                @Override
                public void run(){
                    ll_main_click.setVisibility(View.GONE);
                }
            }, 5);
            if (activityWeakRef.get() != null
                    && !activityWeakRef.get().isFinishing()) {
                if (aLong == -1)
                    Log.e("MLogs", "Handsof: Error to save sectorsColors.");
                else Log.d("MLogs", "Handsof: Sectors saved.");
            }
        }
    }

    public Bitmap getShareBitmap(Drawable drawable) {
        int w = getResources().getDimensionPixelSize(R.dimen.share_image_width_px);
        int iw = drawable.getIntrinsicWidth();
        int ih = drawable.getIntrinsicHeight();
        float ar = (float) iw / w;
        int ah = (int) (ih / ar);
        int aw = (int) (iw / ar);

        Bitmap btm = Bitmap.createBitmap(aw, ah, Bitmap.Config.ARGB_8888);
        btm.eraseColor(0xFFFFFFFF);
        Canvas canvas = new Canvas(btm);
        int p = getResources().getDimensionPixelSize(R.dimen.share_image_padding_px);
        drawable.setBounds(p, p, aw - p, ah - p);
        drawable.draw(canvas);
        return btm;
    }

    public void setSectorsDAO(SectorsDAO sectorsDAO) {
        this.sd = sectorsDAO;
    }

    public void clearAll() {
        if (sectorsColors!= null && sectorsColors.size()>0)
        new reset_data().execute();
        //   MainActivity.ll_main_progress.setVisibility(GONE);
    }

    public class reset_data extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            for (int i = 0; i < sectorsColors.size(); i++) {
                if (sectorsColors.get(i) != -16777216) {
                    sectorsColors.set(i, 0xFFFFFFFF);
                    sd.update(i, 0xFFFFFFFF);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (Share.redoItems.size() > 0)
                    Share.redoItems.clear();
                if (Share.undoItems.size() > 0)
                    Share.undoItems.clear();
                if (Share.AllFilledData.size() > 0)
                    Share.AllFilledData.clear();
//        for (int i = 0; i < sectorsColors.size(); i++) {
//            if (sectorsColors.get(i) != -16777216) {
//                sectorsColors.set(i, 0xFFFFFFFF);
//                sectorsDAO.update(i, 0xFFFFFFFF);
//            }
//        }
                MainStoreObj main_obj = new MainStoreObj();
                main_obj.setImage_id(Share.image_id);
                main_obj.setApp_id(Share.app_id);
                main_obj.setUndo_list(Share.undoItems);
                main_obj.setRedo_list(Share.redoItems);

                ArrayList<SVG_obj> sub_list = new ArrayList<>();
                for (int i = 0; i < Share.AllFilledData.size(); i++) {

                    SVG_obj sub_obj = new SVG_obj();
                    sub_obj.setSector(Share.AllFilledData.get(i).getSector());
                    sub_obj.setFill_color(Share.AllFilledData.get(i).getFill_color());
                    sub_list.add(sub_obj);
                }
                main_obj.setObject_list(sub_list);

                Gson gson = new Gson();
                String json = gson.toJson(main_obj).toString();
//              SharedPrefs.save(context, "svg_data_" + main_obj.getApp_id() + "_" + main_obj.getImage_id(), json);
               // spref.save(context, "svg_" + Share.image_id + "_" + Share.app_id, json);
                sd.save_pref_data("svg_" + Share.image_id + "_" + Share.app_id, json);
                updatePicture();
                Share.deleteFilePath(context, Share.app_id, Share.image_id);
                ll_main_progress.setVisibility(GONE);
                ll_main_click.setVisibility(View.GONE);
//                MainActivity.centerImageView.setEnabled(true);
//                System.gc();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public int iscolorAllWhite() {
        Random random = new Random();
        int count = 0;
        for (int i = 0; i < sectorsColors.size(); i++) {
            if (sectorsColors.get(i) != Color.WHITE) {
                count++;
            }
        }
        return count;

    }

    public void updatePicture() {

        Log.e("upppppp", "uppppp" + sectorsColors.size());
        Paint paint = new Paint();
        paint.setAntiAlias(true);

        Canvas canvas = sharpDrawable.getPicture().beginRecording(
                sharpDrawable.getPicture().getWidth(),
                sharpDrawable.getPicture().getHeight()
        );

        int j = 0, k = 0, s = 0;
        for (int i = 0; i < sectorsPaths.size(); i++) {
            //if (sectorsFlags.get(i)) {
            if (sectorsColors.size() > j)
                paint.setColor(sectorsColors.get(j));
            s = j++;
            Log.e("iddddd", "id" + sectorsPaths.get(s).getFillType());
            canvas.drawPath(sectorsPaths.get(s), paint);
           /* } else {
                paint.setColor(bckgSectorsColors.get(k));
                s = k++;
                Log.e("iddddd","id"+bckgSectorsColors.get(s));
                canvas.drawPath(bckgSectorsPaths.get(s), paint);
            }*/
        }
        sharpDrawable.getPicture().endRecording();
        vectorImageView.invalidate();
    }
}