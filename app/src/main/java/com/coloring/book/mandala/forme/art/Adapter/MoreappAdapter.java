package com.coloring.book.mandala.forme.art.Adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.coloring.book.mandala.forme.art.Model.moreAppObject;
import com.coloring.book.mandala.forme.art.R;

import java.util.ArrayList;
import java.util.List;

public class MoreappAdapter extends Adapter<ViewHolder> {

    private List<moreAppObject.dataobj> list = new ArrayList<>();
    private Context mContext;
    private OnItemClickListener mOnItemClickListener;

    public class MenuItemViewHolder extends ViewHolder {
        public ImageView iv_thumb;
        public TextView tv_category_name;
        public View view_footer;

        public MenuItemViewHolder(View itemView) {
            super(itemView);
//            this.iv_thumb = (ImageView) itemView.findViewById(R.id.iv_thumb);
            this.tv_category_name = (TextView) itemView.findViewById(R.id.tv_category_name);
            this.view_footer = itemView.findViewById(R.id.view_footer);

            itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int i, String name);
    }

    public MoreappAdapter(Context context, List<moreAppObject.dataobj> commons, OnItemClickListener onItemClickListener) {
        this.mContext = context;
        this.list = commons;
        this.mOnItemClickListener = onItemClickListener;
    }

    public int getItemViewType(int position) {
        return position;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MenuItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.moreapp_list_item, parent, false));
    }

    @TargetApi(23)
    public void onBindViewHolder(ViewHolder holder, final int position) {

        MenuItemViewHolder holder1 = (MenuItemViewHolder) holder;
        //holder1.iv_thumb.getLayoutParams().height = (int) (DisplayMetricsHandler.getScreenWidth() * 0.46);
        final moreAppObject.dataobj item = (moreAppObject.dataobj) this.list.get(position);
        ((MenuItemViewHolder) holder).tv_category_name.setText(item.getName()); //item.getName()
        holder1.itemView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemClickListener.onItemClick(view, position, item.getName());
            }
        });

    }

    public int getItemCount() {
        return this.list.size();
    }
}
