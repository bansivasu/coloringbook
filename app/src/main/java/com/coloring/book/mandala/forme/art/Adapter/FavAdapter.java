package com.coloring.book.mandala.forme.art.Adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.coloring.book.mandala.forme.art.Model.FavObject;
import com.coloring.book.mandala.forme.art.Model.MainStoreObj;
import com.coloring.book.mandala.forme.art.R;
import com.coloring.book.mandala.forme.art.share.DisplayMetricsHandler;
import com.coloring.book.mandala.forme.art.share.Share;

import java.util.ArrayList;
import java.util.List;

public class FavAdapter extends Adapter<ViewHolder> {
    private List<FavObject> al_commons = new ArrayList<>();
    private Context mContext;
    private OnItemClickListener mOnItemClickListener;
    private ArrayList<MainStoreObj> pathlists = new ArrayList<>();

    public class MenuItemViewHolder extends ViewHolder {
        public ImageView iv_full_img;
        private ProgressBar progressBar1;

        public MenuItemViewHolder(View itemView) {
            super(itemView);
            this.iv_full_img = (ImageView) itemView.findViewById(R.id.iv_full_image);
            progressBar1 = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int i);
    }

    public FavAdapter(Context context, List<FavObject> commons, OnItemClickListener onItemClickListener) {
        this.mContext = context;
        this.al_commons = commons;
        this.mOnItemClickListener = onItemClickListener;
    //    sd = new SectorsDAO(mContext, DataBaseHelper.SECTORS.SECTORS_PHIL);
    }

    public int getItemViewType(int position) {
        return position;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MenuItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false));
    }

    public void onBindViewHolder(ViewHolder holder, final int position) {
        Log.e("TAG", "onBindViewHolder==>");
//        holder.setIsRecyclable(false);
        final MenuItemViewHolder holder1 = (MenuItemViewHolder) holder;

        FavObject item = (FavObject) this.al_commons.get(position);

        String path = null;
        if (Share.getFilePath(mContext,"1", item.getImage_id()) != null) {
            Uri uri = Uri.parse(Share.getFilePath(mContext,"1", item.getImage_id()));
            path = uri.getPath();
            Log.e("favpath","favpath"+path);
            Glide.with(this.mContext).load(path)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            holder1.progressBar1.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(holder1.iv_full_img);
        }
        else {
            path = item.getThumb_url();
            Log.e("favpath", "favpath else" + path);

            Glide.with(this.mContext).load(Integer.valueOf(path))
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .listener(new RequestListener<Integer, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, Integer model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, Integer model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            holder1.progressBar1.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(holder1.iv_full_img);
        }
        holder1.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemClickListener.onItemClick(view, position);
            }
        });
        holder1.iv_full_img.getLayoutParams().height = (int) (DisplayMetricsHandler.getScreenWidth() * 0.46);

    }

    public int getItemCount() {
        Log.e("TAG", "Home Adpater array size:==>" + this.al_commons.size());
        return this.al_commons.size();
    }
}
