package com.coloring.book.mandala.forme.art.Fragment;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.coloring.book.mandala.forme.art.Adapter.PhotosAdapter;
import com.coloring.book.mandala.forme.art.BuildConfig;
import com.coloring.book.mandala.forme.art.ColoringApplication;
import com.coloring.book.mandala.forme.art.ImageSlideAllActivity;
import com.coloring.book.mandala.forme.art.R;
import com.coloring.book.mandala.forme.art.share.NetworkManager;
import com.coloring.book.mandala.forme.art.share.Share;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Bansi on 02-01-2018.
 */
public class MyPhotosFragment extends Fragment implements View.OnClickListener {

    // widgets
    private RecyclerView rv_items;
    private LinearLayout ll_not_available, ll_items;
    private AdView mAdView;
    private TextView tv_not_avail;

    // variables
    private View viewFavFragment;
    public ProgressDialog mProgressDialog;
    private ArrayList<File> datalist = new ArrayList<>();
    private PhotosAdapter photosAdapter;
    private Boolean isInForeGround;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewFavFragment = inflater.inflate(R.layout.fragment_favourite, container, false);

        setHasOptionsMenu(true);
        findResources(viewFavFragment);
        setListeners();
        init();
        setAdView(viewFavFragment);
        return viewFavFragment;
    }


    private void findResources(View v) {

        rv_items = (RecyclerView) v.findViewById(R.id.rv_items);
        ll_not_available = (LinearLayout) v.findViewById(R.id.ll_not_available);
        ll_items = (LinearLayout) v.findViewById(R.id.ll_items);
        rv_items.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        tv_not_avail = (TextView) v.findViewById(R.id.tv_not_avail);
    }

    private void setListeners() {

    }

    private void init() {

        tv_not_avail.setText(getResources().getString(R.string.photos_not_avail));
        //sd = new SectorsDAO(getActivity(), DataBaseHelper.SECTORS.SECTORS_PHIL);
        getData();

        photosAdapter = new PhotosAdapter(getActivity(), datalist, new PhotosAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int i) {
                if (!ColoringApplication.getInstance().requestNewInterstitial()) {
                    Intent intent = new Intent(getActivity(), ImageSlideAllActivity.class);
                    Share.selectGalleryImgPos = i;
                    startActivity(intent);
                } else {
                    ColoringApplication.getInstance().mInterstitialAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdClosed() {
                            super.onAdClosed();
                              ColoringApplication.getInstance().mInterstitialAd.setAdListener(null);
                                ColoringApplication.getInstance().mInterstitialAd = null;
                                ColoringApplication.getInstance().ins_adRequest = null;
                                ColoringApplication.getInstance().LoadAds();

                                Intent intent = new Intent(getActivity(), ImageSlideAllActivity.class);
                                Share.selectGalleryImgPos = i;
                                startActivity(intent);
                        }

                        @Override
                        public void onAdFailedToLoad(int i) {
                            super.onAdFailedToLoad(i);
                        }

                        @Override
                        public void onAdLoaded() {
                            super.onAdLoaded();
                        }
                    });
                }
            }
        });
        rv_items.setAdapter(photosAdapter);
    }

    private void setAdView(View v) {
        try {
            mAdView = (AdView) v.findViewById(R.id.adView);
            if (NetworkManager.isInternetConnected(getActivity().getApplicationContext())) {
                //TODO : Ads
                AdRequest adRequest = new AdRequest.Builder()
                        .addTestDevice("E19949FB5E7C5A28C30A875934AC8181") //SWIPE
                        .addTestDevice("41E9C9F5D1F985FB36C9760EFC8F3916") //Lenovo
                        .addTestDevice("CEB37F370ECF287C057F70BBC902632E")  //Coolpad
                        .addTestDevice("51A49E7B1B359D1999E5C85CE4F54978") //XOLO
                        .addTestDevice("F9EBC1840023CB004A83005514278635")  //MI 6otu (No SIM)
                        .addTestDevice("2442EC754FEF046014B26ACCEEAE9C23") // Micromax New
                        .addTestDevice("413FAED40213710754F4D30AC4F60355")  //INTEX
                        .addTestDevice("A7A19E06342F7D3868ABA7863D707BD7") //Samsung Tab
                        .addTestDevice("F9EB72307322A818A3399B51B00A765D") //LAVA
                        .addTestDevice("0E45853B0874EAA6418891AD964CC470") //X-ZIOX
                        .addTestDevice("3C8E4AA9C3802D60B83603426D16E430") //Celkon
                        .addTestDevice("89FAEE279F58CCC3FA1ABC0904E1FCBE") //Karbonn
                        .addTestDevice("74527FD0DD7B0489CFB68BAED192733D") //Nexus TAB
                        .addTestDevice("26DFCD14E05E54F19609B18400F672A0") //Samsung j2
                        .addTestDevice("E56855A0C493CEF11A7098FE6EA840CB") //Videocon
                        .addTestDevice("390FED1AE343E9FF9D644C4085C3868E") //jivi
                        .addTestDevice("ACFC7B7082B3F3FD4E0AC8E92EA10D53") //MI Tab
                        .addTestDevice("863D8BAE88E209F38FF3C94A0403C776") //Samsung old
                        .addTestDevice("85F8E335F3BBDFA2782587EEA19688D2") //Samsung new
                        .addTestDevice("517048997101BE4535828AC2360582C2") //motorola
                        .addTestDevice("8BB4BCB27396AB8ED222B7F902E13420") //micromax old
                        .addTestDevice("7F3924D7AB776DC5FF2D314CCFEE0EE0")//Gionee
                        .addTestDevice("F7803FE72A2748F6028D87DC36D7C574") //Mi Chhotu JIO
                        .build();
                mAdView.loadAd(adRequest);
                mAdView.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        mAdView.setVisibility(View.VISIBLE);
                    }
                });
            } else {
                mAdView.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showToastMsg(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    private void showProgressDialog() {
        try {
            if (mProgressDialog == null) {
                mProgressDialog = ProgressDialog.show(getActivity(), BuildConfig.FLAVOR, getResources().getString(R.string.loading), false, false);
            }
            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    public void hideDialog() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        } catch (IllegalArgumentException e) {
        }
    }


    private void LoadData(final boolean b) {
        //datalist.addAll(sd.getGalleryData());
        datalist.clear();
        getData();
        if (datalist.size() > 0) {
            ll_items.setVisibility(View.VISIBLE);
            ll_not_available.setVisibility(View.GONE);
        } else {
            ll_items.setVisibility(View.GONE);
            ll_not_available.setVisibility(View.VISIBLE);
        }
        getActivity().invalidateOptionsMenu();
        photosAdapter.notifyDataSetChanged();
        hideDialog();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_retry_home /*2131558584*/:
                showProgressDialog();
                LoadData(false);
                break;
            default:
                break;
        }
    }

    public void onResume() {
        super.onResume();
        isInForeGround = true;
        /*if (ColoringApplication.getInstance() != null && !ColoringApplication.getInstance().isLoaded())
            ColoringApplication.getInstance().LoadAds();*/
        showProgressDialog();
        LoadData(false);
    }

    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDetach() {
        try {
            rv_items.destroyDrawingCache();
            rv_items.removeAllViews();
            rv_items.removeAllViewsInLayout();
            rv_items.getRecycledViewPool().clear();

            datalist.clear();
            datalist = null;

            photosAdapter= null;

            viewFavFragment.destroyDrawingCache();
            viewFavFragment = null;
          //  System.gc();
        }catch (Exception e){
            e.printStackTrace();
        }
        super.onDetach();
    }

    public void onDestroyView() {
        isInForeGround = false;
        super.onDestroyView();
    }

    public void onStop() {
        super.onStop();
        isInForeGround = false;
    }

    public void onSaveInstanceState(Bundle outState) {
    }

    public void getData() {
        File path = new File(Share.IMAGE_PATH, "");
        File[] allFiles;
        datalist.clear();
        if (path.exists()) {

            allFiles = path.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return (name.endsWith(".jpg") || name.endsWith(".jpeg") || name.endsWith(".png"));
                }
            });
            if (allFiles != null) {
                if (allFiles.length > 0) {
                    for (int i = 0; i < allFiles.length; i++) {
                        datalist.add(allFiles[i]);
                        Collections.sort(datalist, Collections.reverseOrder());
                    }
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_items:
                if (datalist != null && datalist.size() > 0) {
                    try {
                        android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(getActivity());
                        alert.setMessage(getResources().getString(R.string.msg_delete_all));
                        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                                for (int j = 0; j < datalist.size(); j++) {

                                    new File(datalist.get(j).getPath()).delete();
                                }
                                datalist.clear();
                                photosAdapter.notifyDataSetChanged();
                                getActivity().invalidateOptionsMenu();
                                Toast.makeText(getActivity(), getResources().getString(R.string.success_del_all), Toast.LENGTH_SHORT).show();
                                ll_not_available.setVisibility(View.VISIBLE);
                                ll_items.setVisibility(View.GONE);
                            }
                        });
                        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });
                        alert.show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item_delete = menu.findItem(R.id.delete_items);
        if (datalist.size() > 0) {
            item_delete.setIcon(getResources().getDrawable(R.drawable.ic_delete_big));
        } else {
            item_delete.setIcon(getResources().getDrawable(R.drawable.ic_delete_desable));
        }
    }
}
