package com.coloring.book.mandala.forme.art.share;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;

import com.coloring.book.mandala.forme.art.Model.AdModel;
import com.coloring.book.mandala.forme.art.Model.CategoryModel;
import com.coloring.book.mandala.forme.art.Model.ImageObject;
import com.coloring.book.mandala.forme.art.Model.SubCatModel;
import com.coloring.book.mandala.forme.art.Model.UndoObj;
import com.coloring.book.mandala.forme.art.Model.moreAppObject;
import com.coloring.book.mandala.forme.art.R;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Bansi on 21-11-2017.
 */
public class Share {
    public static int SELECTED = 0;
    public static double screenHeight;
    public static final String HIDDEN_IMAGE_PATH = Environment.getExternalStorageDirectory().getPath() + File.separator + ".ColoringBook";
    public static final String IMAGE_PATH = Environment.getExternalStorageDirectory().getPath() + File.separator + "ColoringBook_IMG";

    public static List<moreAppObject.dataobj> moreapp_datalist = new ArrayList<>();
    public static ArrayList<UndoObj> undoItems = new ArrayList<>();
    public static ArrayList<UndoObj> redoItems = new ArrayList<>();
    public static ArrayList<UndoObj> AllFilledData = new ArrayList<>();
    public static int currentcolor = Color.parseColor("#ffff4444");
    public static int firstcolor = Color.parseColor("#ffff4444");
    public static int secondcolor = Color.parseColor("#ff99cc00");
    public static int thirdcolor = Color.parseColor("#FFFF33");
    public static String is_items = "0";
    public static String app_id;
    public static String image_id;
    public static int selectGalleryImgPos = 0;
    public static int screenWidth = 0;
    public static boolean is_button_click = false;
    public static ArrayList<AdModel> full_ad = new ArrayList<>();
    public static int AD_index;
    public static ArrayList<AdModel> al_ad_data = new ArrayList<>();
    public static Boolean is_start = false;
    public static ArrayList<CategoryModel> al_app_center_home_data = new ArrayList<>();
    public static ArrayList<CategoryModel> al_app_center_data = new ArrayList<>();
    public static ArrayList<SubCatModel> more_apps_data = new ArrayList<>();
    public static String ntv_inglink;
    public static String ntv_img;
    public static boolean is_more_apps = false;
    public static int position;
    public static String selected_tab;
    public static boolean APD_FLAG = false;
    public static boolean is_refreshable;
    public static String title;
    public static ArrayList<Integer> colors = new ArrayList<>();

    public static int getBackgroundColor(View view) {
        Drawable drawable = view.getBackground();
        if (drawable instanceof ColorDrawable) {
            ColorDrawable colorDrawable = (ColorDrawable) drawable;
            if (Build.VERSION.SDK_INT >= 11) {
                return colorDrawable.getColor();
            }
            try {
                Field field = colorDrawable.getClass().getDeclaredField("mState");
                field.setAccessible(true);
                Object object = field.get(colorDrawable);
                field = object.getClass().getDeclaredField("mUseColor");
                field.setAccessible(true);
                return field.getInt(object);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public static void showAlert(final Activity activity, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.MyAlertDialog);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", null);
        builder.show();
    }

    // get color of touch point of imageview
    public static int getPixelAtPoint(Bitmap bitmap, int x, int y) {
        if (isValidPoint(x, y, bitmap)) {
            return bitmap.getPixel(x, y);
        }
        return Color.TRANSPARENT;
    }

    public static boolean isValidPoint(int x, int y, Bitmap bitmap) {
        final int width = bitmap.getWidth();
        final int height = bitmap.getHeight();

        return isValidCoordinate(x, width) && isValidCoordinate(y, height);
    }

    public static boolean isValidCoordinate(int coordinate, int size) {
        return coordinate > 0 && coordinate < size;
    }

    public static String getFilePath(Context c,String app_id, String image_id) {
        try {
//            String path = c.getExternalCacheDir().getPath() + File.separator + ".ColoringBook";
            File directory = new File(HIDDEN_IMAGE_PATH);
            if (!directory.exists())
                directory.mkdir();
//            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String file_name = "colorimage_" + app_id + "_" + image_id;
            File mypath = new File(directory, file_name + ".jpeg");

            if (mypath.exists()) {
                return directory.getPath() + "/" + file_name + ".jpeg";
            } else {
                return null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void deleteFilePath(Context c,String app_id, String image_id) {
        try {
//            String path = c.getExternalCacheDir().getPath() + File.separator + ".ColoringBook";
            File directory = new File(HIDDEN_IMAGE_PATH);
            if (directory.exists()) {
//            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                String file_name = "colorimage_" + app_id + "_" + image_id;
                File mypath = new File(directory, file_name + ".jpeg");

                if (mypath.exists()) {
                    mypath.delete();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void deleteDirectory(Context c) {
        try {
            Log.e("delete","delete all");
            File file = new File(HIDDEN_IMAGE_PATH);
            if (file.exists()) {
                String deleteCmd = "rm -r " + HIDDEN_IMAGE_PATH;
                Runtime runtime = Runtime.getRuntime();
                try {
                    runtime.exec(deleteCmd);
                } catch (IOException e) { }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String saveToHiddenPhoto(Context c, Bitmap bitmapImage, String app_id, String image_id) {
        try {
//            String path = c.getExternalCacheDir().getPath() + File.separator + ".ColoringBook";
            File directory = new File(HIDDEN_IMAGE_PATH);
            if (!directory.exists())
                directory.mkdir();
            String file_name = "colorimage_" + app_id + "_" + image_id;
            File mypath = new File(directory, file_name + ".jpeg");
            mypath.deleteOnExit();
            mypath.createNewFile();
            // Create imageDir
            if (mypath.exists()) {
                FileOutputStream fos = new FileOutputStream(mypath);
                // Use the compress method on the BitMap object to write image to the OutputStream
                bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.flush();
                fos.close();
            } else {
            }
            return directory.getPath() + "/" + file_name + ".jpeg";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String saveToPhoto(Bitmap bitmapImage, String app_id, String image_id) {
        try {
            File directory = new File(IMAGE_PATH);
            if (!directory.exists())
                directory.mkdir();
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String file_name = "colorimage_" + timeStamp;
            File mypath = new File(directory, file_name + ".jpeg");
            mypath.deleteOnExit();
            mypath.createNewFile();
            Log.e("TAG", "" + mypath);
            // Create imageDir
            if (mypath.exists()) {
                FileOutputStream fos = new FileOutputStream(mypath);
                // Use the compress method on the BitMap object to write image to the OutputStream
                bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.flush();
                fos.close();
            }
            return directory.getPath() + "/" + file_name + ".jpeg";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
