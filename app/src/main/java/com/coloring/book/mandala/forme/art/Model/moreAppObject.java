package com.coloring.book.mandala.forme.art.Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Bansi on 08-05-2017.
 */
public class moreAppObject implements Serializable {

    public String status;
    public String message;

    public ArrayList<dataobj> data;

    public static class dataobj implements Serializable {

        public String id;
        public String name;
        public String image;
        public String Is_active_android;

        public String getIs_active_android() {
            return Is_active_android;
        }

        public void setIs_active_android(String is_active_android) {
            Is_active_android = is_active_android;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
