package com.coloring.book.mandala.forme.art.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.coloring.book.mandala.forme.art.Model.AdModel;
import com.coloring.book.mandala.forme.art.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 11/18/2016.
 */
public class SplashAdImageAdapter extends RecyclerView.Adapter<SplashAdImageAdapter.ViewHolder> {
    Context context;
    int screen_width;
    int screen_height;
    List<AdModel> al_ad_item = new ArrayList<>();


    public SplashAdImageAdapter(final Context context, List<AdModel> list_data) {
        this.context = context;
        this.al_ad_item = list_data;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_ad_image;
        TextView tv_app_name;
        ProgressBar progressBar;
        LinearLayout sfl_main;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_ad_image = (ImageView) itemView.findViewById(R.id.iv_ad_image);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
            tv_app_name = (TextView) itemView.findViewById(R.id.tv_app_name);
            sfl_main = (LinearLayout) itemView.findViewById(R.id.sfl_main);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.splash_row_ad_data, parent, false);
        return new ViewHolder(view);
    }

    private float convertDpToPx(Context context, float dp) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.setIsRecyclable(false);

        holder.tv_app_name.setText(al_ad_item.get(position).getName());

        Glide.with(context)
                .load(al_ad_item.get(position).getThumb_image())
                .asBitmap().into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                int height = (int) (resource.getHeight() * (512.0 / resource.getWidth()));
                Bitmap scaled = Bitmap.createScaledBitmap(resource, 512, height, true);
                holder.iv_ad_image.setImageBitmap(scaled);

                scaled = null;
                System.gc();

//                holder.iv_ad_image.setImageBitmap(resource);
                holder.progressBar.setVisibility(View.INVISIBLE);
                holder.tv_app_name.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                super.onLoadFailed(e, errorDrawable);
            }
        });

        holder.iv_ad_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(al_ad_item.get(position).getApp_link())));
            }
        });
    }

    @Override
    public int getItemCount() {
        return al_ad_item.size();
    }
}
