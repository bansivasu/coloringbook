package com.coloring.book.mandala.forme.art.Interface;

/**
 * Created by Bansi on 02-06-2017.
 */
public interface ActivityCommunicator{
    public void passDataToActivity(Boolean someValue, String myPhotosFragment);
}
