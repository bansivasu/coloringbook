package com.coloring.book.mandala.forme.art.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.coloring.book.mandala.forme.art.Adapter.HomeAdapter;
import com.coloring.book.mandala.forme.art.ColoringApplication;
import com.coloring.book.mandala.forme.art.MainActivity;
import com.coloring.book.mandala.forme.art.Model.ImageObject;
import com.coloring.book.mandala.forme.art.R;
import com.coloring.book.mandala.forme.art.WebService.SimpleService;
import com.coloring.book.mandala.forme.art.share.NetworkManager;
import com.coloring.book.mandala.forme.art.share.Share;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MoreappFragment extends Fragment implements OnClickListener {


    TextView tv_menu_titleDrawMore;
    private AdView mAdView;
    LinearLayout ll_no_internet_draw_more_single;
    TextView tv_retry_draw_more_single;

    int pos = 0;
    ProgressDialog mProgressDialog;
    ImageLoader imageLoader;
    private FirebaseAnalytics mFirebaseAnalytics;
    boolean isInForeGround;
    private RecyclerView rv_items;
    private TextView tv_coming;
    private ArrayList<String> list = new ArrayList<>();
    private Integer[] listImages = new Integer[]{R.drawable.mandala1,R.drawable.mandala2,R.drawable.mandala3,R.drawable.mandala4,
            R.drawable.mandala5,R.drawable.mandala6,R.drawable.mandala7,R.drawable.mandala8,
            R.drawable.mandala9,R.drawable.mandala11,R.drawable.mandala12,
            R.drawable.mandala13,R.drawable.mandala14,R.drawable.mandala15,R.drawable.mandala16,
            R.drawable.mandala17,R.drawable.mandala18,R.drawable.mandala19,R.drawable.mandala20,
            R.drawable.mandala21,R.drawable.mandala22};

    private HomeAdapter itemsAdapter;
    private View viewHomeFragment;

    public MoreappFragment() {
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewHomeFragment = inflater.inflate(R.layout.activity_draw_more_single, container, false);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        Share.is_refreshable = false;
        findResources(viewHomeFragment);
        setListeners();
        init();

//        if (!SharedPrefs.getBoolean(this, SharedPrefs.IS_ADS_REMOVED)) {
        adsBanner();
//        }
        if (Share.app_id != null && Share.app_id.equalsIgnoreCase("-1")) {
            tv_coming.setVisibility(View.VISIBLE);
            ll_no_internet_draw_more_single.setVisibility(View.GONE);
            rv_items.setVisibility(View.GONE);
        } else {
            rv_items.setVisibility(View.VISIBLE);
            ll_no_internet_draw_more_single.setVisibility(View.GONE);
            tv_coming.setVisibility(View.GONE);
            // showProgressDialog();
            // LoadMoreAppData();
            if (itemsAdapter != null)
                itemsAdapter.notifyDataSetChanged();
        }
        return this.viewHomeFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void findResources(View v) {

        ll_no_internet_draw_more_single = (LinearLayout) v.findViewById(R.id.ll_no_internet_draw_more_single);
        tv_retry_draw_more_single = (TextView) v.findViewById(R.id.tv_retry_draw_more_single);
        rv_items = (RecyclerView) v.findViewById(R.id.rv_items);
        tv_coming = (TextView) v.findViewById(R.id.tv_coming);

        ll_no_internet_draw_more_single.setOnClickListener(this);

        rv_items.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        rv_items.setNestedScrollingEnabled(true);
    }

    private void setListeners() {
        ll_no_internet_draw_more_single.setOnClickListener(this);
    }

    private void init() {

        imageLoader = ImageLoader.getInstance();
       /* getActivity().setSupportActionBar(toolbar_draw_more_single);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_header_back);*/

        //  ArrayList<String> listImages = new ArrayList<String>(Arrays.asList(getAssets().list("mandla")));
//        tv_menu_titleDrawMore.setText(Share.al_appsModels.get(pos).getTitle());
        // Share.image_id = Share.datalist.get(i).getId();

        itemsAdapter = new HomeAdapter(getActivity(), listImages, list, new HomeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int i) {
                Log.e("click", "click" + i);

                if (!ColoringApplication.getInstance().requestNewInterstitial()) {
                    Share.AllFilledData.clear();
                    // Share.app_id = Share.datalist.get(i).getApp_id();
                    Share.app_id = "1";
                    Share.image_id = String.valueOf(i + 1);
                    Share.undoItems.clear();
                    Share.redoItems.clear();
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.putExtra("title", "Mandala");
                    intent.putExtra("thumburl", String.valueOf(listImages[i]));
                            /*intent.putExtra("imageurl", Share.datalist.get(i).getImage());*/
                    startActivity(intent);
                } else {
                    ColoringApplication.getInstance().mInterstitialAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdClosed() {
                            super.onAdClosed();

                            ColoringApplication.getInstance().mInterstitialAd.setAdListener(null);
                            ColoringApplication.getInstance().mInterstitialAd = null;
                            ColoringApplication.getInstance().ins_adRequest = null;
                            ColoringApplication.getInstance().LoadAds();
                            Share.AllFilledData.clear();
                            // Share.app_id = Share.datalist.get(i).getApp_id();
                            // Share.image_id = Share.datalist.get(i).getId();
                            Share.app_id = "1";
                            Share.image_id = String.valueOf(i + 1);
                            Share.undoItems.clear();
                            Share.redoItems.clear();
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            intent.putExtra("title", "Mandala");
                            intent.putExtra("thumburl", String.valueOf(listImages[i]));
                            /*intent.putExtra("imageurl", Share.datalist.get(i).getImage());*/
                            startActivity(intent);
                        }

                        @Override
                        public void onAdFailedToLoad(int i) {
                            super.onAdFailedToLoad(i);
                        }

                        @Override
                        public void onAdLoaded() {
                            super.onAdLoaded();
                        }
                    });
                }
            }
        });
        rv_items.setAdapter(itemsAdapter);
    }

    private void adsBanner() {
        try {
            mAdView = (AdView) viewHomeFragment.findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    // Check the LogCat to get your test device ID
                    //.addTestDevice(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID))
                    //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .addTestDevice("E19949FB5E7C5A28C30A875934AC8181") //SWIPE
                    .addTestDevice("41E9C9F5D1F985FB36C9760EFC8F3916") //Lenovo
                    .addTestDevice("CEB37F370ECF287C057F70BBC902632E")  //Coolpad
                    .addTestDevice("51A49E7B1B359D1999E5C85CE4F54978") //XOLO
                    .addTestDevice("F9EBC1840023CB004A83005514278635")  //MI 6otu (No SIM)
                    .addTestDevice("2442EC754FEF046014B26ACCEEAE9C23") // Micromax New
                    .addTestDevice("413FAED40213710754F4D30AC4F60355")  //INTEX
                    .addTestDevice("A7A19E06342F7D3868ABA7863D707BD7") //Samsung Tab
                    .addTestDevice("F9EB72307322A818A3399B51B00A765D") //LAVA
                    .addTestDevice("0E45853B0874EAA6418891AD964CC470") //X-ZIOX
                    .addTestDevice("3C8E4AA9C3802D60B83603426D16E430") //Celkon
                    .addTestDevice("89FAEE279F58CCC3FA1ABC0904E1FCBE") //Karbonn
                    .addTestDevice("74527FD0DD7B0489CFB68BAED192733D") //Nexus TAB
                    .addTestDevice("26DFCD14E05E54F19609B18400F672A0") //Samsung j2
                    .addTestDevice("E56855A0C493CEF11A7098FE6EA840CB") //Videocon
                    .addTestDevice("390FED1AE343E9FF9D644C4085C3868E") //jivi
                    .addTestDevice("ACFC7B7082B3F3FD4E0AC8E92EA10D53") //MI Tab
                    .addTestDevice("863D8BAE88E209F38FF3C94A0403C776") //Samsung old
                    .addTestDevice("85F8E335F3BBDFA2782587EEA19688D2") //Samsung new
                    .addTestDevice("517048997101BE4535828AC2360582C2") //motorola
                    .addTestDevice("8BB4BCB27396AB8ED222B7F902E13420") //micromax old
                    .addTestDevice("7F3924D7AB776DC5FF2D314CCFEE0EE0")//Gionee
                    .addTestDevice("F7803FE72A2748F6028D87DC36D7C574") //Mi Chhotu JIO
                    .addTestDevice("36FE3612E964A00DC40A18301E964202") // nokia

                    .build();
            mAdView.loadAd(adRequest);

            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                }

                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    mAdView.setVisibility(View.GONE);
                }

                @Override
                public void onAdLeftApplication() {
                    super.onAdLeftApplication();
                }

                @Override
                public void onAdOpened() {
                    super.onAdOpened();
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    mAdView.setVisibility(View.VISIBLE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showToastMsg(String msg) {
        Toast toastMsg = Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT);
        toastMsg.show();
    }

    protected void showProgressDialog() {
        if (mProgressDialog == null)
            mProgressDialog = ProgressDialog.show(getActivity(), "", getResources().getString(R.string.loading), false, false);
        if (!mProgressDialog.isShowing())
            mProgressDialog.show();
    }

    protected void hideDialog() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        } catch (IllegalArgumentException e) {
        }
    }

    private void LoadMoreAppData() {

        if (NetworkManager.isInternetConnected(getActivity()) && Share.app_id != null) {
            // if (Share.datalist.size() == 0) {
            SimpleService.get().getstatusObj(Integer.valueOf(Share.app_id), new Callback<ImageObject>() {

                @Override
                public void success(ImageObject StoreObj, Response response) {
                    rv_items.setVisibility(View.VISIBLE);
                    ll_no_internet_draw_more_single.setVisibility(View.GONE);

                    //   Share.datalist.clear();
                    list.clear();
/*
                        if (StoreObj.data != null && StoreObj.data.size() > 0)
                            Share.datalist.addAll(StoreObj.data);
*/
                        /*for (int i = 0; i < Share.datalist.size(); i++) {
                            String path = null;
                            if (Share.getFilePath(getApplicationContext(), Share.datalist.get(i).getApp_id(), Share.datalist.get(i).getId()) != null) {
                                Uri uri = Uri.parse(Share.getFilePath(getApplicationContext(), Share.datalist.get(i).getApp_id(), Share.datalist.get(i).getId()));
                                path = uri.getPath();
                                list.add(path);
                            }
                        }*/
                    itemsAdapter.notifyDataSetChanged();
                    hideDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    hideDialog();
                    rv_items.setVisibility(View.GONE);
                    ll_no_internet_draw_more_single.setVisibility(View.VISIBLE);
                }
            });
        } else {
            if (itemsAdapter != null) {
                rv_items.setVisibility(View.VISIBLE);
                ll_no_internet_draw_more_single.setVisibility(View.GONE);
                itemsAdapter.notifyDataSetChanged();
                hideDialog();
            }
        }
        /*} else {
            hideDialog();
            ll_no_internet_draw_more_single.setVisibility(View.VISIBLE);
        }*/
    }

    @Override
    public void onDestroy() {
        isInForeGround = false;
        super.onDestroy();
       /* System.gc();
        Runtime.getRuntime().gc();*/
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_no_internet_draw_more_single:
                showProgressDialog();
                LoadMoreAppData();
                break;
        }
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    public void onResume() {
        super.onResume();

        Log.e("resume", "resume");
        /*if (Share.is_items.equalsIgnoreCase("0")) {
            if (moreappAdapter != null)
                moreappAdapter.notifyDataSetChanged();
        } else if (Share.is_items.equalsIgnoreCase("mandala1")) {
            if (itemsAdapter != null)
                itemsAdapter.notifyDataSetChanged();
        } else if (Share.is_items.equalsIgnoreCase("mandala2")) {
            if (favAdapter != null)
                favAdapter.notifyDataSetChanged();
        }*/
        isInForeGround = true;
        if (ColoringApplication.getInstance() != null && !ColoringApplication.getInstance().isLoaded())
            ColoringApplication.getInstance().LoadAds();
         /* try {
            showProgressDialog();
            Log.e("data", "data" + Share.is_items);
            if (!Share.is_items) {
                ll_items.setVisibility(View.GONE);
                ll_moreapps.setVisibility(View.VISIBLE);
                ll_no_internet_home.setVisibility(View.GONE);
                LoadData();
            } else {
                ll_items.setVisibility(View.VISIBLE);
                ll_moreapps.setVisibility(View.GONE);
                ll_no_internet_home.setVisibility(View.GONE);
                LoadData_items();
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }*/
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDetach() {
        try {
            Log.e("MoreAppFragment", "ondetach");
            /*recyclerViewHome.destroyDrawingCache();
            recyclerViewHome.removeAllViews();
            recyclerViewHome.removeAllViewsInLayout();
            recyclerViewHome.getRecycledViewPool().clear();
            moreapp_datalist.clear();*/
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDetach();
    }

    public void onDestroyView() {
        super.onDestroyView();
        viewHomeFragment.destroyDrawingCache();
        viewHomeFragment = null;
        isInForeGround = false;
        //  System.gc();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        isInForeGround = false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
        } else {
        }
    }
}
