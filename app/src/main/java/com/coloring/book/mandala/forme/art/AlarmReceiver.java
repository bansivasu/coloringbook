package com.coloring.book.mandala.forme.art;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.coloring.book.mandala.forme.art.Model.AdModel;
import com.coloring.book.mandala.forme.art.Model.NotiModel;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by Bansi on 20-02-2018.
 */

public class AlarmReceiver extends BroadcastReceiver {

    private static int MID = 0, m=0;
    private static ArrayList<AdModel> datalist = new ArrayList<>();

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
        Log.i("receive","receive"+MID+"mmm"+m+"mmm"+datalist.size());

        SharedPreferences preferences = context.getSharedPreferences("draw_tattoos_shared_prefs", Context.MODE_PRIVATE);
        String FoodName = preferences.getString("noti_list", "");
        MID = preferences.getInt("noti_count",0);
        m = preferences.getInt("m",0);
        Log.i("json","json"+FoodName);
        if (!FoodName.equalsIgnoreCase("")){
            Gson gson = new Gson();
            NotiModel model = gson.fromJson(FoodName, NotiModel.class);
            if (model.getList() != null && model.getList().size()>0)
                datalist.clear();
                datalist.addAll(model.getList());
        }

        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder mNotifyBuilder;
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent notificationIntent = new Intent(context, SplashHomeActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent;

        if (MID % 2 == 0) {
            if (datalist.size() > m) {
                boolean isAppInstalled = appInstalledOrNot(context, datalist.get(m).getPackage_name());
                if (isAppInstalled) {
                    //This intent will help you to launch if the package is already installed
                    notificationIntent = context.getPackageManager()
                            .getLaunchIntentForPackage(datalist.get(m).getPackage_name());

                } else {
                    try {
                        notificationIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + datalist.get(m).getPackage_name()));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        notificationIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + datalist.get(m).getPackage_name()));
                    }
                }
                pendingIntent = PendingIntent.getActivity(context, 0,
                        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                mNotifyBuilder = new NotificationCompat.Builder(context, context.getResources().getString(R.string.app_name)+" ChannelId")
                        .setSmallIcon(R.mipmap.ic_notification)
                        .setContentTitle(datalist.get(m).getName())
                        .setContentText("Click here to open " + datalist.get(m).getName().toLowerCase()).setSound(alarmSound)
                        .setWhen(when)
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent)
                        .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
                m++;
                if (m >= datalist.size())
                    m = 0;
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt("m", m);
                editor.commit();
            } else {
                pendingIntent = PendingIntent.getActivity(context, 0,
                        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                mNotifyBuilder = new NotificationCompat.Builder(context, context.getResources().getString(R.string.app_name)+" ChannelId")
                        .setSmallIcon(R.drawable.appicon)
                        .setContentTitle(context.getText(R.string.app_name))
                        .setContentText("Click here to open " + context.getText(R.string.app_name).toString().toLowerCase())
                        .setWhen(when)
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent)
                        .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
            }
        } else {
            pendingIntent = PendingIntent.getActivity(context, 0,
                    notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            mNotifyBuilder = new NotificationCompat.Builder(context,context.getResources().getString(R.string.app_name)+" ChannelId")
                    .setSmallIcon(R.drawable.appicon)
                    .setContentTitle(context.getText(R.string.app_name))
                    .setContentText("Click here to open " + context.getText(R.string.app_name).toString().toLowerCase()).setSound(alarmSound)
                    .setWhen(when)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_LOW;

      /* Create or update. */
            NotificationChannel channel = new NotificationChannel(context.getResources().getString(R.string.app_name)+" ChannelId",
                    context.getResources().getString(R.string.app_name)+" Channel",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(MID, mNotifyBuilder.build());
        MID++;
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("noti_count",MID);
        editor.commit();
    }

    private boolean appInstalledOrNot(Context context, String uri) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }
}