package com.coloring.book.mandala.forme.art;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.coloring.book.mandala.forme.art.Adapter.SplashAdImageAdapter;
import com.coloring.book.mandala.forme.art.Model.AdModel;
import com.coloring.book.mandala.forme.art.Model.CategoryModel;
import com.coloring.book.mandala.forme.art.Model.NotiModel;
import com.coloring.book.mandala.forme.art.Model.SubCatModel;
import com.coloring.book.mandala.forme.art.WebService.Webservice;
import com.coloring.book.mandala.forme.art.share.ExitApplication;
import com.coloring.book.mandala.forme.art.share.NetworkManager;
import com.coloring.book.mandala.forme.art.share.Share;
import com.coloring.book.mandala.forme.art.share.SharedPrefs;
import com.coloring.book.mandala.forme.art.share.Urls;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.hsalf.smilerating.BaseRating;
import com.hsalf.smilerating.SmileRating;
import com.startapp.android.publish.adsCommon.StartAppSDK;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

//import com.google.firebase.analytics.FirebaseAnalytics;

public class SplashHomeActivity extends Activity {

    Activity activity;
    private LinearLayout ll_adview, ll_ad_data, ll_no_connection;
    TextView tv_retry_start;
    private Button start;

    private RecyclerView rcv_ad_images;
    private ArrayList<AdModel> al_ad_data = new ArrayList<>();
    List<String> listPermissionsNeeded = new ArrayList<>();
    ImageView iv_half_logo;
    private int RESULT_LOAD_IMAGE = 123;
    boolean isInForeGround;
    private FirebaseAnalytics mFirebaseAnalytics;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        StartAppSDK.init(this, "200494941", false);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash_home);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        activity = SplashHomeActivity.this;
//        setAlarmforNotification(Share.al_ad_data);
        findViews();
        initView();

        String open = SharedPrefs.getString(SplashHomeActivity.this, "is_open_app");
        /*SharedPrefs preff = new SharedPrefs();
        String open = "0";
        open = preff.getString(getApplicationContext(),"app_open","1");*/
        if (open==null) {
            SharedPrefs.save(getApplicationContext(),"is_open_app","0");
            Share.deleteDirectory(getApplicationContext());
        }
        else if (!open.equalsIgnoreCase("0")){
            SharedPrefs.save(getApplicationContext(),"is_open_app","0");
            Share.deleteDirectory(getApplicationContext());
        }
    }

    private void findViews() {
        ll_adview = (LinearLayout) findViewById(R.id.ll_adview);
        ll_ad_data = (LinearLayout) findViewById(R.id.ll_ad_data);
        ll_no_connection = (LinearLayout) findViewById(R.id.ll_no_connection);
        tv_retry_start = (TextView) findViewById(R.id.tv_retry_start);

        rcv_ad_images = (RecyclerView) findViewById(R.id.rv_load_ads);
        ll_ad_data = (LinearLayout) findViewById(R.id.ll_ad_data);
        start = (Button) findViewById(R.id.start);
        iv_half_logo = (ImageView) findViewById(R.id.iv_half_logo);
    }

    private void initView() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(activity, 3);
        rcv_ad_images.setLayoutManager(gridLayoutManager);
        setDimen();

        Log.e("TAG", "SPLASH_AD_DATA :" + SharedPrefs.getString(SplashHomeActivity.this, SharedPrefs.SPLASH_AD_DATA));
        if (!SharedPrefs.getString(SplashHomeActivity.this, SharedPrefs.SPLASH_AD_DATA).equals("")) {
            Offline_Data();
        }

        if (NetworkManager.isInternetConnected(activity)) {
            new GetAdData().execute();
        } else {
//            Offline_Data();
            Share.is_more_apps = false;
            ll_adview.setVisibility(View.VISIBLE);
            iv_half_logo.setVisibility(View.VISIBLE);
            ll_ad_data.setVisibility(View.GONE);
            ll_no_connection.setVisibility(View.VISIBLE);
        }

        tv_retry_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkManager.isInternetConnected(SplashHomeActivity.this)) {
                    new GetAdData().execute();
                } else {
//                    Offline_Data();
                    Share.is_more_apps = false;
                    ll_adview.setVisibility(View.VISIBLE);
                    iv_half_logo.setVisibility(View.VISIBLE);
                    ll_ad_data.setVisibility(View.GONE);
                    ll_no_connection.setVisibility(View.VISIBLE);
                }
            }
        });


        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("after full_ad.size()", Share.full_ad.size() + "");
                int ad_index;
                try {
                    ad_index = SharedPrefs.getInt(getApplicationContext(), SharedPrefs.AD_INDEX);
                } catch (Exception e) {
                    ad_index = 0;
                }

                if (ad_index >= Share.full_ad.size()) {
                    ad_index = 0;
                    SharedPrefs.save(getApplicationContext(), SharedPrefs.AD_INDEX, ad_index);
                }
                Log.e("index", "index" + ad_index);
                Share.AD_index = ad_index;
                ad_index++;
                SharedPrefs.save(getApplicationContext(), SharedPrefs.AD_INDEX, ad_index);

                Intent intent = new Intent(activity, Splash_MenuActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);

                if (Share.full_ad.size() > 0) {
                    Log.e("full_ad_size", "> 0");
                    Intent intent_ad = new Intent(activity, FullScreenAdActivity.class);
                    startActivity(intent_ad);
                }
            }
        });
    }

    private void setDimen() {

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Share.screenWidth = size.x;
        Share.screenHeight = size.y;
    }

    private boolean checkAndRequestPermissions() {
        listPermissionsNeeded.clear();
        int storage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (storage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        Log.e("TAG", "listPermissionsNeeded===>" + listPermissionsNeeded);
        if (!listPermissionsNeeded.isEmpty()) {
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == RESULT_LOAD_IMAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            } else {
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean isPackageInstalled(String packagename) {
        try {
            PackageManager pm = getPackageManager();
            pm.getPackageInfo(packagename, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private class GetAdData extends AsyncTask<String, Void, Void> {
        String response;

        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                String temp = Urls.BASE_URL_APPS + "/AdvertiseNewApplicationsAppCenter/17/" + getPackageName();
                temp = temp.replaceAll(" ", "%20");
                URL sourceUrl = new URL(temp);
                response = Webservice.GET(sourceUrl);
            } catch (Exception e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ll_adview.setVisibility(View.INVISIBLE);
                        iv_half_logo.setVisibility(View.GONE);
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                if (!response.equals("")) {
                    try {
                        JSONObject data = new JSONObject(response);

                        if (data.getString("status").equals("1")) {
                            Share.al_ad_data.clear();
                            Share.al_app_center_home_data.clear();
                            Share.al_app_center_data.clear();
                            SharedPrefs.save(activity, SharedPrefs.SPLASH_AD_DATA, response.toString());
                            JSONArray arr_data = data.getJSONArray("data");

                            for (int i = 0; i < arr_data.length(); i++) {
                                JSONObject obj_data = arr_data.getJSONObject(i);

                                if (!getApplicationContext().getPackageName().equals(obj_data.getString("package_name"))) {
                                    AdModel adModel = new AdModel();
                                    adModel.setApp_link(obj_data.getString("app_link"));
                                    adModel.setThumb_image(obj_data.getString("thumb_image"));
                                    adModel.setName(obj_data.getString("name"));
                                    adModel.setFull_thumb_image(obj_data.getString("full_thumb_image"));
                                    adModel.setPackage_name(obj_data.getString("package_name"));
                                    Share.al_ad_data.add(adModel);
                                }
                            }


                            JSONArray arr_app_center = data.getJSONArray("app_center");
                            for (int i = 0; i < arr_app_center.length(); i++) {
                                JSONObject obj_data = arr_app_center.getJSONObject(i);
                                CategoryModel catModel = new CategoryModel();
                                catModel.setId(obj_data.getString("id"));
                                catModel.setName(obj_data.getString("name"));
                                catModel.setIs_active(obj_data.getString("is_active"));

                                ArrayList<SubCatModel> sub_list = new ArrayList<>();
                                JSONArray sub_arr_app_center = obj_data.getJSONArray("sub_category");
                                for (int j = 0; j < sub_arr_app_center.length(); j++) {
                                    JSONObject sub_obj_data = sub_arr_app_center.getJSONObject(j);
                                    SubCatModel subCatModel = new SubCatModel();
                                    subCatModel.setId(sub_obj_data.getString("id"));
                                    subCatModel.setApp_id(sub_obj_data.getString("app_id"));
                                    subCatModel.setPosition(sub_obj_data.getString("position"));
                                    subCatModel.setName(sub_obj_data.getString("name"));
                                    subCatModel.setIcon(sub_obj_data.getString("icon"));
                                    subCatModel.setStar(sub_obj_data.getString("star"));
                                    subCatModel.setInstalled_range(sub_obj_data.getString("installed_range"));
                                    subCatModel.setApp_link(sub_obj_data.getString("app_link"));
                                    subCatModel.setBanner(sub_obj_data.getString("banner"));
                                    sub_list.add(subCatModel);
                                }
                                catModel.setSub_category(sub_list);
                                Share.al_app_center_data.add(catModel);
                            }

                            JSONArray arr_home = data.getJSONArray("home");
                            for (int i = 0; i < arr_home.length(); i++) {
                                JSONObject obj_data = arr_home.getJSONObject(i);
                                CategoryModel catModel = new CategoryModel();
                                catModel.setId(obj_data.getString("id"));
                                catModel.setName(obj_data.getString("name"));
                                catModel.setIs_active(obj_data.getString("is_active"));

                                ArrayList<SubCatModel> sub_list = new ArrayList<>();
                                JSONArray sub_arr_app_center = obj_data.getJSONArray("sub_category");
                                for (int j = 0; j < sub_arr_app_center.length(); j++) {
                                    JSONObject sub_obj_data = sub_arr_app_center.getJSONObject(j);
                                    SubCatModel subCatModel = new SubCatModel();
                                    subCatModel.setId(sub_obj_data.getString("id"));
                                    subCatModel.setApp_id(sub_obj_data.getString("app_id"));
                                    subCatModel.setPosition(sub_obj_data.getString("position"));
                                    subCatModel.setName(sub_obj_data.getString("name"));
                                    subCatModel.setIcon(sub_obj_data.getString("icon"));
                                    subCatModel.setStar(sub_obj_data.getString("star"));
                                    subCatModel.setInstalled_range(sub_obj_data.getString("installed_range"));
                                    subCatModel.setApp_link(sub_obj_data.getString("app_link"));
                                    subCatModel.setBanner(sub_obj_data.getString("banner"));
                                    sub_list.add(subCatModel);
                                }
                                catModel.setSub_category(sub_list);
                                Share.al_app_center_home_data.add(catModel);
                            }

                            JSONArray arr_more_app = data.getJSONArray("more_apps");
                            if (arr_more_app.length() > 0) {
                                JSONObject obj_data = arr_more_app.getJSONObject(0);

                                ArrayList<SubCatModel> sub_list = new ArrayList<>();
                                JSONArray sub_arr_app_center = obj_data.getJSONArray("sub_category");
                                for (int j = 0; j < sub_arr_app_center.length(); j++) {
                                    JSONObject sub_obj_data = sub_arr_app_center.getJSONObject(j);
                                    SubCatModel subCatModel = new SubCatModel();
                                    subCatModel.setId(sub_obj_data.getString("id"));
                                    subCatModel.setApp_id(sub_obj_data.getString("app_id"));
                                    subCatModel.setPosition(sub_obj_data.getString("position"));
                                    subCatModel.setName(sub_obj_data.getString("name"));
                                    subCatModel.setIcon(sub_obj_data.getString("icon"));
                                    subCatModel.setStar(sub_obj_data.getString("star"));
                                    subCatModel.setInstalled_range(sub_obj_data.getString("installed_range"));
                                    subCatModel.setApp_link(sub_obj_data.getString("app_link"));
                                    subCatModel.setBanner(sub_obj_data.getString("banner"));
                                    sub_list.add(subCatModel);
                                }
                                Share.more_apps_data.clear();
                                Share.more_apps_data.addAll(sub_list);
                            }


                            JSONObject arr_nativ_add = data.getJSONObject("native_add");
                            Log.e("ATG", "arr_nativ_add-----" + arr_nativ_add.toString());

                            Share.ntv_img = arr_nativ_add.getString("image");
                            Share.ntv_inglink = arr_nativ_add.getString("playstore_link");
                            Log.e("ATG", "Image" + Share.ntv_img + " -----0---" + Share.ntv_inglink);

                            SplashAdImageAdapter adImageAdapter = new SplashAdImageAdapter(activity, Share.al_ad_data);
                            rcv_ad_images.setAdapter(adImageAdapter);
                            ll_adview.setVisibility(View.VISIBLE);
                            iv_half_logo.setVisibility(View.VISIBLE);
                            ll_ad_data.setVisibility(View.VISIBLE);
                            ll_no_connection.setVisibility(View.GONE);
                            Share.is_button_click = true;
                            Share.is_more_apps = true;
                            new DownLoadFullAdData().execute("");
                        } else {
                            Share.is_button_click = false;
                            Share.is_more_apps = false;
                            ll_adview.setVisibility(View.INVISIBLE);
                            iv_half_logo.setVisibility(View.GONE);
                            //Offline_Data();
                            new DownLoadFullAdData().execute("");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
  //--------------------------for local notification-------------------//
                    SharedPrefs preff = new SharedPrefs();
                    String open = "0";
                    open = preff.getString(getApplicationContext(),"is_open","0");
                    if (open!=null && open.equalsIgnoreCase("0") & Share.al_ad_data.size()>0) {
//                        Share.deleteDirectory(getApplicationContext());
                        setAlarmforNotification();
                    }
                    if (Share.full_ad.size() == 0) {
                        new DownLoadFullAdData().execute("");
                    }
                } else {
                    Share.is_more_apps = false;
                    ll_adview.setVisibility(View.INVISIBLE);
                    iv_half_logo.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();

                ll_adview.setVisibility(View.INVISIBLE);
                iv_half_logo.setVisibility(View.GONE);
            }
        }
    }

    private void setAlarmforNotification() {

        NotiModel model = new NotiModel();
        model.setList(Share.al_ad_data);
        Gson gson = new Gson();
        String json = gson.toJson(model).toString();
        Log.e("json","json"+json);
        SharedPrefs preff = new SharedPrefs();
        preff.save(getApplicationContext(),"noti_list",json);
        preff.save(getApplicationContext(),"is_open","1");
        preff.save(getApplicationContext(),"noti_count",0);
        preff.save(getApplicationContext(),"m",0);

        Calendar updateTime = Calendar.getInstance();
        updateTime.setTimeZone(TimeZone.getDefault());
        updateTime.set(Calendar.HOUR_OF_DAY, 12);
        updateTime.set(Calendar.MINUTE, 30);
        Intent downloader = new Intent(getApplicationContext(), AlarmReceiver.class);
        downloader.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, downloader, PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+AlarmManager.INTERVAL_DAY * 3,  AlarmManager.INTERVAL_DAY * 3, pendingIntent);
//        sendBroadcast(downloader);
    }

    private void Offline_Data() {
        String jsonFavorites = SharedPrefs.getString(SplashHomeActivity.this, SharedPrefs.SPLASH_AD_DATA);
        Log.e("TAG", "jsonFavorites :" + jsonFavorites.toString());
        Share.al_ad_data.clear();
        try {
            JSONObject data2 = new JSONObject(jsonFavorites.toString());
            JSONArray arr_data = data2.getJSONArray("data");

            for (int i = 0; i < arr_data.length(); i++) {
                JSONObject obj_data = arr_data.getJSONObject(i);
                if (!getApplicationContext().getPackageName().equals(obj_data.getString("package_name"))) {
                    AdModel adModel = new AdModel();
                    adModel.setApp_link(obj_data.getString("app_link"));
                    adModel.setThumb_image(obj_data.getString("thumb_image"));
                    adModel.setFull_thumb_image(obj_data.getString("full_thumb_image"));
                    adModel.setPackage_name(obj_data.getString("package_name"));
                    adModel.setName(obj_data.getString("name"));
                    Share.al_ad_data.add(adModel);
                }
            }
            new DownLoadFullAdData().execute("");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class DownLoadFullAdData extends AsyncTask<String, Void, Void> {
        ArrayList<Bitmap> b = new ArrayList<>();
        ArrayList<Integer> position = new ArrayList<>();

        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {

                for (int i = 0; i < Share.al_ad_data.size(); i++) {

                    if (Share.al_ad_data.get(i).getFull_thumb_image() != null && !Share.al_ad_data.get(i).getFull_thumb_image().equalsIgnoreCase("")) {
                        final int finalI = i;
                        final Bitmap theBitmap = Glide.
                                with(activity).
                                load(Share.al_ad_data.get(i).getFull_thumb_image()).
                                asBitmap().
                                into(300, 300). // Width and height
                                get();
                        Log.e("bitmap", "bitmap" + theBitmap);
                        b.add(theBitmap);
                        position.add(i);

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Share.full_ad.clear();
            for (int p = 0; p < position.size(); p++) {
                AdModel model = Share./**/al_ad_data.get(position.get(p));
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                b.get(p).compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] bytedata = baos.toByteArray();

                String encodedImage = Base64.encodeToString(bytedata, Base64.DEFAULT);
                model.setFull_img(encodedImage);
                Share.full_ad.add(model);
            }
            Log.e("asaaa", "sdsadas" + Share.full_ad.size());
            Gson gson = new Gson();
            String jsonad = gson.toJson(Share.full_ad);
            SharedPrefs.save(getApplicationContext(), SharedPrefs.FULL_AD_IMAGE, jsonad);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            final Dialog dialog = new Dialog(SplashHomeActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.choose_category_alert1);

            SmileRating smileRating = (SmileRating) dialog.findViewById(R.id.smile_rating);
            Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
            Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    Intent finish = new Intent(SplashHomeActivity.this, ExitApplication.class);
                    finish.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(finish);
                    finish();
                }
            });

            btn_yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        dialog.dismiss();
                        AccountRedirectActivity.get_url(SplashHomeActivity.this);
                    } catch (Exception anfe) {
                    }
                }
            });

            smileRating.setOnSmileySelectionListener(new SmileRating.OnSmileySelectionListener() {
                @Override
                public void onSmileySelected(@BaseRating.Smiley int smiley, boolean reselected) {
                    switch (smiley) {
                        case SmileRating.BAD:
                            Toast.makeText(SplashHomeActivity.this, "Thanks for review", Toast.LENGTH_SHORT).show();
                            break;
                        case SmileRating.GOOD:
                            rate_app();
                            break;
                        case SmileRating.GREAT:
                            rate_app();
                            break;
                        case SmileRating.OKAY:
                            Toast.makeText(SplashHomeActivity.this, "Thanks for review", Toast.LENGTH_SHORT).show();
                            break;
                        case SmileRating.TERRIBLE:
                            Toast.makeText(SplashHomeActivity.this, "Thanks for review", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            });
            dialog.show();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void rate_app() {
        Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName());
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException anfe) {
            // Hmm, market is not installed
            Intent your_browser_intent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://market.android.com/details?id=" + getApplicationContext().getPackageName()));
            startActivity(your_browser_intent);
        }
    }
}
