package com.coloring.book.mandala.forme.art.share;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Bansi on 27-02-2017.
 */
public class ExitApplication extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        finish();
      //  System.exit(0);
    }
}
