package com.coloring.book.mandala.forme.art;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AppCompatDelegate;
import android.util.Base64;
import android.util.Log;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;

import static android.content.ContentValues.TAG;

/**
 * Created by Bansi on 13-01-2018.
 */
public class ColoringApplication extends MultiDexApplication {

    private static ColoringApplication singleton;
    public AdRequest ins_adRequest;
    public InterstitialAd mInterstitialAd;

    @Override
    public void onCreate() {
        super.onCreate();

        //TODO to solve camera & URI issue
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        String android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceId = md5(android_id).toUpperCase();
        Log.e("device id","------->"+deviceId);
/*
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);
*/
        // Normal app init code...
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        singleton = this;

        getKeyHash();
        LoadAds();
        InitializeService();
    }

    public static final String md5(final String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
        }
        return "";
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    public void getKeyHash() {
        PackageInfo info = null;
        try {
            info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public void LoadAds() {

        try {
            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId(getApplicationContext().getResources().getString(R.string.Intertial_Id));

            ins_adRequest = new AdRequest.Builder()
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .addTestDevice("E19949FB5E7C5A28C30A875934AC8181") //SWIPE
                    .addTestDevice("41E9C9F5D1F985FB36C9760EFC8F3916") //Lenovo
                    .addTestDevice("CEB37F370ECF287C057F70BBC902632E")  //Coolpad
                    .addTestDevice("51A49E7B1B359D1999E5C85CE4F54978") //XOLO
                    .addTestDevice("F9EBC1840023CB004A83005514278635")  //MI 6otu (No SIM)
                    .addTestDevice("2442EC754FEF046014B26ACCEEAE9C23") // Micromax New
                    .addTestDevice("413FAED40213710754F4D30AC4F60355")  //INTEX
                    .addTestDevice("A7A19E06342F7D3868ABA7863D707BD7") //Samsung Tab
                    .addTestDevice("F9EB72307322A818A3399B51B00A765D") //LAVA
                    .addTestDevice("0E45853B0874EAA6418891AD964CC470") //X-ZIOX
                    .addTestDevice("3C8E4AA9C3802D60B83603426D16E430") //Celkon
                    .addTestDevice("89FAEE279F58CCC3FA1ABC0904E1FCBE") //Karbonn
                    .addTestDevice("74527FD0DD7B0489CFB68BAED192733D") //Nexus TAB
                    .addTestDevice("26DFCD14E05E54F19609B18400F672A0") //Samsung j2
                    .addTestDevice("E56855A0C493CEF11A7098FE6EA840CB") //Videocon
                    .addTestDevice("390FED1AE343E9FF9D644C4085C3868E") //jivi
                    .addTestDevice("ACFC7B7082B3F3FD4E0AC8E92EA10D53") //MI Tab
                    .addTestDevice("863D8BAE88E209F38FF3C94A0403C776") //Samsung old
                    .addTestDevice("85F8E335F3BBDFA2782587EEA19688D2") //Samsung new
                    .addTestDevice("517048997101BE4535828AC2360582C2") //motorola
                    .addTestDevice("8BB4BCB27396AB8ED222B7F902E13420") //micromax old
                    .addTestDevice("7F3924D7AB776DC5FF2D314CCFEE0EE0")//Gionee
                    .addTestDevice("F7803FE72A2748F6028D87DC36D7C574") //Mi Chhotu JIO
                    .addTestDevice("36FE3612E964A00DC40A18301E964202") // nokia
                    .build();

            mInterstitialAd.loadAd(ins_adRequest);
        } catch (Exception e) {
        }
    }

    private void InitializeService() {
        try {
            OneSignal.startInit(this)
                    .setNotificationOpenedHandler(new ExampleNotificationOpenedHandler())
                    .autoPromptLocation(true)
                    .init();

            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                    .diskCacheExtraOptions(480, 800, null)
                    .diskCacheSize(100 * 1024 * 1024)
                    .diskCacheFileCount(100)
                    .build();
            ImageLoader.getInstance().init(config);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean requestNewInterstitial() {

        try {
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public boolean isLoaded() {

        try {
            if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    public class ExampleNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
        // This fires when a notification is opened by tapping on it.
        @Override
        public void notificationOpened(OSNotificationOpenResult result) {

            String launchURL = result.notification.payload.launchURL;

            String smallIcon = result.notification.payload.smallIcon;
            Log.e("smallIcon", "---->" + smallIcon);
            Log.e("launchURL", "0------->" + launchURL);

            if (launchURL != null) {
                Log.d("", "Launch URL: " + launchURL);
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(launchURL));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent
                            .FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);

                } catch (android.content.ActivityNotFoundException anfe) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(launchURL));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent
                            .FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                }
            }
        }
    }

    public void onTerminate() {
        super.onTerminate();
    }

    public static ColoringApplication getInstance() {
        return singleton;
    }

}
