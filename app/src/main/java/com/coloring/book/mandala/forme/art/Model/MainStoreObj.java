package com.coloring.book.mandala.forme.art.Model;

import android.graphics.Matrix;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Bansi on 24-11-2017.
 */
public class MainStoreObj{

    private String path;
    private Boolean is_saved;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Boolean getIs_saved() {
        return is_saved;
    }

    public void setIs_saved(Boolean is_saved) {
        this.is_saved = is_saved;
    }

    public Boolean getIs_fav() {
        return is_fav;
    }

    public void setIs_fav(Boolean is_fav) {
        this.is_fav = is_fav;
    }

    private Boolean is_fav;

    private String app_id;

    public String getScreenshot() {
        return screenshot;
    }

    public void setScreenshot(String screenshot) {
        this.screenshot = screenshot;
    }

    private String image_id;
    private ArrayList<UndoObj> undo_list = new ArrayList<>();
    private ArrayList<UndoObj> redo_list = new ArrayList<>();
    private String screenshot;

    public ArrayList<UndoObj> getUndo_list() {
        return undo_list;
    }

    public void setUndo_list(ArrayList<UndoObj> undo_list) {
        this.undo_list = undo_list;
    }

    public ArrayList<UndoObj> getRedo_list() {
        return redo_list;
    }

    public void setRedo_list(ArrayList<UndoObj> redo_list) {
        this.redo_list = redo_list;
    }

    public Matrix getZooming_scale() {
        return Zooming_scale;
    }

    public void setZooming_scale(Matrix zooming_scale) {
        Zooming_scale = zooming_scale;
    }

    private Matrix Zooming_scale;

    private ArrayList<SVG_obj> object_list = new ArrayList<>();

    public ArrayList<SVG_obj> getObject_list() {
        return object_list;
    }

    public void setObject_list(ArrayList<SVG_obj> object_list) {
        this.object_list = object_list;
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getImage_id() {
        return image_id;
    }

    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }
}
