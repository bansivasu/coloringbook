package com.coloring.book.mandala.forme.art.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.coloring.book.mandala.forme.art.MainActivity;
import com.coloring.book.mandala.forme.art.R;
import com.coloring.book.mandala.forme.art.share.Share;

/**
 * Created by Bansi on 03-01-2018.
 */
public class MyCustomPagerAdapter extends PagerAdapter {
    public ImageView iv_first_color, iv_second_color, iv_third_color;
    Context context;
    int images[];
    LayoutInflater layoutInflater;


    public MyCustomPagerAdapter(Context context, int[] images, ImageView iv_first_color, ImageView iv_second_color, ImageView iv_third_color) {
        this.context = context;
        this.images = images;
        this.iv_first_color = iv_first_color;
        this.iv_second_color = iv_second_color;
        this.iv_third_color = iv_third_color;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        final View itemView = layoutInflater.inflate(R.layout.viewpager_item, container, false);
        itemView.setTag("myview" + position);

        final ImageView imageView = (ImageView) itemView.findViewById(R.id.iv_color_picker);
        imageView.setImageResource(images[position]);

        if (position == 0) {
          //  MainActivity.image = imageView;
            imageView.setDrawingCacheEnabled(true);
            imageView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);

            //  final Bitmap bitmap = ((BitmapDrawable) MainActivity.image.getDrawable()).getBitmap();
            imageView.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        final int action = event.getActionMasked();
                        switch (event.getActionMasked()) {
                            case MotionEvent.ACTION_DOWN:
                                int x = (int) event.getX();
                                int y = (int) event.getY();

                                final Bitmap drawingCache = imageView.getDrawingCache();
                                int color = Share.getPixelAtPoint(drawingCache, x, y);
                                if (color != Color.TRANSPARENT && color != Color.WHITE && color != Color.BLACK) {
                                    Share.thirdcolor = Share.secondcolor;
                                    Share.secondcolor = Share.currentcolor;
                                    Share.currentcolor = Share.firstcolor = color;
                                    iv_third_color.setBackgroundColor(Share.thirdcolor);
                                    iv_second_color.setBackgroundColor(Share.secondcolor);
                                    iv_first_color.setBackgroundColor(color);
                                    iv_first_color.performClick();
                                }
                                break;
                            case MotionEvent.ACTION_UP:
                                break;
                        }
                        return false;
                    }
            });
        }
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}