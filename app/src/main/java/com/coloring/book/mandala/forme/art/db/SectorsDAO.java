package com.coloring.book.mandala.forme.art.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.util.Log;

import com.coloring.book.mandala.forme.art.Model.FavObject;
import com.coloring.book.mandala.forme.art.share.Share;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

public class SectorsDAO extends dbsDAO {

    Bitmap bitmap = null;

    private static final String WHERE_MAP_SECTORS_EQUALS = DataBaseHelper.KEY_MAP
            + " =?";

    private static final String WHERE_ID_ROW_EQUALS = DataBaseHelper.ID_ROW
            + " =?";

    private final DataBaseHelper.TABLES tableName = DataBaseHelper.TABLES.SECTORS_TABLE;
    private DataBaseHelper.SECTORS sectorType;

    ArrayList<Integer> sectorsID;
    ArrayList<Integer> sectors;
    ArrayList<Integer> allsectors;

    public SectorsDAO(Context context, DataBaseHelper.SECTORS sectorType) {
        super(context);
        this.sectorType = sectorType;
        sectorsID = new ArrayList<>();
    }

    public long init(ArrayList<Integer> sectorsColors) {
        Log.e("size of array", "size" + sectorsColors.size());
        if (!database.isOpen())
            open();
        clear();
        sectors = sectorsColors;
        ContentValues values = new ContentValues();
        long res = -1;
        for (int color : sectors) {
            values.put(DataBaseHelper.KEY_MAP, sectorType.toString());
            values.put(DataBaseHelper.COLOR_COLUMN, color);
            res = database.insert(tableName.toString(), null, values);
            Log.e("size of array", "size" + res);
            if (res == -1) break;
        }

        sectorsID.clear();
        Cursor cursor = database.query(tableName.toString(), new String[]{DataBaseHelper.ID_ROW},
                WHERE_MAP_SECTORS_EQUALS, new String[]{sectorType.toString()}, null,
                null, null);

        while (cursor.moveToNext()) {
            sectorsID.add(cursor.getInt(0));
        }
        cursor.close();
        database.close();
        return res;
    }

    public void save_pref_data(String name, String json) {
        try {
            open();
            Log.e("name","name"+name);
            ContentValues values = new ContentValues();
            if (is_available_pref(name))
            {
                values.put(DataBaseHelper.ALL_DATA, json);
                database.update(DataBaseHelper.TABLE_PREF_DATA, values, "id=?", new String[]{name});
            }
            else {
                values.put(DataBaseHelper.ID_ROW, name);
                values.put(DataBaseHelper.ALL_DATA, json);
                database.insertWithOnConflict(DataBaseHelper.TABLE_PREF_DATA, null, values, SQLiteDatabase.CONFLICT_IGNORE);
            }
            database.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void savePathData(String savedpath, String image_id, String app_id, boolean is_save, boolean is_fav, String thumb_url, String all_data, String image_url) {
        try {
            byte[] byteArray1 = new byte[0];
            ContentValues values = new ContentValues();
            open();
            if (is_fav) {
                values.put(DataBaseHelper.IS_DATETIME, getDateTime());
                Log.e("date and time","==>"+getDateTime());
            }
            if (is_available(app_id, image_id)) {
                values.put(DataBaseHelper.IS_SAVE, is_save);
                values.put(DataBaseHelper.IS_FAV, is_fav);
                if (all_data != null)
                    values.put(DataBaseHelper.ALL_DATA, all_data);
                database.update(DataBaseHelper.TABLE_SAVE_IMAGES, values, "app_id=? and image_id=?", new String[]{app_id, image_id});
            } else {
                if (savedpath == null) {
                    savedpath = thumb_url;
                }
                values.put(DataBaseHelper.IMAGE_PATH, savedpath);
                values.put(DataBaseHelper.APP_ID, app_id);
                values.put(DataBaseHelper.IMAGE_ID, image_id);
                values.put(DataBaseHelper.IS_SAVE, is_save);
                values.put(DataBaseHelper.IS_FAV, is_fav);
                values.put(DataBaseHelper.IMAGE_URL, image_url);
                values.put(DataBaseHelper.THUMB_URL, thumb_url);
                values.put("Title",Share.title);
               /* if (all_data != null)
                    values.put(DataBaseHelper.ALL_DATA, all_data);*/

                Log.e("insert", "insert");
                database.insertWithOnConflict(DataBaseHelper.TABLE_SAVE_IMAGES, null, values, SQLiteDatabase.CONFLICT_IGNORE);
            }

           /* ContentValues values1 = new ContentValues();
            values1.put(DataBaseHelper.IMAGE_ID, image_id);
            values1.put(DataBaseHelper.APP_ID, app_id);
            values1.put(DataBaseHelper.IS_FAV, is_fav);
            database.replace(DataBaseHelper.FAV_TABLE, null, values1);*/
            database.close();
            if (all_data != null)
            save_pref_data("svg_" + Share.image_id + "_" + Share.app_id, all_data);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    //-------------------------- CHECK ------------------------------------------------//
    public Boolean is_available(String app_id, String image_id) {
        try {
            if (database.rawQuery("select * FROM " + DataBaseHelper.TABLE_SAVE_IMAGES + " WHERE " + DataBaseHelper.APP_ID + " = '" + app_id + "' AND " + DataBaseHelper.IMAGE_ID + " = '" + image_id+"'", null).getCount() > 0) {
                Log.e("available","available");
                return true;
            }
            return false;
        } catch (Throwable th) {
            return false;
        }
    }

    public Boolean is_available_pref(String name) {
        try {
            if (database.rawQuery("select * FROM " + DataBaseHelper.TABLE_PREF_DATA + " WHERE " + DataBaseHelper.ID_ROW + " = '" + name+"'", null).getCount() > 0) {
                Log.e("true","true");
                return true;
            }
            return false;
        } catch (Throwable th) {
            return false;
        }
    }

    public Boolean is_favourite(String app_id, String image_id) {
        try {
            Cursor c = database.rawQuery("select * FROM " + DataBaseHelper.TABLE_SAVE_IMAGES + " WHERE " + DataBaseHelper.APP_ID + " = " + app_id + " AND " + DataBaseHelper.IMAGE_ID + " = " + image_id, null);
            if (c.getCount() > 0) {
                c.moveToFirst();
                int m = c.getInt(4);
                Log.e("is_faaav", "---" + m);
                c.close();
                if (m == 0) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } catch (Throwable th) {
            return false;
        }
    }

    //-------------------------- GET DATA FROM THE TABLE ------------------------------------//

    public ArrayList<FavObject> getFavData() {
        try {
            open();
            Cursor cursor = database.rawQuery("select * FROM " + DataBaseHelper.TABLE_SAVE_IMAGES + " WHERE " + DataBaseHelper.IS_FAV + " = 1 ORDER BY date("+DataBaseHelper.IS_DATETIME+") DESC", null);

            ArrayList<FavObject> fav_data = new ArrayList<>();
            String path = null;
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        path = cursor.getString(0);
                        FavObject obj = new FavObject();
                        obj.setPath(cursor.getString(0));
                        obj.setImage_id(cursor.getString(1));
                        obj.setApp_id(cursor.getString(2));
                        obj.setImage_url(cursor.getString(6));
                        obj.setThumb_url(cursor.getString(7));
                        obj.setDateString(cursor.getString(8));
                        obj.setCat_name(cursor.getString(9));
                        Log.e("path----", "path" + path+"///"+cursor.getString(8));
                        fav_data.add(obj);
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
/*
            Cursor cursor1 = database.rawQuery("select * FROM " + DataBaseHelper.FAV_TABLE + " WHERE " + DataBaseHelper.IS_FAV + " = mandala1", null);
            cursor1.moveToLast();
            do {
                Log.e("iddddd","id"+cursor1.getString(mandala1));
            }while (cursor1.moveToPrevious());
*/
            database.close();
            return fav_data;
        } catch (Exception t) {
            t.printStackTrace();
            return null;
        }
    }

    public Boolean unfavouriteAll() {
        try {
            open();
            ContentValues values = new ContentValues();
            values.put(DataBaseHelper.IS_FAV, false);
            database.update(DataBaseHelper.TABLE_SAVE_IMAGES, values, DataBaseHelper.IS_FAV + " = 1", null);
            if (database.isOpen())
                database.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public String getAllData(String name) {
        try {
            if (!database.isOpen())
                open();
            Cursor c = database.rawQuery("select * FROM " + DataBaseHelper.TABLE_PREF_DATA + " WHERE " + DataBaseHelper.ID_ROW + " = '" +name +"'", null);

            if (c.getCount() > 0) {
                c.moveToFirst();
                String m = c.getString(1);
                Log.d("all_data", "---" + m);
                c.close();
                database.close();
                return m;
            } else {
                database.close();
                return null;
            }
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public long update(int sectorID, int color) {
        long i = 0;
        try {
            open();
            ContentValues values = new ContentValues();
            sectors.set(sectorID, color);
            values.put(DataBaseHelper.COLOR_COLUMN, color);

            Share.colors.clear();
            sectors = getSectors();

            i = (long) database.update(tableName.toString(), values,
                    WHERE_ID_ROW_EQUALS,
                    new String[]{
                            String.valueOf(sectorsID.get(sectorID))
                    });
            database.close();
           /* Share.colors.clear();
            Share.colors = sectors;*/
        } catch (Exception e) {
            e.printStackTrace();
        }
        return i;
    }

    public int delete(int sector) {
        return database.delete(tableName.toString(),
                WHERE_ID_ROW_EQUALS,
                new String[]{
                        String.valueOf(sectorsID.get(sector))
                });
    }

    public int clear() {
        return database.delete(tableName.toString(), null, null);
    }

    public ArrayList<Integer> getSectors() {

        if (sectors == null) {
            sectors = new ArrayList<>();
//            sectorsID.clear();

            Cursor cursor = database.query(tableName.toString(),
                    new String[]{
                            DataBaseHelper.ID_ROW,
                            DataBaseHelper.COLOR_COLUMN
                    },
                    WHERE_MAP_SECTORS_EQUALS,
                    new String[]{
                            sectorType.toString()
                    },
                    null,
                    null, null);

            while (cursor.moveToNext()) {
                sectorsID.add(cursor.getInt(0));
                sectors.add(cursor.getInt(1));
                Share.colors.add(cursor.getInt(1));
            }
        }
        return sectors;
    }

    public ArrayList<Integer> getallSectors() {
        return sectors;
    }
}
