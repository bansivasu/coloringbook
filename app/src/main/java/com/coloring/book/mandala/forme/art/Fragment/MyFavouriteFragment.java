package com.coloring.book.mandala.forme.art.Fragment;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.coloring.book.mandala.forme.art.Adapter.FavAdapter;
import com.coloring.book.mandala.forme.art.BuildConfig;
import com.coloring.book.mandala.forme.art.ColoringApplication;
import com.coloring.book.mandala.forme.art.MainActivity;
import com.coloring.book.mandala.forme.art.Model.FavObject;
import com.coloring.book.mandala.forme.art.Model.moreAppObject;
import com.coloring.book.mandala.forme.art.R;
import com.coloring.book.mandala.forme.art.db.DataBaseHelper;
import com.coloring.book.mandala.forme.art.db.SectorsDAO;
import com.coloring.book.mandala.forme.art.share.NetworkManager;
import com.coloring.book.mandala.forme.art.share.Share;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Bansi on 02-01-2018.
 */
public class MyFavouriteFragment extends Fragment implements View.OnClickListener {

    // widgets
    private RecyclerView rv_items;
    private LinearLayout ll_not_available, ll_items;
    private AdView mAdView;
    private TextView tv_not_avail;

    // variables
    private View viewFavFragment;
    public ProgressDialog mProgressDialog;
    private SectorsDAO sd;
    private ArrayList<FavObject> datalist = new ArrayList<>();
    private FavAdapter favAdapter;
    private Boolean isInForeGround;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewFavFragment = inflater.inflate(R.layout.fragment_favourite, container, false);

        setHasOptionsMenu(true);
        findResources(viewFavFragment);
        setListeners();
        init();
        setAdView(viewFavFragment);
        return viewFavFragment;
    }

    public static MyFavouriteFragment newInstance() {

        Bundle args = new Bundle();

        MyFavouriteFragment fragment = new MyFavouriteFragment();
        fragment.setArguments(args);
        return fragment;
    }
    private void findResources(View v) {

        rv_items = (RecyclerView) v.findViewById(R.id.rv_items);
        ll_not_available = (LinearLayout) v.findViewById(R.id.ll_not_available);
        ll_items = (LinearLayout) v.findViewById(R.id.ll_items);
        tv_not_avail = (TextView) v.findViewById(R.id.tv_not_avail);
        rv_items.setLayoutManager(new GridLayoutManager(getActivity(), 2));
    }

    private void setListeners() {
    }

    private void init() {
        tv_not_avail.setText(getResources().getString(R.string.fav_not_avail));

        sd = new SectorsDAO(getActivity(), DataBaseHelper.SECTORS.SECTORS_PHIL);

        favAdapter = new FavAdapter(getActivity(), datalist, new FavAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int i) {
                if (!ColoringApplication.getInstance().requestNewInterstitial()) {
                    Share.AllFilledData.clear();
                    Share.app_id = datalist.get(i).getApp_id();
                    Share.image_id = datalist.get(i).getImage_id();
                    Share.title = datalist.get(i).getCat_name();
                    Share.undoItems.clear();
                    Share.redoItems.clear();
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.putExtra("is_fav",true);
                    intent.putExtra("imageurl",datalist.get(i).getImage_url());
                    intent.putExtra("thumburl",datalist.get(i).getThumb_url());
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else {
                    ColoringApplication.getInstance().mInterstitialAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdClosed() {
                            super.onAdClosed();
                               ColoringApplication.getInstance().mInterstitialAd.setAdListener(null);
                                ColoringApplication.getInstance().mInterstitialAd = null;
                                ColoringApplication.getInstance().ins_adRequest = null;
                                ColoringApplication.getInstance().LoadAds();

                                Share.AllFilledData.clear();
                                Share.app_id = datalist.get(i).getApp_id();
                                Share.image_id = datalist.get(i).getImage_id();
                                Share.title = datalist.get(i).getCat_name();
                                Share.undoItems.clear();
                                Share.redoItems.clear();
                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                intent.putExtra("is_fav",true);
                                intent.putExtra("imageurl",datalist.get(i).getImage_url());
                                intent.putExtra("thumburl",datalist.get(i).getThumb_url());
                                startActivity(intent);
                        }

                        @Override
                        public void onAdFailedToLoad(int i) {
                            super.onAdFailedToLoad(i);
                        }

                        @Override
                        public void onAdLoaded() {
                            super.onAdLoaded();
                        }
                    });
                }
            }
        });
        rv_items.setAdapter(favAdapter);
    }

    private void setAdView(View v) {
        try
        {
            mAdView = (AdView) v.findViewById(R.id.adView);
            if(NetworkManager.isInternetConnected(getActivity().getApplicationContext()))
            {
                //TODO : Ads
                AdRequest adRequest = new AdRequest.Builder()
                        .addTestDevice(Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID))
                        .addTestDevice("67F459F428BA080AC0E5D48751A42AD7")  //sumsung office
                        .addTestDevice("03E2207FBED3E8811B414918D8077E25")
                        .addTestDevice("74527FD0DD7B0489CFB68BAED192733D")
                        .addTestDevice("7D27AE6CC478DBF13BAEFEB9F873562B")
                        .addTestDevice("E65765D74A642A5F0993F9107AE0B307")
                        .addTestDevice("86021572C8EFA2DD0DB69DB2BA2CA050")
                        .addTestDevice("EC0086E4DD57398BD70018389A92BB9A")
                        .addTestDevice("53E4CD6C36D8A29795391C24C02724F8")
                        .addTestDevice("790037035108AEA31323422EBA149D03")
                        .addTestDevice("3A9619098ED320FC729B6ED2972C7536")
                        .addTestDevice("7377159F8453DCC60F4109F19FA52FFE")
                        .addTestDevice("6951A7DFDC016130A7C94F5568794431")
                        .addTestDevice("AE74B0C567C2121A613144D22D3B0554")
                        .build();
                mAdView.loadAd(adRequest);
                mAdView.setAdListener(new AdListener(){
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        mAdView.setVisibility(View.VISIBLE);
                    }
                });
            }
            else
            {
                mAdView.setVisibility(View.GONE);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void showToastMsg(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    private void showProgressDialog() {
        try {
            if (mProgressDialog == null) {
                mProgressDialog = ProgressDialog.show(getActivity(), BuildConfig.FLAVOR, getResources().getString(R.string.loading), false, false);
            }
            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    public void hideDialog() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        } catch (IllegalArgumentException e) {
        }
    }


    private void LoadData(final boolean b) {
        datalist.clear();
        ArrayList<FavObject> fav_data = sd.getFavData();
        if (fav_data.size() > 0) {
            datalist.addAll(fav_data);
        }
        ArrayList<String> datestrings = new ArrayList<>();
        for (int i = 0; i < datalist.size(); i++) {
            datestrings.add(datalist.get(i).getDateString());
        }

        Collections.sort(datestrings, new Comparator<String>() {
            DateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            @Override
            public int compare(String o1, String o2) {
                try {
                    return f.parse(o1).compareTo(f.parse(o2));
                } catch (ParseException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        });
        for (int i = 0; i < datestrings.size(); i++) {
            Log.e("000000","0000"+datestrings.get(i));
        }
        ArrayList<FavObject> fav_list = new ArrayList<>();
        fav_list.addAll(datalist);
        datalist.clear();

        for (int j = datestrings.size()-1; j >= 0; j--) {
            for (int i = 0; i < fav_list.size(); i++) {
                if (datestrings.get(j).equalsIgnoreCase(fav_list.get(i).getDateString()))
                {
                    datalist.add(fav_list.get(i));
                    break;
                }
            }
        }
        fav_list = null;
        datestrings = null;

        if (datalist.size()>0)
        {
            ll_items.setVisibility(View.VISIBLE);
            ll_not_available.setVisibility(View.GONE);
        }
        else {
            ll_items.setVisibility(View.GONE);
            ll_not_available.setVisibility(View.VISIBLE);
        }
        favAdapter.notifyDataSetChanged();
        getActivity().invalidateOptionsMenu();
        hideDialog();
    }

/*
    private void DisplayList(ImageObject mainobj, boolean b) {
        recyclerViewHome.setVisibility(View.VISIBLE);
        ll_no_internet_home.setVisibility(View.GONE);

        Share.datalist.clear();
        Share.pathlists.clear();
        Share.datalist = mainobj.data;
        mainobj = null;

        for (int i = 0; i < Share.datalist.size(); i++) {

            Gson maingson = new Gson();
            ImageObject.dataobj item = (ImageObject.dataobj) Share.datalist.get(i);
            String mainjson = SharedPrefs.getString(getActivity().getApplicationContext(), "svg_data_" + item.getApp_id() + "_" +
                    item.getId());
            StoreObj = maingson.fromJson(mainjson, MainStoreObj.class);
            if (StoreObj != null) {
                Share.pathlists.add(StoreObj);
                StoreObj = null;
            }
        }

        recyclerViewHome.removeAllViews();
        for (int i = 0; i <  Share.datalist.size(); i++) {
            LayoutInflater inflater =(LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View myView = inflater.inflate(R.layout.list_item, null);
            ImageView iv_full_img = (ImageView) myView.findViewById(R.id.iv_full_image);
            Glide.with(getActivity())
                    .load(Share.datalist.get(i).getThumb_image())
                    .override(300,300)
                    .into(iv_full_img);

            iv_full_img.getLayoutParams().height = (int) (DisplayMetricsHandler.getScreenWidth() * 0.4);
            final int finalI = i;
            iv_full_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Share.clicked_svg = finalI;
                    Share.AllFilledData.clear();
                    Share.undoItems.clear();
                    Share.redoItems.clear();
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    startActivity(intent);
                }
            });
            recyclerViewHome.addView(myView);
        }
        hideDialog();

       */
/* homeAdapter = null;
        homeAdapter = new HomeAdapter(getActivity(), Share.datalist, new HomeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int i) {
                Share.clicked_svg = i;
                Share.AllFilledData.clear();
                Share.undoItems.clear();
                Share.redoItems.clear();
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
            }
        });
        recyclerViewHome.setAdapter(homeAdapter);*//*

    }
*/

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_retry_home /*2131558584*/:
                showProgressDialog();
                LoadData(false);
                break;
            default:
                break;
        }
    }

    public void onResume() {
        super.onResume();
        isInForeGround = true;
        if (!ColoringApplication.getInstance().isLoaded())
            ColoringApplication.getInstance().LoadAds();
            showProgressDialog();
            if (sd != null)
                sd = new SectorsDAO(getActivity(), DataBaseHelper.SECTORS.SECTORS_PHIL);
            LoadData(false);
    }

    public void onPause() {
        super.onPause();
    }

    public void onDestroyView() {
        isInForeGround = false;
        super.onDestroyView();
        viewFavFragment.destroyDrawingCache();
        viewFavFragment = null;
    }

    public void onStop() {
        super.onStop();
        isInForeGround = false;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item_delete = menu.findItem(R.id.delete_items);
        if (datalist.size()>0)
        {
            item_delete.setIcon(getResources().getDrawable(R.drawable.ic_delete_big));
        }
        else {
            item_delete.setIcon(getResources().getDrawable(R.drawable.ic_delete_desable));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_items:
                if (datalist != null && datalist.size()>0) {
                    try {
                        android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(getActivity());
                        alert.setMessage(getResources().getString(R.string.my_favorite_dlt_all));
                        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                                Log.e("favourite","favourite");
                                Boolean b = sd.unfavouriteAll();
                                if (b) {
                                    datalist.clear();
                                    favAdapter.notifyDataSetChanged();
                                    Toast.makeText(getActivity(), getResources().getString(R.string.success_unfav_all),Toast.LENGTH_SHORT);
                                    ll_not_available.setVisibility(View.VISIBLE);
                                    ll_items.setVisibility(View.GONE);
                                    getActivity().invalidateOptionsMenu();
                                }
                                else {
                                    Toast.makeText(getActivity(), getResources().getString(R.string.exception_msg),Toast.LENGTH_SHORT);
                                }
                            }
                        });
                        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });
                        alert.show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
