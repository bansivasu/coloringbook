package com.coloring.book.mandala.forme.art.db;

import android.graphics.Path;

/**
 * Created by Bansi on 18-11-2017.
 */
public class PathModel {

    Path path;

    public Boolean getIs_back() {
        return is_back;
    }

    public void setIs_back(Boolean is_back) {
        this.is_back = is_back;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    Boolean is_back;
}
