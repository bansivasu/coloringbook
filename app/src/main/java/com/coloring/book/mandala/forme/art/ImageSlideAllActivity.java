package com.coloring.book.mandala.forme.art;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.coloring.book.mandala.forme.art.share.Share;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;

public class ImageSlideAllActivity extends Activity implements View.OnClickListener, ViewPager.OnPageChangeListener {

    //Widgets-------
    private ViewPager viewPager;
    private ImageView ivShareFB, ivShareInsta, ivShareMail, ivShareWhatsApp, ivShare, ivNavBack;
    private TextView tv_count;
    private AdView mAdView;
    private ArrayList<File> fav_list = new ArrayList<>();
    private LinearLayout ll_no_photos;
    private RelativeLayout rll_pager;

    //Variables-----
    private String TAG = ImageSlideAllActivity.class.getSimpleName();
    private MyViewPagerAdapter myViewPagerAdapter;

    private ImageView ivDelete;
    private int selectedPosition = 0;
    private String mBackToGallery = "no";
    private FirebaseAnalytics mFirebaseAnalytics;
    private String from_main = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_slide);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        findViews();
        setListeners();
        initViews();
        adsBanner();
    }

    public void findViews() {

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tv_count = (TextView) findViewById(R.id.tv_count);
        ivNavBack = (ImageView) findViewById(R.id.ivNavBack);
        ivDelete = (ImageView) findViewById(R.id.ivDelete);
        mAdView = (AdView) findViewById(R.id.adView);

        ivShareFB = (ImageView) findViewById(R.id.ivShareFB);
        ivShareInsta = (ImageView) findViewById(R.id.ivShareInsta);
        ivShareMail = (ImageView) findViewById(R.id.ivShareMail);
        ivShareWhatsApp = (ImageView) findViewById(R.id.ivShareWhatsApp);
        ivShare = (ImageView) findViewById(R.id.ivShare);

        ll_no_photos = (LinearLayout) findViewById(R.id.ll_no_photos);
        rll_pager = (RelativeLayout) findViewById(R.id.rll_pager);

    }

    public void setListeners() {

        ivNavBack.setOnClickListener(this);
        ivDelete.setOnClickListener(this);

        viewPager.addOnPageChangeListener(this);

        ivShareFB.setOnClickListener(this);
        ivShareInsta.setOnClickListener(this);
        ivShareMail.setOnClickListener(this);
        ivShareWhatsApp.setOnClickListener(this);
        ivShare.setOnClickListener(this);
    }

    public void initViews() {

        selectedPosition = Share.selectGalleryImgPos;

        if (getIntent() != null)
        {
            from_main = getIntent().getStringExtra("is_main");
        }
        getData();
    }

    public void getData() {
        File path = new File(Share.IMAGE_PATH, "");
        File[] allFiles;
        fav_list.clear();
        if (path.exists()) {

            allFiles = path.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return (name.endsWith(".jpg") || name.endsWith(".jpeg") || name.endsWith(".png"));
                }
            });
            if (allFiles != null) {
                if (allFiles.length > 0) {
                    for (int i = 0; i < allFiles.length; i++) {
                        fav_list.add(allFiles[i]);
                        Collections.sort(fav_list, Collections.reverseOrder());
                    }
                }
            }
        }
        if (fav_list.size() > 0) {
            ivDelete.setEnabled(true);
            ivDelete.setImageDrawable(getResources().getDrawable(R.drawable.ic_delete_big));
            ll_no_photos.setVisibility(View.GONE);
            rll_pager.setVisibility(View.VISIBLE);
            myViewPagerAdapter = new MyViewPagerAdapter();
            viewPager.setAdapter(myViewPagerAdapter);
            setCurrentItem(selectedPosition);
        } else {
            ivDelete.setEnabled(false);
            ivDelete.setImageDrawable(getResources().getDrawable(R.drawable.ic_delete_desable));
            tv_count.setText("My Saved");
            ll_no_photos.setVisibility(View.VISIBLE);
            rll_pager.setVisibility(View.GONE);
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.ivDelete:
                deleteImage(viewPager.getCurrentItem());
                break;
            case R.id.ivNavBack:
                onBackPressed();
                break;
            case R.id.ivShareFB:
                shareInFb();
                break;
            case R.id.ivShareInsta:
                shareInInstagram();
                break;
            case R.id.ivShareMail:
                shareInEmail();
                break;
            case R.id.ivShareWhatsApp:
                shareInWhatsapp();
                break;
            case R.id.ivShare:
                share();
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        displayMetaInfo(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void setCurrentItem(int position) {
        viewPager.setCurrentItem(position, false);
        displayMetaInfo(selectedPosition);
    }

    private void displayMetaInfo(int position) {

        if (fav_list.size() > 0) {
            String txt1 = (position + 1) <= 9 ? "0" + String.valueOf((position + 1)) : String.valueOf(position + 1);
            String txt2 = fav_list.size() <= 9 ? "0" + String.valueOf(0 + fav_list.size()) : String.valueOf(0 + fav_list.size());
            if (txt1.equalsIgnoreCase("00"))
            {
                txt1 = "01";
            }
            tv_count.setText(txt1 + "/" + txt2);
        }
        else {
            tv_count.setText("My Saved");
        }
    }

    public class MyViewPagerAdapter extends PagerAdapter {

        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.image_fullscreen_preview, container, false);

            final ImageView imageViewPreview = (ImageView) view.findViewById(R.id.image_preview);

/*
            Picasso.with(ImageSlideAllActivity.this).load(fav_list.get(position))
                    .skipMemoryCache()
//                    .resize(800,800)
                    .placeholder(R.drawable.progress_animation)
                    .into(imageViewPreview, new Callback(){
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            imageViewPreview.setImageBitmap(null);
                        }
                    });
*/

            Glide.with(ImageSlideAllActivity.this)
                    .load(fav_list.get(position))
                    .placeholder(R.drawable.progress_animation)
                    .into(imageViewPreview);

            /*(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    imageViewPreview.setImageBitmap(resource);
                }

                @Override
                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                    super.onLoadFailed(e, errorDrawable);
                    imageViewPreview.setImageBitmap(null);
                }
            });
*/
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return fav_list.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == ((View) obj);
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        public void deletePage(int position, int position_view_pager) {
            fav_list.remove(position);
            notifyDataSetChanged();
            viewPager.setAdapter(myViewPagerAdapter);
            viewPager.setCurrentItem(position_view_pager);
            displayMetaInfo(viewPager.getCurrentItem());
        }
    }

    private void deleteImage(final int position) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ImageSlideAllActivity.this, R.style.MyAlertDialog);
        builder.setMessage(getResources().getString(R.string.msg_delete));
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Share.selectGalleryImgPos = viewPager.getCurrentItem();
                if (fav_list.size() > 0) {
                    new File(fav_list.get(Share.selectGalleryImgPos).getPath()).delete();
                    fav_list.remove(viewPager.getCurrentItem());
                }
                //TODO : added
                if (fav_list.size() == 0) {
//                                finish();
                    ivDelete.setEnabled(false);
                    ivDelete.setImageDrawable(getResources().getDrawable(R.drawable.ic_delete_desable));
                    tv_count.setText("My Saved");
                    ll_no_photos.setVisibility(View.VISIBLE);
                    rll_pager.setVisibility(View.GONE);
                } else {
                    if (fav_list.size() > 0) {
                        viewPager.setCurrentItem(Share.selectGalleryImgPos);
                        if(myViewPagerAdapter != null) {
                            myViewPagerAdapter.notifyDataSetChanged();
                            viewPager.setAdapter(myViewPagerAdapter);
                            viewPager.invalidate();
                            viewPager.setCurrentItem(position - 1);
                            Share.selectGalleryImgPos = position - 1;
                            displayMetaInfo(position-1);
                        }
                    }
                }
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        System.gc();

        if (from_main != null) {
            if (from_main.equalsIgnoreCase("1")) {
                Intent i = new Intent(ImageSlideAllActivity.this, HomeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            } else if (from_main.equalsIgnoreCase("2")) {
                Intent i = new Intent(ImageSlideAllActivity.this, Splash_MenuActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            } else {
                finish();
            }
        }
        else {
            finish();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mAdView != null) {
            mAdView.destroy();
        }

        try {
            viewPager.removeAllViews();
            viewPager.removeAllViewsInLayout();
            viewPager.destroyDrawingCache();
            fav_list.clear();
            myViewPagerAdapter = null;
        } catch (Exception e) {
        }
    }

    private void shareInFb() {
        if (appInstalledOrNot("com.facebook.katana")) {

            Intent share = new Intent(Intent.ACTION_SEND);
            share.setPackage("com.facebook.katana");
            if (fav_list.size() > 0) {
                share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(fav_list.get(viewPager.getCurrentItem()).getPath())));
            }
//            share.putExtra(Intent.EXTRA_TEXT, "Make more pics with app link \n https://play.google.com/store/apps/details?id=" + FullScreenImageActivity.this.getPackageName());
            share.setType("image/jpeg");
            startActivity(Intent.createChooser(share, "Share Picture"));
        } else
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.facebook.katana")));
    }

    private void shareInEmail() {

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
        if (fav_list.size() > 0) {
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(fav_list.get(viewPager.getCurrentItem()).getPath())));
        }
        intent.putExtra(Intent.EXTRA_TEXT, "Make more pics with app link \n https://play.google.com/store/apps/details?id=" + getPackageName());
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(Intent.createChooser(intent, "Share Picture"));
        }
    }

    private void shareInInstagram() {

        if (appInstalledOrNot("com.instagram.android")) {

            Intent share = new Intent(Intent.ACTION_SEND);
            share.setPackage("com.instagram.android");
            if (fav_list.size() > 0) {
                share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(fav_list.get(viewPager.getCurrentItem()).getPath())));
            }
            share.putExtra(Intent.EXTRA_TEXT, "Make more pics with app link \n https://play.google.com/store/apps/details?id=" + getPackageName());
            share.setType("image/jpeg");
            share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(Intent.createChooser(share, "Share Picture"));

        } else {

            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.instagram.android")));
            } catch (Exception e) {
                e.printStackTrace();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.instagram.android")));
            }
        }
    }

    private void shareInWhatsapp() {
        try {
            if (appInstalledOrNot("com.whatsapp")) {
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setPackage("com.whatsapp");
                share.setType("image/jpeg");
                share.putExtra(Intent.EXTRA_TEXT, "Make more pics with app link \n https://play.google.com/store/apps/details?id=" + getPackageName());
                if (fav_list.size() > 0) {
                    share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(fav_list.get(viewPager.getCurrentItem()).getPath())));
                }
                startActivity(Intent.createChooser(share, "Select"));
            } else
                Toast.makeText(ImageSlideAllActivity.this, "Whatsapp have not been installed", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void share() {

        try {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
            intent.setType("image/jpeg");
            intent.putExtra(Intent.EXTRA_TEXT, "Make more pics with app link \n https://play.google.com/store/apps/details?id=" + getPackageName());
            if (fav_list.size() > 0) {
                intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(fav_list.get(viewPager.getCurrentItem()).getPath())));
            }
            startActivity(Intent.createChooser(intent, "Share Picture"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void adsBanner() {
        try {
            AdRequest adRequest = new AdRequest.Builder()
                    //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    // Check the LogCat to get your test device ID
                    //.addTestDevice(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID))
                    .addTestDevice("E19949FB5E7C5A28C30A875934AC8181") //SWIPE
                    .addTestDevice("41E9C9F5D1F985FB36C9760EFC8F3916") //Lenovo
                    .addTestDevice("CEB37F370ECF287C057F70BBC902632E")  //Coolpad
                    .addTestDevice("51A49E7B1B359D1999E5C85CE4F54978") //XOLO
                    .addTestDevice("F9EBC1840023CB004A83005514278635")  //MI 6otu (No SIM)
                    .addTestDevice("2442EC754FEF046014B26ACCEEAE9C23") // Micromax New
                    .addTestDevice("413FAED40213710754F4D30AC4F60355")  //INTEX
                    .addTestDevice("A7A19E06342F7D3868ABA7863D707BD7") //Samsung Tab
                    .addTestDevice("F9EB72307322A818A3399B51B00A765D") //LAVA
                    .addTestDevice("0E45853B0874EAA6418891AD964CC470") //X-ZIOX
                    .addTestDevice("3C8E4AA9C3802D60B83603426D16E430") //Celkon
                    .addTestDevice("89FAEE279F58CCC3FA1ABC0904E1FCBE") //Karbonn
                    .addTestDevice("74527FD0DD7B0489CFB68BAED192733D") //Nexus TAB
                    .addTestDevice("26DFCD14E05E54F19609B18400F672A0") //Samsung j2
                    .addTestDevice("E56855A0C493CEF11A7098FE6EA840CB") //Videocon
                    .addTestDevice("390FED1AE343E9FF9D644C4085C3868E") //jivi
                    .addTestDevice("ACFC7B7082B3F3FD4E0AC8E92EA10D53") //MI Tab
                    .addTestDevice("863D8BAE88E209F38FF3C94A0403C776") //Samsung old
                    .addTestDevice("85F8E335F3BBDFA2782587EEA19688D2") //Samsung new
                    .addTestDevice("517048997101BE4535828AC2360582C2") //motorola
                    .addTestDevice("8BB4BCB27396AB8ED222B7F902E13420") //micromax old
                    .addTestDevice("7F3924D7AB776DC5FF2D314CCFEE0EE0")//Gionee
                    .addTestDevice("F7803FE72A2748F6028D87DC36D7C574") //Mi Chhotu JIO
                    .addTestDevice("36FE3612E964A00DC40A18301E964202") // nokia
                    .build();
            mAdView.loadAd(adRequest);

            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                }

                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    mAdView.setVisibility(View.GONE);
                }

                @Override
                public void onAdLeftApplication() {
                    super.onAdLeftApplication();
                }

                @Override
                public void onAdOpened() {
                    super.onAdOpened();
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    mAdView.setVisibility(View.VISIBLE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mAdView != null) {
            mAdView.pause();
        }
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }
}
